/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.io;

import net.coalevo.empp.impl.Activator;
import org.apache.commons.pool.BasePoolableObjectFactory;
import org.apache.commons.pool.impl.GenericObjectPool;

import javax.xml.stream.*;
import java.io.*;

/**
 * Provides a factory for I/O streams.
 *
 * @author Dieter Wimberger (coalevo)
 * @version @version@ (@date@)
 */
public class StreamFactory {

  //private static final Logger log = LoggerFactory.getLogger(StreamFactory.class);
  protected XMLInputFactory m_XMLInputFactory;
  protected XMLOutputFactory m_XMLOutputFactory;
  private static GenericObjectPool.Config m_PoolConfig;

  private GenericObjectPool m_BytesOutputStreams;
  private GenericObjectPool m_ByteBufferInputs;
  private GenericObjectPool m_ByteBufferOutputs;

  public StreamFactory() {
  }//StreamFactory

  public void prepare() {
    // pool config
    m_PoolConfig = new GenericObjectPool.Config();
    //set for
    m_PoolConfig.maxActive = 50;
    m_PoolConfig.maxIdle = 25;
    m_PoolConfig.maxWait = -1;
    m_PoolConfig.testOnBorrow = false;
    m_PoolConfig.testOnReturn = false;
    //make it growable so it adapts
    m_PoolConfig.whenExhaustedAction = GenericObjectPool.WHEN_EXHAUSTED_GROW;
    m_BytesOutputStreams = new GenericObjectPool(new BytesOutputStreamFactory(), m_PoolConfig); //factory
    m_ByteBufferOutputs = new GenericObjectPool(new ByteBufferOutputFactory(), m_PoolConfig); //factory
    m_ByteBufferInputs = new GenericObjectPool(new ByteBufferInputFactory(), m_PoolConfig); //factory
    //factories
    if (Activator.isStarted()) {
      m_XMLInputFactory = Activator.getXMLInputFactory();
      m_XMLOutputFactory = Activator.getXMLOutputFactory();
    } else {
      m_XMLInputFactory = javax.xml.stream.XMLInputFactory.newInstance();
      Activator.log().debug("!!!!!!!!!!!!!!!!!!!!!!! m_XMLInputFactory == " + m_XMLInputFactory);
      m_XMLOutputFactory = javax.xml.stream.XMLOutputFactory.newInstance();
      Activator.log().debug("!!!!!!!!!!!!!!!!!!!!!!! m_XMLOutputFactory == " + m_XMLOutputFactory);
    }
  }//prepare

  public final XMLStreamReader forInput(InputStream input)
      throws XMLStreamException {
    return m_XMLInputFactory.createXMLStreamReader(input);
  }//forInput

  public final XMLStreamWriter forOutput(OutputStream output)
      throws XMLStreamException {
    return m_XMLOutputFactory.createXMLStreamWriter(output);
  }//forOutput

  public final XMLStreamReader forInput(Reader input)
      throws XMLStreamException {
    return m_XMLInputFactory.createXMLStreamReader(input);
  }//forInput

  public final XMLStreamWriter forOutput(Writer output)
      throws XMLStreamException {
    return m_XMLOutputFactory.createXMLStreamWriter(output);
  }//forOutput

  public final BytesOutputStream leaseBytesOutputStream() {
    try {
      return (BytesOutputStream) m_BytesOutputStreams.borrowObject();
    } catch (Exception ex) {
      return new BytesOutputStream(4096);
    }
  }//leaseBytesOutputStream

  public final void releaseBytesOutputStream(BytesOutputStream bout) {
    try {
      m_BytesOutputStreams.returnObject(bout);
    } catch (Exception ex) {
    }
  }//releaseBytesOutputStream

  public final ByteBufferOutput leaseByteBufferOutput() {
    try {
      return (ByteBufferOutput) m_ByteBufferOutputs.borrowObject();
    } catch (Exception ex) {
      return new ByteBufferOutput(2048);
    }
  }//leaseByteBufferOutput

  public final void releaseByteBufferOutput(ByteBufferOutput bout) {
    try {
      m_ByteBufferOutputs.returnObject(bout);
    } catch (Exception ex) {
    }
  }//releaseByteBufferOutput

  public final ByteBufferInput leaseByteBufferInput() {
    try {
      return (ByteBufferInput) m_ByteBufferInputs.borrowObject();
    } catch (Exception ex) {
      return new ByteBufferInput();
    }
  }//leaseByteBufferInput

  public final void releaseByteBufferInput(ByteBufferInput bout) {
    try {
      m_ByteBufferInputs.returnObject(bout);
    } catch (Exception ex) {
    }
  }//releaseByteBufferInput

  private final static class BytesOutputStreamFactory
      extends BasePoolableObjectFactory {

    public Object makeObject() throws Exception {
      return new BytesOutputStream(4096);
    }//makeObject

    public void passivateObject(Object o) {
      ((BytesOutputStream) o).reset();
    }//passivateObject
  }//BytesOutputStreamFactory


  private final static class ByteBufferOutputFactory
      extends BasePoolableObjectFactory {

    public Object makeObject() throws Exception {
      return new ByteBufferOutput(2048);
    }//makeObject

    public void passivateObject(Object o) {
      ((ByteBufferOutput) o).reset();
    }//passivateObject

  }//ByteBufferOutputFactory

  private final static class ByteBufferInputFactory
      extends BasePoolableObjectFactory {

    public Object makeObject() throws Exception {
      return new ByteBufferInput();
    }//makeObject

    public void passivateObject(Object o) {
    }//passivateObject

  }//BytesBufferInputFactory


  public static void main(String[] args) {
    StreamFactory sf = new StreamFactory();
    sf.prepare();
    try {
      long[] times = new long[10000];
      long start = 0;
      StringReader str = new StringReader("<>");
      XMLStreamReader r = null;
      for (int i = 0; i < 10000; i++) {
        start = System.currentTimeMillis();
        r = sf.m_XMLInputFactory.createXMLStreamReader(str);
        times[i] = System.currentTimeMillis() - start;
      }
      start = 0;
      for (int i = 0; i < times.length; i++) {
        start += times[i];
      }
      System.out.println("Created Reader in average " + start / 10000 + " [ms].");

      start = 0;
      StringWriter strw = new StringWriter();
      XMLStreamWriter w = null;
      for (int i = 0; i < 10000; i++) {
        start = System.currentTimeMillis();
        w = sf.m_XMLOutputFactory.createXMLStreamWriter(strw);
        times[i] = System.currentTimeMillis() - start;
      }
      start = 0;
      for (int i = 0; i < times.length; i++) {
        start += times[i];
      }
      System.out.println("Created Writer in average " + start / 10000 + " [ms].");


    } catch (Exception ex) {
      Activator.log().error("main()", ex);
    }
  }
}//class StreamFactory

/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.io;

import org.apache.mina.common.ByteBuffer;

import java.io.DataInput;
import java.io.IOException;
import java.io.InputStream;
import java.io.UTFDataFormatException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;

/**
 * Provides an implementation of <tt>DataInput</tt> wrapping
 * a MINA <tt>org.apache.mina.common.ByteBuffer</tt>
 * <p/>
 * <tt>readLine()</tt> will throw an <tt>UnsupportedOperationException</tt>.
 *
 * @author Dieter Wimberger (coalevo)
 * @version @version@ (@date@)
 */
public class ByteBufferInput
    extends InputStream
    implements DataInput {

  private ByteBuffer m_Buffer;
  protected CharsetDecoder m_UTF8Decoder = Charset.forName("UTF-8").newDecoder();


  public ByteBufferInput() {

  }//ByteBufferInput

  public int read() throws IOException {
    if (m_Buffer.remaining() == 0) {
      return -1;
    }
    return readUnsignedByte();
  }//read

  public int read(byte[] b)
      throws IOException {
    int avail = m_Buffer.remaining();
    if (avail == 0) {
      return -1;
    }
    int read = Math.min(avail, b.length);
    readFully(b, 0, read);
    return read;
  }//read

  public long skip(long n)
      throws IOException {
    int avail = m_Buffer.remaining();
    if (avail == 0) {
      return -1;
    } else {
      int skip = Math.min(avail, (int) n);
      m_Buffer.skip(skip);
      return skip;
    }
  }//skip

  public void reset(ByteBuffer buf) {
    m_Buffer = buf;
  }//reset

  public void readFully(byte b[]) throws IOException {
    for (int i = 0; i < b.length; i++) {
      b[i] = m_Buffer.get();
    }
  }//readFully

  public void readFully(byte b[], int off, int len) throws IOException {
    for (int i = off; i < b.length + off; i++) {
      b[i] = m_Buffer.get();
    }
  }//readFully

  public int skipBytes(int n) throws IOException {
    m_Buffer.skip(n);
    return n;
  }//skipBytes

  public boolean readBoolean() throws IOException {
    return m_Buffer.get() == 1;
  }//readBoolean

  public byte readByte() throws IOException {
    return m_Buffer.get();
  }//readByte

  public int readUnsignedByte() throws IOException {
    return m_Buffer.get() & 0xFF;
  }//readUnsignedByte

  public short readShort() throws IOException {
    int ch1 = m_Buffer.get() & 0xff;
    int ch2 = m_Buffer.get() & 0xff;
    return (short) ((ch1 << 8) + (ch2));
  }//readShort

  public int readUnsignedShort() throws IOException {
    int ch1 = m_Buffer.get() & 0xff;
    int ch2 = m_Buffer.get() & 0xff;
    return (ch1 << 8) + (ch2);
  }//readUnsignedShort

  public char readChar() throws IOException {
    int ch1 = m_Buffer.get() & 0xff;
    int ch2 = m_Buffer.get() & 0xff;
    return (char) ((ch1 << 8) + (ch2));
  }//readChar

  public int readInt() throws IOException {
    int ch1 = m_Buffer.get() & 0xff;
    int ch2 = m_Buffer.get() & 0xff;
    int ch3 = m_Buffer.get() & 0xff;
    int ch4 = m_Buffer.get() & 0xff;

    return ((ch1 << 24) + (ch2 << 16) + (ch3 << 8) + (ch4));
  }//readInt

  public long readLong() throws IOException {
    return (((long) readInt()) << 32)
        + (((long) readInt()) & 0xffffffffL);
  }//readLong

  public final float readFloat() throws IOException {
    return Float.intBitsToFloat(readInt());
  }//readFloat

  public final double readDouble() throws IOException {
    return Double.longBitsToDouble(readLong());
  }//readDouble

  public String readLine() throws IOException {
    throw new UnsupportedOperationException();
  }//readLine

  public String readUTF() throws IOException {
    int length = readUnsignedShort();
    int c;
    int char2;
    int char3;
    int count = 0;

    StringBuilder buf = new StringBuilder(length * 3);
    while (count < length) {
      c = readByte();
      if (c > 0) {
        /* 0xxxxxxx*/
        count++;
        buf.append((char) c);
        continue;
      }
      c &= 0xff;
      switch (c >> 4) {
        case 12:
        case 13:

          /* 110x xxxx   10xx xxxx*/
          count += 2;

          if (count > length) {
            throw new UTFDataFormatException();
          }

          char2 = readByte();

          if ((char2 & 0xC0) != 0x80) {
            throw new UTFDataFormatException();
          }

          buf.append((char) (((c & 0x1F) << 6) | (char2 & 0x3F)));
          break;

        case 14:

          /* 1110 xxxx  10xx xxxx  10xx xxxx */
          count += 3;

          if (count > length) {
            throw new UTFDataFormatException();
          }

          char2 = readByte();
          char3 = readByte();

          if (((char2 & 0xC0) != 0x80)
              || ((char3 & 0xC0) != 0x80)) {
            throw new UTFDataFormatException();
          }

          buf.append(
              (char) ((c & 0x0F) << 12 | (char2 & 0x3F) << 6 | char3 & 0x3F));
          break;

        default:

          /* 10xx xxxx,  1111 xxxx */
          throw new UTFDataFormatException();
      }
    }

    return new String(buf);
  }//readUTF

}//class ByteBufferInput

/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.io;

import net.coalevo.empp.util.ByteBufferUtil;
import net.coalevo.empp.impl.Activator;
import net.coalevo.foundation.util.crypto.BlockCipherFactory;
import net.coalevo.foundation.util.crypto.MacFactory;
import net.coalevo.foundation.util.crypto.RandomUtil;
import org.apache.mina.common.ByteBuffer;
import org.apache.mina.common.IoFilterAdapter;
import org.apache.mina.common.IoFilterChain;
import org.apache.mina.common.IoSession;

import javax.crypto.Cipher;
import javax.crypto.Mac;
import java.io.IOException;
import java.security.MessageDigest;

/**
 * Implements a transport security filter.
 * <p/>
 *
 * @author Dieter Wimberger (coalevo)
 * @version @version@ (@date@)
 */
public class TSFilter
    extends IoFilterAdapter {

  //private static final Logger log = LoggerFactory.getLogger(TSFilter.class);
  private BlockCipherFactory m_CipherFactory = new BlockCipherFactory();

  //out
  public void messageReceived(NextFilter nextFilter,
                              IoSession session, Object message) throws Exception {

    Activator.log().debug("messageReceived()");
    if (isSessionSecure(session)) {
      ByteBuffer inBuffer = (ByteBuffer) message;
      TSPacketBuffer packet = getPacket(session);
      do {
        if (packet.add(inBuffer)) {
          //1. get message
          ByteBuffer msg =
              decryptAndCheck(
                  session,
                  packet.getIV(),
                  packet.getMAC(),
                  packet.getBuffer()
              );
          nextFilter.messageReceived(session, msg);
        }
      } while (inBuffer.remaining() > 0);
    } else {
      nextFilter.messageReceived(session, message);
    }
  }//messageReceived

  private ByteBuffer decryptAndCheck(IoSession session,
                                     byte[] IV, byte[] MAC, ByteBuffer inBuffer)
      throws IOException {

    try {
      //Activator.log().info("RIV =" + java.util.Arrays.toString(IV));
      //Activator.log().info("RMAC=" + java.util.Arrays.toString(MAC));
      //Activator.log().info("RREM=" + inBuffer.remaining());
      //Activator.log().info("RBUF=" + inBuffer.toString());
      Cipher c = m_CipherFactory.createDecryptionCypher(getCryptKey(session), IV);
      ByteBuffer outBuf = ByteBuffer.allocate(c.getOutputSize(inBuffer.remaining()));
      c.doFinal(inBuffer.buf(), outBuf.buf());
      outBuf.flip();
      Mac mac = getMac(session, true);
      mac.update(outBuf.buf());
      if (!MessageDigest.isEqual(mac.doFinal(), MAC)) {
        Activator.log().error("MAC Mismatch.");
        throw new IOException("MAC mismatch.");
      }
      mac.reset();
      outBuf.flip();
      return outBuf;
    } catch (Exception ex) {
      Activator.log().error("decryptAndCheck()", ex);
      throw new IOException(ex.getMessage());
    }
  }//decryptAndCheck


  //out
  public void filterWrite(NextFilter nextFilter,
                          IoSession session, WriteRequest writeRequest) throws IOException {
    //Activator.log().debug("filterWrite()\n======\n" + java.util.Arrays.toString(Thread.currentThread().getStackTrace()) + "\n======\n");

    if (isSessionSecure(session) && !isDontEncryptNext(session)) {
      try {
        ByteBuffer mbuf = (ByteBuffer) writeRequest.getMessage();
        //Make sure empty buffers are ignored:
        if (!mbuf.hasRemaining()) {
          nextFilter.filterWrite(session, writeRequest);
          Activator.log().debug("filterWrite()::EMPTY BUFFER SKIPPED");
          return;
        }

        //MAC
        Mac mac = getMac(session, false);
        mac.update(mbuf.buf());
        byte[] MAC = mac.doFinal();
        mac.reset();
        mbuf.rewind();
        //IV
        byte[] IV = RandomUtil.getBlock(16);
        //Activator.log().debug("Created IV = " + new String(Hex.encodeHex(IV)));
        //Prepare Cipher and corresponding outbuf
        Cipher c = m_CipherFactory.createEncryptionCypher(getCryptKey(session), IV);
        //Activator.log().debug(c.getBlockSize());
        int LEN = c.getOutputSize(mbuf.remaining());

        // Activator.log().debug("Create Cipher; ciphered LEN will be " + LEN + " Original length was " + mbuf.remaining());
        ByteBuffer outBuf = ByteBuffer.allocate(LEN + TS_HEADER_LENGTH);
        //write header
        outBuf.put(TS_VERSION_1);
        outBuf.putInt(LEN);
        outBuf.put(IV);
        outBuf.put(MAC);
        //add encrypted payload
        c.doFinal(mbuf.buf(), outBuf.buf());
        //Activator.log().info("WRIV =" + java.util.Arrays.toString(IV));
        //Activator.log().info("WMAC=" + java.util.Arrays.toString(MAC));


        //flip for writing and pass on
        outBuf.flip();
        nextFilter.filterWrite(session, new WriteRequest(outBuf, writeRequest.getFuture(), writeRequest.getDestination()));
      } catch (Exception ex) {
        throw new IOException(ex.getMessage());
      }
    } else {
      nextFilter.filterWrite(session, writeRequest);
    }
  }//filterWrite

  /**
   * *** Life cycle *****
   */

  public void onPreAdd(IoFilterChain parent, String name, NextFilter nextFilter) throws Exception {
    if (parent.contains(TSFilter.class)) {
      throw new IllegalStateException("A filter chain cannot contain more than" +
          " one Stream Compression filter.");
    }

    IoSession session = parent.getSession();
    session.setAttribute(TS_PACKETBUFFER, new TSPacketBuffer());
    session.setAttribute(TS_ENABLED, false);
  }//setAttributes


  public void sessionClosed(NextFilter nextFilter, IoSession session)
      throws Exception {
    Activator.log().debug("sessionClosed()");
    session.removeAttribute(TS_ENABLED);
    session.removeAttribute(TS_CRYPTKEY);
    session.removeAttribute(TS_MACKEY);
    session.removeAttribute(TS_MACIN);
    session.removeAttribute(TS_MACOUT);
    session.removeAttribute(TS_PACKETBUFFER);
    nextFilter.sessionClosed(session);
  }//sessionClosed

  public TSPacketBuffer getPacket(IoSession session) {
    Object o = session.getAttribute(TS_PACKETBUFFER);
    if (o != null) {
      return (TSPacketBuffer) o;
    } else {
      return null;
    }
  }//getPacket

  public byte[] getCryptKey(IoSession session) throws IOException {
    Object o = session.getAttribute(TS_CRYPTKEY);
    if (o != null) {
      Activator.log().debug("CryptKey" + java.util.Arrays.toString((byte[]) o));
      return (byte[]) o;
    } else {
      throw new IOException("Session crypt key not available.");
    }
  }//getCryptKey

  public Mac getMac(IoSession session, boolean in) throws IOException {
    Object o = session.getAttribute((in) ? TS_MACIN : TS_MACOUT);
    if (o != null) {
      return (Mac) o;
    } else {
      o = session.getAttribute(TS_MACKEY);
      if (o != null) {
        Mac mac = MacFactory.createMac((byte[]) o);
        session.setAttribute((in) ? TS_MACIN : TS_MACOUT, mac);
        return mac;
      } else {
        throw new IOException("Session crypt key not available.");
      }
    }
  }//getMac

  public boolean isSessionSecure(IoSession session) {
    return (Boolean) session.getAttribute(TS_ENABLED);
  }//isSessionSecure

  public boolean isDontEncryptNext(IoSession session) {
    Object o = session.removeAttribute(TSFilter.TS_DONT_ENCRYPT_NEXT);
    if (o == null) {
      return false;
    } else {
      return true;
    }
  }//isSessionSecure

  static class TSPacketBuffer {

    private byte m_Version;
    private int m_Length;
    private byte[] m_IV = new byte[16];  //Blocksize of AES
    private byte[] m_MAC = new byte[24]; //Size of Tiger Hash
    private ByteBuffer m_Buffer = ByteBuffer.allocate(1024).setAutoExpand(true); //1k

    public TSPacketBuffer() {
    }//TSPacketBuffer

    public void readHeader(ByteBuffer in) {
      m_Version = in.get();
      m_Length = in.getInt();
      in.get(m_IV);
      in.get(m_MAC);
    }//readHeader

    public byte getVersion() {
      return m_Version;
    }//getVersion

    public int getLength() {
      return m_Length;
    }//getLength

    public byte[] getIV() {
      return m_IV;
    }//getIV

    public byte[] getMAC() {
      return m_MAC;
    }//getMAC

    public boolean add(ByteBuffer in) {
      if (m_Buffer.position() == 0) {
        readHeader(in);
      }
      return (ByteBufferUtil.accumulateInto(m_Buffer, in, m_Length));
    }//add

    public ByteBuffer getBuffer() {
      ByteBuffer bbuf = m_Buffer.flip();
      m_Buffer = ByteBuffer.allocate(1024).setAutoExpand(true); //1k
      return bbuf;
    }//reset

  }//class

  public static final String TS_ENABLED = "net.coalevo.empp.io.ts_enabled";
  public static final String TS_DONT_ENCRYPT_NEXT = "net.coalevo.empp.io.ts_cleartext_next";
  public static final String TS_CRYPTKEY = "net.coalevo.empp.io.ts.cryptkey";
  public static final String TS_MACKEY = "net.coalevo.empp.io.ts.mackey";

  private static final String TS_MACIN = "net.coalevo.empp.io.ts.macin";
  private static final String TS_MACOUT = "net.coalevo.empp.io.ts.macout";
  private static final String TS_PACKETBUFFER = "net.coalevo.empp.io.ts.packetbuffer";
  private static final int TS_HEADER_LENGTH = 44;
  private static final byte TS_VERSION_1 = 1;
}//class TSFilter

/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.io;

import net.coalevo.empp.impl.Packets;
import net.coalevo.empp.impl.Activator;
import net.coalevo.empp.model.EMPPPacket;
import org.apache.mina.common.ByteBuffer;
import org.apache.mina.common.IoSession;
import org.apache.mina.filter.codec.ProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;

/**
 * Provides a <tt>ProtocolDecoder</tt> for EMPP.
 * <p/>
 *
 * @author Dieter Wimberger (coalevo)
 * @version @version@ (@date@)
 */
public class EMPPDecoder
    implements ProtocolDecoder {

  ////private static final Logger log = LoggerFactory.getLogger(EMPPDecoder.class);

  public void decode(IoSession ioSession, ByteBuffer byteBuffer, ProtocolDecoderOutput protocolDecoderOutput) throws Exception {
    ByteBufferInput in = getBuffer(ioSession);
    in.reset(byteBuffer);
    int kind = in.readInt();
    if (Packets.isPacketAvailable(kind)) {
      EMPPPacket p = Packets.leasePacket(kind);
      p.readFrom(in);

      //DEBUG:
      BytesOutputStream bout = new BytesOutputStream(2048);
      p.writeTo(bout);
      Activator.log().debug("-- IN ---> " + new String(bout.toByteArray()));

      protocolDecoderOutput.write(p);
    } else {
      throw new Exception("packet.kind.unsupported" + kind);
    }
  }//decode

  public void finishDecode(IoSession ioSession, ProtocolDecoderOutput protocolDecoderOutput) throws Exception {
    //To change body of implemented methods use File | Settings | File Templates.
  }//finishDecode

  private ByteBufferInput getBuffer(IoSession ios) {
    Object o = ios.getAttribute(INPUT_BUFFER);
    if (o != null) {
      return (ByteBufferInput) o;
    } else {
      ByteBufferInput in = new ByteBufferInput();
      ios.setAttribute(INPUT_BUFFER, in);
      return in;
    }
  }//getBuffer

  public void dispose(IoSession ioSession) throws Exception {
    ioSession.removeAttribute(INPUT_BUFFER);
  }//dispose

  public static final String INPUT_BUFFER = "net.coalevo.empp.server.inputbuffer";

}//class EMPPDecoder

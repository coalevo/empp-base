/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.io;

import org.apache.mina.common.ByteBuffer;

import java.io.DataOutput;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UTFDataFormatException;

/**
 * Provides an implementation of <tt>DataOutput</tt> wrapping
 * a MINA <tt>org.apache.mina.common.ByteBuffer</tt>
 * <p/>
 *
 * @author Dieter Wimberger (coalevo)
 * @version @version@ (@date@)
 */
public class ByteBufferOutput
    extends OutputStream
    implements DataOutput {

  private int m_PreAllocateLength = 1024; //1k
  protected ByteBuffer m_Buffer;

  public ByteBufferOutput(int preallocate) {
    m_PreAllocateLength = preallocate;
  }//ByteBufferOutput

  public ByteBufferOutput() {
    reset();
  }//ByteBufferOutput

  public ByteBuffer getBuffer() {
    return m_Buffer;
  }//getBuffer

  public void reset() {
    m_Buffer = ByteBuffer.allocate(m_PreAllocateLength);
    m_Buffer.setAutoExpand(true);
  }//reset

  public void write(int b) throws IOException {
    m_Buffer.put((byte) b);
  }//write

  public void write(byte b[]) throws IOException {
    if (b.length == 0) {
      return;
    }
    m_Buffer.put(b);
  }//write

  public void write(byte b[], int off, int len) throws IOException {
    m_Buffer.put(b, off, len);
  }//write

  public void writeBoolean(boolean v) throws IOException {
    m_Buffer.put((byte) (v ? 1 : 0));
  }//writeBoolean

  public void writeByte(int v) throws IOException {
    m_Buffer.put((byte) v);
  }//writeByte

  public void writeShort(int v) throws IOException {
    m_Buffer.put((byte) (v >>> 8));
    m_Buffer.put((byte) v);
  }//writeShort

  public void writeChar(int v) throws IOException {
    m_Buffer.put((byte) (0xff & (v >> 8)));
    m_Buffer.put((byte) (0xff & v));
  }//writeChar

  public final void writeInt(int v) {
    m_Buffer.put((byte) (v >>> 24));
    m_Buffer.put((byte) (v >>> 16));
    m_Buffer.put((byte) (v >>> 8));
    m_Buffer.put((byte) v);
  }//writeInt

  public final void writeLong(long v) {
    m_Buffer.put((byte) (0xff & (v >> 56)));
    m_Buffer.put((byte) (0xff & (v >> 48)));
    m_Buffer.put((byte) (0xff & (v >> 40)));
    m_Buffer.put((byte) (0xff & (v >> 32)));
    m_Buffer.put((byte) (0xff & (v >> 24)));
    m_Buffer.put((byte) (0xff & (v >> 16)));
    m_Buffer.put((byte) (0xff & (v >> 8)));
    m_Buffer.put((byte) (0xff & v));
  }//writeLong

  public final void writeFloat(float v) {
    writeInt(Float.floatToIntBits(v));
  }//writeFloat

  public final void writeDouble(double v) {
    writeLong(Double.doubleToLongBits(v));
  }//writeDouble

  public void writeBytes(String s) throws IOException {
    int len = s.length();
    for (int i = 0; i < len; i++) {
      int v = s.charAt(i);
      m_Buffer.put((byte) v);
    }
  }//writeBytes

  public void writeChars(String s) throws IOException {
    int len = s.length();
    for (int i = 0; i < len; i++) {
      int v = s.charAt(i);
      m_Buffer.put((byte) (v >>> 8));
      m_Buffer.put((byte) v);
    }
  }//writeChars

  public void writeUTF(String str) throws IOException {
    int pos = m_Buffer.position();
    m_Buffer.skip(2);
    int len = str.length();
    if (len > 0xffff) {
      throw new UTFDataFormatException();
    }
    int c = 0;
    for (int i = 0; i < len; i++) {
      c = str.charAt(i);
      if (c >= 0x0001 && c <= 0x007F) {
        m_Buffer.put((byte) c);
      } else if (c > 0x07FF) {
        m_Buffer.put((byte) (0xE0 | ((c >> 12) & 0x0F)));
        m_Buffer.put((byte) (0x80 | ((c >> 6) & 0x3F)));
        m_Buffer.put((byte) (0x80 | c & 0x3F));
      } else {
        m_Buffer.put((byte) (0xC0 | ((c >> 6) & 0x1F)));
        m_Buffer.put((byte) (0x80 | c & 0x3F));
      }
    }

    int bytecount = m_Buffer.position() - pos - 2;
    //System.out.println(bytecount);

    if (bytecount > 0xffff) {
      throw new UTFDataFormatException();
    }
    m_Buffer.put(pos++, (byte) (bytecount >>> 8));
    m_Buffer.put(pos, (byte) bytecount);
  }//writeUTF

}//class ByteBufferOutputStream

/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.io;

import net.coalevo.empp.impl.Activator;
import net.coalevo.empp.impl.Packets;
import net.coalevo.empp.model.EMPPPacket;
import org.apache.mina.common.IoSession;
import org.apache.mina.filter.codec.ProtocolEncoder;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;

/**
 * Provides a <tt>ProtocolEncoder</tt> for EMPP.
 * <p/>
 *
 * @author Dieter Wimberger (coalevo)
 * @version @version@ (@date@)
 */
public class EMPPEncoder
    implements ProtocolEncoder {


  //private static final Logger log = LoggerFactory.getLogger(EMPPEncoder.class);

  /**
   * Encodes a given {@link EMPPPacket} into the wire format
   * for transmission.
   * <p/>
   * The packet will be released after it was written, cloosing the
   * loop for recycling of packets with {@link Packets} pools.
   * </p>
   *
   * @param ioSession
   * @param object
   * @param protocolEncoderOutput
   * @throws Exception
   */
  public void encode(IoSession ioSession,
                     Object object,
                     ProtocolEncoderOutput protocolEncoderOutput)
      throws Exception {

    EMPPPacket packet = (EMPPPacket) object;

    try {
      //Activator.log().debug("encode()");

      //Activator.log().debug("encode()::have packet " + packet.toString());

      ByteBufferOutput out = getBuffer(ioSession);
      out.reset();
      //Activator.log().debug("encode()::have buffer " + out.toString());

      packet.writeTo(out);
      //Activator.log().debug("encode()::wrote buffer");

      //DEBUG
      BytesOutputStream bout = new BytesOutputStream(2048);
      ((EMPPPacket) object).writeTo(bout);
      Activator.log().debug("<-- OUT -- " + new String(bout.toByteArray()));

      protocolEncoderOutput.write(out.getBuffer().flip());
      //System.err.println("encode()::passed on buffer ");

    } finally {
      Packets.releasePacket(packet);
    }
  }//encode

  public void dispose(IoSession ioSession) throws Exception {
    ioSession.removeAttribute(OUTPUT_BUFFER);
  }//dispose

  private ByteBufferOutput getBuffer(IoSession ios) {
    Object o = ios.getAttribute(OUTPUT_BUFFER);
    if (o != null) {
      return (ByteBufferOutput) o;
    } else {
      ByteBufferOutput out = new ByteBufferOutput();
      ios.setAttribute(OUTPUT_BUFFER, out);
      return out;
    }
  }//getBuffer

  public static final String OUTPUT_BUFFER = "net.coalevo.empp.server.outputbuffer";

}//class EMPPEncoder
/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.util;

import org.apache.mina.common.ByteBuffer;

/**
 * Utility to manage ByteBuffer stuff that does'nt seem obvious at all.
 * <p/>
 *
 * @author Dieter Wimberger (coalevo)
 * @version @version@ (@date@)
 */
public class ByteBufferUtil {

  /**
   * Transfer a certain number of bytes from one buffer to the other.
   * <p/>
   * If the requested length is smaller then the remaining bytes in the
   * source, the remaining bytes will be transferred.
   * Source and destination buffers will be returned with their position
   * advanced by max(len,src.remaining).
   * </p>
   *
   * @param src
   * @param dest
   * @param len
   * @return num bytes transferred.
   */
  public static int transfer(ByteBuffer src, ByteBuffer dest, int len) {
    int nt = src.remaining();
    if (nt <= len) {
      dest.put(src);
      return nt;
    } else {
      int lim = src.limit();
      src.limit(src.position() + len);
      try {
        dest.put(src);
      } finally {
        src.limit(lim);
      }
      return len;
    }
  }//transfer

  public static boolean accumulateInto(ByteBuffer dest, int offset, int numbytes, ByteBuffer src) {
    int missing = numbytes - dest.position() + offset;
    int trans = transfer(src, dest, missing);
    return (missing - trans == 0);
  }//accumulateBuffer

  public static boolean accumulateInto(ByteBuffer dest, ByteBuffer src, int numbytes) {
    int missing = numbytes - dest.position();
    int trans = transfer(src, dest, missing);
    return (missing - trans == 0);
  }//accumulateBuffer


}//class ByteBufferUtil

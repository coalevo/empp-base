/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.model;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 * Extends {@link IQNamespaceExtension} to provide an interface for
 * a namespace extension that provides standardized data access.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface DataAccessIQExtension extends IQNamespaceExtension {

  /**
   * Reads the query.
   *
   * @param reader the reader.
   * @throws XMLStreamException if the stream is malformed or an IO error occurs.
   */
  public void readQuery(XMLStreamReader reader)
      throws XMLStreamException;

  /**
   * Returns the parameter with the given key.
   *
   * @param key a parameter key.
   * @return the value bound to the key as <tt>String</tt>.
   */
  public String getParameter(String key);

  /**
   * Tests if there is any parameter available for the query.
   *
   * @return true if available, false otherwise.
   */
  public boolean hasParameters();

  /**
   * Tests if this <tt>DataAccessIQExtension</tt> is of {@link #TYPE_GET}.
   *
   * @return true it of {@link #TYPE_GET}, false otherwise.
   */
  public boolean isGet();

  /**
   * Tests if this <tt>DataAccessIQExtension</tt> is of {@link #TYPE_DELETE}.
   *
   * @return true it of {@link #TYPE_DELETE}, false otherwise.
   */
  public boolean isDelete();

  /**
   * Tests if this <tt>DataAccessIQExtension</tt> is of {@link #TYPE_PUT}.
   *
   * @return true it of {@link #TYPE_PUT}, false otherwise.
   */
  public boolean isPut();

  /**
   * Defines the type that represents an access for retrieving data.
   */
  public static final String TYPE_GET = "get";

  /**
   * Defines the type that represents an access for adding or updating
   * data.
   */
  public static final String TYPE_PUT = "put";

  /**
   * Defines the type that represents an access for deleting data.
   */
  public static final String TYPE_DELETE = "delete";


}//interface DataAccessIQExtension

/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.model;

import net.coalevo.empp.util.Recyclable;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

/**
 * This interface defines the contract for a generic
 * namespace extension.
 * <p/>
 *
 * @author Dieter Wimberger (coalevo)
 * @version @version@ (@date@)
 */
public interface NamespaceExtension
    extends Recyclable {

  /**
   * Returns the specific namespace
   * for this <tt>NamespaceExtension</tt>.
   *
   * @return ns the namespace as <tt>String</tt>.
   */
  public String getNamespace();

  /**
   * Reads this <tt>NamespaceExtension</tt> from the given
   * <tt>XMLStreamReader</tt>.
   * <p/>
   * Note that an implementation <em>MUST</em>:
   * <ol>
   * <li>Assume that the cursor is at the start element of the
   * <tt>NamespaceExtension</tt></li>
   * <li>Return with the cursor at the end element of the <tt>NamespaceExtension</tt></li>
   * </ol>
   * </p>
   *
   * @param reader a <tt>XMLStreamReader</tt> instance.
   * @throws XMLStreamException          if a stream related error occurs.
   * @throws MalformedStructureException if the structure of the <tt>XMLPayload</tt>
   *                                     is not wellformed or invalid.
   * @throws MalformedContentException   if the content of the <tt>XMLPayload</tt>
   *                                     is not wellformed or invalid.
   */
  public void readExtensionSpecific(XMLStreamReader reader)
      throws XMLStreamException, MalformedStructureException, MalformedContentException;

  /**
   * Writes this <tt>NamespaceExtension</tt> to
   * the given <tt>XMLStreamWriter</tt>.
   *
   * @param writer a <tt>XMLStreamWriter</tt> instance.
   */
  public void writeExtensionSpecific(XMLStreamWriter writer)
      throws XMLStreamException;


}//interface NamespaceExtension

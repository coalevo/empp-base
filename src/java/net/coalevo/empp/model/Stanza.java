/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.model;

import net.coalevo.empp.util.Recyclable;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;


/**
 * This interface defines a payload that
 * is an EMPP <tt>Stanza</tt>.
 * <p/>
 *
 * @author Dieter Wimberger (coalevo)
 * @version @version@ (@date@)
 */
public interface Stanza
    extends Recyclable, EMPPPacket {

  /**
   * Returns the identifier of this
   * <tt>Stanza</tt>.
   *
   * @return the identifier as <tt>String</tt>.
   */
  public String getIdentifier();

  /**
   * Sets the identifier of this
   * <tt>Stanza</tt>.
   *
   * @param id an identfier as <tt>String</tt>.
   */
  public void setIdentifier(String id);

  /**
   * Sets the {@link Type} of this <tt>Stanza</tt>.
   *
   * @param t the  {@link Type} of this <tt>Stanza</tt>.
   */
  public void setType(Type t);

  /**
   * Returns the {@link Type} of this <tt>Stanza</tt>.
   *
   * @return the {@link Type} of this <tt>Stanza</tt>.
   */
  public Type getType();

  /**
   * Tests if this <tt>Stanza</tt> has a
   * {@link Type}.
   *
   * @return true if this <tt>Stanza</tt> has a
   *         {@link Type} false otherwise.
   */
  public boolean hasType();

  /**
   * Returns the {@link Language} of this <tt>Stanza</tt>.
   *
   * @return an {@link Language} instance.
   */
  public Language getLanguage();

  /**
   * Tests if this <tt>Stanza</tt> has a
   * {@link Language}.
   *
   * @return true if this <tt>Stanza</tt> has a
   *         {@link Language} false otherwise.
   */
  public boolean hasLanguage();

  public abstract void writeTo(XMLStreamWriter writer)
      throws XMLStreamException;

  public abstract void readFrom(XMLStreamReader reader)
      throws XMLStreamException, MalformedContentException, MalformedStructureException;


}//interface Stanza

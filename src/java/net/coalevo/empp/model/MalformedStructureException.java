/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.model;

/**
 * This class implements a <tt>MalformedStructureException</tt>.
 * <p/>
 * This exception is thrown when the structure of the XML payload of an
 * {@link net.coalevo.empp.model.EMPPPacket} is not well formed or invalid.
 * </p>
 * <p/>
 *
 * @author Dieter Wimberger (coalevo)
 * @version @version@ (@date@)
 */
public class MalformedStructureException
    extends Exception {

  public MalformedStructureException() {
  }//constructor

  public MalformedStructureException(String s) {
    super(s);
  }//constructor

  public MalformedStructureException(String s, Throwable throwable) {
    super(s, throwable);
  }//MalformedStructureException

}//class MalformedStructureException

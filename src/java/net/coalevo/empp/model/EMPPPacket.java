/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.model;


import net.coalevo.empp.util.Recyclable;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.model.MalformedAgentIdentifierException;

import javax.xml.stream.XMLStreamException;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * This interface defines a <tt>EMPPPacket</tt>.
 * <p/>
 *
 * @author Dieter Wimberger (coalevo)
 * @version @version@ (@date@)
 */
public interface EMPPPacket
    extends Recyclable {

  /**
   * Returns the {@link AgentIdentifier} that corresponds to
   * the receiver of this <tt>EMPPPacket</tt>.
   *
   * @return the {@link AgentIdentifier} instance of the sender.
   */
  public AgentIdentifier getTo();

  /**
   * Sets the {@link AgentIdentifier} that corresponds to
   * the receiver of this <tt>EMPPPacket</tt>.
   *
   * @param jid the {@link AgentIdentifier} instance of the sender.
   */
  public void setTo(AgentIdentifier jid);

  /**
   * Returns the {@link AgentIdentifier} that corresponds to
   * the sender of this <tt>EMPPPacket</tt>.
   *
   * @return the {@link AgentIdentifier} instance of the sender.
   */
  public AgentIdentifier getFrom();

  /**
   * Sets the {@link AgentIdentifier} that corresponds to
   * the sender of this <tt>EMPPPacket</tt>.
   *
   * @param jid the {@link AgentIdentifier} instance of the sender.
   */
  public void setFrom(AgentIdentifier jid);

  /**
   * Tests if this <tt>EMPPPacket</tt> has a corresponding
   * sender {@link AgentIdentifier}.
   *
   * @return true if this <tt>EMPPPacket</tt> has a
   *         sender false otherwise.
   */
  public boolean hasFrom();

  /**
   * Returns the session identifier of this
   * <tt>EMPPPacket</tt>.
   *
   * @return a session identifier.
   */
  public String getSessionIdentifier();

  /**
   * Sets the session identifier of this
   * <tt>EMPPPacket</tt>.
   *
   * @param id the identifier.
   */
  public void setSessionIdentifier(String id);

  /**
   * Returns the {@link PacketKind} of this <tt>EMPPPacket</tt>.
   *
   * @return the {@link PacketKind} of this <tt>EMPPPacket</tt>.
   */
  public PacketKind getKind();

  /*
   * Returns the {@link PacketKind} of this <tt>EMPPPacket</tt>.
   *
   * @param k the {@link PacketKind} of this <tt>EMPPPacket</tt>.
   public void setKind(PacketKind k);
   */

  /**
   * Prepares the payload of this <tt>EMPPPacket</tt>.
   */
  public void preparePayload();

  /**
   * Parses the payload of this <tt>EMPPPacket</tt>.
   *
   * @throws net.coalevo.empp.model.MalformedStructureException
   *          if the structure of the <tt>Payload</tt>
   *          is not wellformed or invalid.
   * @throws net.coalevo.empp.model.MalformedContentException
   *          if the content of the <tt>Payload</tt>
   *          is not wellformed or invalid.
   */
  public void parsePayload()
      throws XMLStreamException, MalformedContentException, MalformedStructureException;

  /**
   * Writes this <tt>EMPPPacket</tt> to the
   * given <tt>DataOutputStream</tt>.
   *
   * @param dout the <tt>DataOutput</tt> to write to.
   * @throws java.io.IOException if an I/O error occurs.
   */
  public void writeTo(DataOutput dout)
      throws IOException;

  /**
   * Reads this <tt>EMPPPacket</tt> from the given
   * <tt>DataInputStream</tt>.
   *
   * @param din the <tt>DataInput</tt> to read from.
   * @throws IOException if an I/O error occurs or the data
   *                     is invalid.
   * @throws MalformedAgentIdentifierException
   *                     if the from or to address is malformed.
   */
  public void readFrom(DataInput din)
      throws IOException, MalformedAgentIdentifierException;

  /**
   * Factory method to create a new instance of the
   * implementing class.
   *
   * @return an <tt>EMPPPacket</tt> instance.
   */
  public EMPPPacket createInstance();

}//interface EMPPPacket

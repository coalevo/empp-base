/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.model;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.util.Iterator;

/**
 * This interface defines an extensible {@link Stanza}
 * <p/>
 *
 * @author Dieter Wimberger (coalevo)
 * @version @version@ (@date@)
 * @see NamespaceExtension
 */
public interface ExtensibleStanza
    extends Stanza {

  /**
   * Adds a {@link NamespaceExtension} with a given namespace
   * to this <tt>ExtensibleStanza</tt>.
   * This will be done while the packet is read.
   *
   * @param namespace the namespace identifier.
   * @param se        a {@link NamespaceExtension} instance.
   */
  public void addExtension(String namespace, NamespaceExtension se);

  /**
   * Tests if some {@link NamespaceExtension} has been added.
   *
   * @return true if extensions have been added, false otherwise.
   */
  public boolean hasExtensions();

  /**
   * Iterator over all the namespaces of the {@link NamespaceExtension}s
   * added.
   *
   * @return an <tt>Iterator</tt> instance.
   */
  public Iterator<String> getExtensions();

  /**
   * Tests if this <tt>ExtensibleStanza</tt> has an extension with
   * the given namespace.
   *
   * @param namespace the namespace.
   * @return true if an extension with this namespace is available,
   *         false otherwise.
   */
  public boolean hasExtension(String namespace);

  /**
   * Returns a {@link NamespaceExtension} for a given namespace.
   *
   * @param namespace the namespace identifier.
   * @return a {@link NamespaceExtension} instance or null if no such extension
   *         has been added for the given namespace.
   */
  public NamespaceExtension getExtension(String namespace);

  /**
   * Writes all {@link NamespaceExtension} instances of this
   * <tt>ExtensibleStanza</tt> to the given <tt>XMLStreamWriter</tt>.
   *
   * @param writer a <tt>XMLStreamWriter</tt> instance.
   * @throws XMLStreamException if a stream or content related
   *                            error occurs.
   */
  public void writeExtensions(XMLStreamWriter writer)
      throws XMLStreamException;

}//interface ExtensibleStanza

/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.model;

/**
 * This interface defines a <tt>StanzaErrorCondition</tt>
 * to be associated with an {@link ErrorStanza}.
 * <p/>
 *
 * @author Dieter Wimberger (coalevo)
 * @version @version@ (@date@)
 * @see ErrorStanza
 */
public interface StanzaErrorCondition
    extends NamespaceExtension {

  /**
   * Returns the identifier of this <tt>StanzaErrorCondition</tt>.
   *
   * @return the identifier as <tt>StanzaErrorCondition</tt>.
   */
  public String getIdentifier();

  /**
   * Returns the associated {@link StanzaErrorType} of this
   * <tt>StanzaErrorCondition</tt>.
   *
   * @return the associated {@link StanzaErrorType}.
   */
  public StanzaErrorType getAssociatedErrorType();

}//interface StanzaErrorCondition

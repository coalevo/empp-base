/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.model;

import net.coalevo.empp.impl.InstantMessageTypes;

import java.util.Iterator;

/**
 * This interface defines the contract for
 * an EMPP <tt>InstantMessage</tt> {@link ExtensibleStanza}.
 *
 * @author Dieter Wimberger
 * @version @version@ (@date@)
 */
public interface InstantMessage
    extends ExtensibleStanza {

  /**
   * Sets the if this <tt>InstantMessage</tt> requires
   * confirmation.
   *
   * @param b true if confirmation required, false otherwise.
   */
  public void setConfirmationRequired(boolean b);

  /**
   * Tests if this <tt>InstantMessage</tt>
   * requires confirmation.
   *
   * @return true if confirmation is required, false otherwise.
   */
  public boolean isConfirmationRequired();

  /**
   * Sets the formatting hint.
   * This hint should indicate the formatting of the message body.
   * @param str the message body formatting hint.
   */
  public void setFormattingHint(String str);

  /**
   * Returns the formatting hint.
   * This hint should indicate the formatting of the message body.
   * @return the message body formatting hint.
   */
  public String getFormattingHint();

  /**
   * Tests if this message has a formatting hint.
   * @return true if there is a formatting hint, false otherwise.
   */
  public boolean hasFormattingHint();

  /**
   * Sets the subject of this <tt>InstantMessage</tt>
   * assuming it to be in the actual {@link Language}.
   *
   * @param subject the subject as <tt>String</tt>.
   * @see #getLanguage()
   */
  public void setSubject(String subject);

  /**
   * Sets the subject of this <tt>InstantMessage</tt>
   * in the given {@link Language}.
   *
   * @param l a {@link Language} instance.
   * @param s the subject as <tt>String</tt>.
   */
  public void setSubject(Language l, String s);

  /**
   * Removes the subject of this <tt>InstantMessage</tt>
   * in the given {@link Language}.
   *
   * @param l a {@link Language} instance.
   */
  public void removeSubject(Language l);

  /**
   * Returns the subject of this <tt>InstantMessage</tt>
   * in the given {@link Language}.
   *
   * @param l a {@link Language} instance.
   * @return the subject as <tt>String</tt> or null if it does not exist.
   */
  public String getSubject(Language l);

  /**
   * Returns the subject of this <tt>InstantMessage</tt>
   * in the default {@link Language}.
   *
   * @return the subject as <tt>String</tt> or null if it does not exist.
   */
  public String getSubject();

  /**
   * Returns an iterator over a set of <tt>Map.Entry</tt> instances,
   * holding the language (=key) the subject (=value)
   * of this <tt>InstantMessage</tt>.
   *
   * @return an <tt>Iterator</tt> over <tt>Map.Entry</tt> instances.
   */
  public Iterator getSubjects();

  /**
   * Tests if this <tt>InstantMessage</tt> has a subject
   * in the given {@link Language}.
   *
   * @param l a {@link Language} instance.
   * @return true if a subject in the given {@link Language} exists,
   *         false otherwise.
   */
  public boolean hasSubject(Language l);

  /**
   * Tests if this <tt>InstantMessage</tt> has a subject
   * in the default {@link Language}.
   *
   * @return true if a subject in the default {@link Language} exists,
   *         false otherwise.
   */
  public boolean hasSubject();

  /**
   * Tests if this <tt>InstantMessage</tt> has subjects in
   * any {@link Language}.
   *
   * @return true if subjects are available, false otherwise.
   */
  public boolean hasSubjects();

  /**
   * Sets the body of this <tt>InstantMessage</tt>
   * assuming it to be in the actual {@link Language}.
   *
   * @param body the body as <tt>String</tt>.
   * @see #getLanguage()
   */
  public void setBody(String body);

  /**
   * Sets the body of this <tt>InstantMessage</tt>
   * in the given {@link Language}.
   *
   * @param l a {@link Language} instance.
   * @param b the body as <tt>String</tt>.
   */
  public void setBody(Language l, String b);

  /**
   * Removes the body of this <tt>Instant<essage</tt>
   * in the given {@link Language}.
   *
   * @param l a {@link Language} instance.
   */
  public void removeBody(Language l);

  /**
   * Returns the body of this <tt>InstantMessage</tt>
   * in the given {@link Language}.
   *
   * @param l a {@link Language} instance.
   * @return the body as <tt>String</tt> or null if it does not exist.
   */
  public String getBody(Language l);

  /**
   * Returns the body of this <tt>InstantMessage</tt>
   * in the default {@link Language}.
   *
   * @return the body as <tt>String</tt> or null if it does not exist.
   */
  public String getBody();

  /**
   * Returns an iterator over a set of <tt>Map.Entry</tt> instances,
   * holding the language (=key) the body (=value)
   * of this <tt>InstantMessage</tt>.
   *
   * @return an <tt>Iterator</tt> over <tt>Map.Entry</tt> instances.
   */
  public Iterator getBodies();

  /**
   * Tests if this <tt>InstantMessage</tt> has a body
   * in the given {@link Language}.
   *
   * @param l a {@link Language} instance.
   * @return true if a body in the given {@link Language} exists,
   *         false otherwise.
   */
  public boolean hasBody(Language l);

  /**
   * Tests if this <tt>InstantMessage</tt> has a body
   * in the default {@link Language}.
   *
   * @return true if a body in the default {@link Language} exists,
   *         false otherwise.
   */
  public boolean hasBody();

  /**
   * Tests if this <tt>InstantMessage</tt> has bodies in
   * any {@link Language}.
   *
   * @return true if bodies are available, false otherwise.
   */
  public boolean hasBodies();


  /**
   * Returns the thread of this <tt>InstantMessage</tt>.
   *
   * @return the thread of this <tt>InstantMessage</tt> as <tt>String</tt>.
   */
  public String getThread();

  /**
   * Sets the thread of this <tt>InstantMessage</tt>.
   *
   * @param str the thread of this <tt>InstantMessage</tt> as <tt>String</tt>.
   */
  public void setThread(String str);

  /**
   * Tests if this <tt>InstantMessage</tt> has a thread.
   *
   * @return true if this <tt>InstantMessage</tt> has a thread set, false otherwise.
   */
  public boolean hasThread();

  /**
   * Tests if this <tt>InstantMessage</tt> thread value equals the
   * given value.
   *
   * @param thread a thread identifier value as <tt>String</tt>.
   * @return true if thread identifiers are equal, false otherwise.
   */
  public boolean isThread(String thread);

  /**
   * Returns the time of receipt for a {@link InstantMessageTypes#CONFIRMATION}.
   *
   * @return the receipt time as long in millis from UTC.
   */
  public long getReceptionTime();

  /**
   * Sets the time of receipt for a {@link InstantMessageTypes#CONFIRMATION}.
   *
   * @param l the receipt time as long in millis from UTC.
   */
  public void setReceptionTime(long l);

  /**
   * Returns the {@link MessageEventType} associated with a {@link InstantMessageTypes#EVENT}
   * type <tt>InstantMessage</tt>.
   *
   * @return a {@link MessageEventType} instance.
   */
  public MessageEventType getEventType();

  /**
   * Sets the {@link MessageEventType} associated with a {@link InstantMessageTypes#EVENT}
   * type <tt>InstantMessage</tt>.
   *
   * @param messageEventType {@link MessageEventType} instance.
   */
  public void setEventType(MessageEventType messageEventType);

}//InstantMessage
/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.model;

import net.coalevo.foundation.model.AgentIdentifier;

import java.util.Iterator;


/**
 * This interface defines the contract for
 * a <tt>Presence</tt> {@link ExtensibleStanza}.
 * <p/>
 *
 * @author Dieter Wimberger (coalevo)
 * @version @version@ (@date@)
 */
public interface Presence
    extends ExtensibleStanza {

  /**
   * Returns the description of this <tt>Presence</tt>.
   *
   * @return the description as <tt>String</tt>
   */
  public String getDescription();

  /**
   * Sets the description of this <tt>Presence</tt>.
   *
   * @param str the description as <tt>String</tt>.
   */
  public void setDescription(String str);

  /**
   * Tests if this <tt>Presence</tt> has a status
   * description.
   *
   * @return true if it has a description, false otherwise.
   */
  public boolean hasDescription();

  /**
   * Returns the reason given with a specific
   * subscription action ({@link #getSubscriptionAction()}).
   *
   * @return the reason as <tt>String</tt>.
   */
  public String getReason();

  /**
   * Set the reason for a specific subscription action
   * ({@link #setSubscriptionAction(SubscriptionAction)}).
   *
   * @param str the reason as <tt>String</tt>.
   */
  public void setReason(String str);

  /**
   * Tests if this <tt>Presence</tt> has a reason
   * given for a subscription action ({@link #getSubscriptionAction()}).
   *
   * @return true if a reason is available, false otherwise.
   */
  public boolean hasReason();

  /**
   * Sets the resources of this <tt>Presence</tt>.
   *
   * @param res the resources.
   */
  public void setResources(String[] res);

  /**
   * Returns the resources of this <tt>Presence</tt>.
   *
   * @return an array containing resources.
   */
  public String[] getResources();

  /**
   * Tests if this <tt>Presence</tt> has resources.
   *
   * @return true if it has, false otherwise.
   */
  public boolean hasResources();

  /**
   * Sets the {@link PresenceStatusType} of this <tt>Presence</tt>.
   *
   * @param pst the {@link PresenceStatusType} to be set.
   */
  public void setStatusType(PresenceStatusType pst);

  /**
   * Returns the {@link PresenceStatusType} of this <tt>Presence</tt>.
   *
   * @return a {@link PresenceStatusType}.
   */
  public PresenceStatusType getStatusType();

  /**
   * Tests if this <tt>Presence</tt> has a {@link PresenceStatusType}
   * set.
   *
   * @return true if set, false otherwise.
   * @see #setStatusType(PresenceStatusType)
   * @see #getStatusType()
   */
  public boolean hasStatusType();

  /**
   * Sets the {@link SubscriptionAction} for this <tt>Presence</tt>.
   *
   * @param action a {@link SubscriptionAction}.
   */
  public void setSubscriptionAction(SubscriptionAction action);

  /**
   * Return the {@link SubscriptionAction} of this <tt>Presence</tt>.
   *
   * @return a {@link SubscriptionAction}.
   */
  public SubscriptionAction getSubscriptionAction();

  /**
   * Returns the {@link PresencePrivacyAction}.
   *
   * @return true if a {@link PresencePrivacyAction} is set, false otherwise.
   */
  public boolean hasPrivacyAction();

  /**
   * Sets the {@link PresencePrivacyAction} for this <tt>Presence</tt>.
   *
   * @param action a {@link PresencePrivacyAction}.
   */
  public void setPrivacyAction(PresencePrivacyAction action);

  /**
   * Return the {@link PresencePrivacyAction} of this <tt>Presence</tt>.
   *
   * @return a {@link PresencePrivacyAction}.
   */
  public PresencePrivacyAction getPrivacyAction();

  /**
   * Returns the {@link SubscriptionAction}.
   *
   * @return true if a {@link SubscriptionAction} is set, false otherwise.
   */
  public boolean hasSubscriptionAction();

  /**
   * Adds an item to a list type subscription action.
   *
   * @param aid   the {@link AgentIdentifier}.
   * @param alias the alias.
   */
  public void addItem(AgentIdentifier aid, String alias);

  /**
   * Returns the items in the list if this is a
   * list type subscription action.
   *
   * @return the items as <tt>Iterator</tt> over {@link Presence.ListItem}
   *         instances.
   */
  public Iterator<ListItem> getItems();

  /**
   * Returns the item count of the list if this is a list
   * type subscription action.
   *
   * @return the number of list items as int.
   */
  public int getItemCount();

  /**
   * Defines a list item for list subscription action types.
   */
  public interface ListItem {

    /**
     * Returns the {@link AgentIdentifier} of this item.
     *
     * @return an {@link AgentIdentifier} instance.
     */
    public AgentIdentifier getIdentifier();

    /**
     * Returns the alias of this item.
     *
     * @return the alias as string.
     */
    public String getAlias();

  }//inner interface ListItem

}//interface Presence

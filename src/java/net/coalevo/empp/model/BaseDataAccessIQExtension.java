/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.model;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.util.HashMap;
import java.util.Map;

/**
 * Abstract class that provides the base for
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public abstract class BaseDataAccessIQExtension
    implements DataAccessIQExtension {

  private String m_Type;
  private Map<String, String> m_Params;

  public String getParameter(String key) {
    return m_Params.get(key);
  }//getParameter

  public boolean hasParameters() {
    return m_Params != null && m_Params.size() > 0;
  }//hasParameters

  public boolean isGet() {
    return TYPE_GET.equals(m_Type);
  }//isGet

  public boolean isDelete() {
    return TYPE_DELETE.equals(m_Type);
  }//isDelete

  public boolean isPut() {
    return TYPE_PUT.equals(m_Type);
  }//isPut

  public void readQuery(XMLStreamReader reader) throws XMLStreamException {
    //Query
    String name = reader.getLocalName();
    String nsuri = reader.getNamespaceURI();
    if (ELEMENT_QUERY.equals(name) && getNamespace().equals(nsuri)) {
      if (reader.getAttributeCount() > 0) {
        setType(reader.getAttributeValue("","type"));
      }
      readParams(reader);
      //TODO: check that we have end tag of query?
    }
  }//readQuery

  private void setType(String type) throws XMLStreamException {
    if (TYPE_GET.equals(type)) {
      m_Type = TYPE_GET;
    } else if (TYPE_PUT.equals(type)) {
      m_Type = TYPE_PUT;
    } else if (TYPE_DELETE.equals(type)) {
      m_Type = TYPE_DELETE;
    } else {
      throw new XMLStreamException("query type!");
    }
  }//setType

  private void readParams(XMLStreamReader reader)
      throws XMLStreamException {

    reader.nextTag();
    String name = reader.getLocalName();
    if (ELEMENT_PARAMS.equals(name)) {
      boolean done = false;
      do {
        reader.nextTag();
        name = reader.getLocalName();
        if (ELEMENT_PARAM.equals(name)) {
          m_Params = new HashMap<String, String>();
          if (reader.getAttributeCount() > 0) {
            addParam(reader.getAttributeValue(0), reader.getElementText());
          }
        } else if (ELEMENT_PARAMS.equals(name)) {
          done = true;
        } else {
          throw new XMLStreamException("Malformed.");
        }
      } while (!done);

    }
  }//readParams

  private void addParam(String name, String value) {
    m_Params.put(name, value);
  }//addParam

  public static final String ELEMENT_QUERY = "query";
  public static final String ELEMENT_PARAMS = "params";
  public static final String ELEMENT_PARAM = "param";
  public static final String ATTR_NAME = "name";

}//class BaseDataAccessIQExtension

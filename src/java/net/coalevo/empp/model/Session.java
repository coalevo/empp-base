/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.model;

import net.coalevo.empp.impl.SessionTypes;

/**
 * This interface defines the contract for an EMPP
 * <tt>Session</tt> {@link Stanza}.
 * <p>
 * These stanzas are used to negotiate an authenticated
 * session using the SRP v6a protocol. The authentication
 * token is the user password, which is never transported
 * over the network. The session key that results is used
 * to derive keys for the transport level encryption.
 * </p>
 * <p>
 * The client should always emit session packets of type
 * {@link SessionTypes#CLIENT}, the server of type
 * {@link SessionTypes#SERVER}.
 * </p>
 *
 * @author Dieter Wimberger (coalevo)
 * @version @version@ (@date@)
 */
public interface Session extends Stanza {

  /**
   * Sets the public ephemeral for the SRP negotiation.
   *
   * @param str the ephemeral as hex encoded String.
   */
  public void setPublicEphemeral(String str);

  /**
   * Returns the public ephemeral for the SRP negotiation.
   *
   * @return the public ephemeral as hex encoded String.
   */
  public String getPublicEphemeral();

  /**
   * Sets the salt as hex encoded String.
   * <p>
   * This method will only work if the <tt>Session</tt>
   * was set to be of type {@link SessionTypes#SERVER}.
   * </p>
   *
   * @param str the salt as hex encoded String.
   */
  public void setSalt(String str);

  /**
   * Returns the salt as hex encoded String.
   * <p>
   * This method will only work if the <tt>Session</tt>
   * was set to be of type {@link SessionTypes#SERVER}.
   * </p>
   *
   * @return the salt as hex encoded String.
   */
  public String getSalt();

  /**
   * Sets the match proof as BASE64 encoded String.
   *
   * @param str the match proof as BASE64 encoded String.
   */
  public void setMatchProof(String str);

  /**
   * Returns the match proof as BASe64 encoded String.
   *
   * @return the match proof as BASE64 encoded String.
   */
  public String getMatchProof();

  /**
   * Convenience method for the client to request encryption.
   *
   * @param b true if to be requested, false otherwise.
   */
  public void requestEncrypt(boolean b);

  /**
   * Convenience method for the server to test if the client
   * requested encryption.
   *
   * @return true if requested, false otherwise.
   */
  public boolean isEncryptRequested();

  /**
   * Convenience method for the server to respond to the
   * encryption request.
   *
   * @param b true if encryption will take place, false otherwise.
   */
  public void respondEncrypt(boolean b);

  /**
   * Convenience method for the client to test if the server
   * will encrypt at transport level.
   *
   * @return true if server will encrypt, false otherwise.
   */
  public boolean doEncrypt();

  /**
   * Returns the index for the key derivation function.
   * <p>
   * This is a random byte provided by the server.
   * </p>
   *
   * @return the KDF index byte.
   */
  public byte getKDFIndex();

  /**
   * Convenience method for the server to create and set
   * a index byte for the key derivation function.
   * <p>
   * Essentially this is a random generated byte that will be used
   * for the KDF in this session.
   * </p>
   */
  public void createKDFIndex();

  /**
   * Returns the key length for the symmetric encryption key.
   * <p>
   * The encryption that is used by default is the AES (Rjindael),
   * the supported key lengths are 128 and 256 bits.
   * </p>
   *
   * @return the key length.
   * @see #KEYLEN_128BIT
   * @see #KEYLEN_256BIT
   */
  public int getKeyLength();

  /**
   * Sets the key length for the symmetric encryption key.
   * <p>
   * The encryption that is used by default is the AES (Rjindael),
   * the supported key lengths are 128 and 256 bits.
   * </p>
   *
   * @param len the key length.
   * @see #KEYLEN_128BIT
   * @see #KEYLEN_256BIT
   */
  public void setKeyLength(int len);

  /**
   * Defines a key length of 128 bits.
   */
  public static final int KEYLEN_128BIT = 128;

  /**
   * Defines the key length of 256 bits.
   */
  public static final int KEYLEN_256BIT = 256;

}//interface Session

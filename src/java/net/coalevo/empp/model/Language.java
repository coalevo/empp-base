/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.model;

import java.util.Locale;

/**
 * This interface defines a convenience contract for
 * wrapping a <tt>Locale</tt> to EMPP needs.
 * <p/>
 *
 * @author Dieter Wimberger (coalevo)
 * @version @version@ (@date@)
 */
public interface Language {

  /**
   * Returns a <tt>Locale</tt> representing this <tt>Language</tt>.
   *
   * @return a <tt>Locale</tt> instance.
   */
  public Locale toLocale();

  /**
   * Returns the two letter ISO code for the language.
   *
   * @return the two letter ISO 639-1 code.
   */
  public String toValue();

}//interface Language

/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.model;


/**
 * This interface defines the contract for a {@link Stanza}
 * that represents an error.
 *
 * @author Dieter Wimberger (coalevo)
 * @version @version@ (@date@)
 */
public interface ErrorStanza
    extends Stanza {

  /**
   * Sets the packet kind of this <tt>ErrorStanza</tt>.
   * <p/>
   *
   * @param kind the {@link PacketKind} of this <tt>ErrorStanza</tt>.
   */
  public void setStanzaKind(PacketKind kind);

  /**
   * Returns the {@link StanzaErrorType} of this
   * <tt>ErrorStanza</tt>.
   * <p/>
   * All {@link StanzaErrorType} instances can be
   * validated through {@link net.coalevo.empp.impl.StanzaErrorTypes}.
   * </p>
   *
   * @return a {@link StanzaErrorType} instance.
   * @see net.coalevo.empp.impl.StanzaErrorTypes
   */
  public StanzaErrorType getErrorType();

  /**
   * Sets the {@link StanzaErrorType} of this
   * <tt>ErrorStanza</tt>.
   * <p/>
   * Valid {@link StanzaErrorType} instances should be
   * obtained from {@link net.coalevo.empp.impl.StanzaErrorTypes}.
   * </p>
   *
   * @param type a {@link StanzaErrorType} instance.
   * @see net.coalevo.empp.impl.StanzaErrorTypes
   */
  public void setErrorType(StanzaErrorType type);

  /**
   * Sets the {@link StanzaErrorType} of this
   * <tt>ErrorStanza</tt>.
   * <p/>
   * The {@link StanzaErrorType} instances should be
   * obtained from {@link net.coalevo.empp.impl.StanzaErrorTypes}.
   * </p>
   *
   * @param type a <tt>String</tt> identifier for a
   *             {@link StanzaErrorType} instance.
   * @see net.coalevo.empp.impl.StanzaErrorTypes
   */
  public void setErrorType(String type);

  /**
   * Returns a text (character data) that describes the error in more
   * detail.
   * <p/>
   * Implementations should assume that the text is in
   * the language returned by {@link #getLanguage()}.
   * </p>
   *
   * @return the text as <tt>String</tt>.
   * @see #getLanguage()
   */
  public String getText();

  /**
   * Sets a text (character data) that describes the error in more
   * detail.
   * <p/>
   * Implementations should assume that the text is in
   * the language returned by {@link #getLanguage()}.
   * </p>
   *
   * @param str the text as <tt>String</tt>.
   * @see #getLanguage()
   */
  public void setText(String str);

  /**
   * Tests if this <tt>ErrorStanza</tt> has a text describing
   * the error in more detail.
   *
   * @return true if text is available, false otherwise.
   */
  public boolean hasText();

  /**
   * Returns the {@link StanzaErrorCondition} of this <tt>ErrorStanza</tt>.
   *
   * @return a {@link StanzaErrorCondition} instance.
   */
  public StanzaErrorCondition getErrorCondition();

  /**
   * Sets the {@link StanzaErrorCondition} of this <tt>ErrorStanza</tt>.
   *
   * @param errorCondition a {@link StanzaErrorCondition} instance.
   */
  public void setErrorCondition(StanzaErrorCondition errorCondition);

  /**
   * Returns the {@link ApplicationErrorCondition} of this <tt>ErrorStanza</tt>.
   *
   * @return a {@link ApplicationErrorCondition} instance.
   */
  public ApplicationErrorCondition getApplicationCondition();

  /**
   * Sets the {@link ApplicationErrorCondition} of this <tt>ErrorStanza</tt>.
   *
   * @param applicationCondition a {@link ApplicationErrorCondition} instance.
   */
  public void setApplicationCondition(ApplicationErrorCondition applicationCondition);

  /**
   * Tests if this <tt>ErrorStanza</tt> has an associated {@link ApplicationErrorCondition}.
   *
   * @return true if an associated {@link ApplicationErrorCondition} is available, false otherwise.
   */
  public boolean hasApplicationCondition();

}//interface ErrorStanza

/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.impl;

import net.coalevo.empp.model.EMPPPacket;
import net.coalevo.empp.model.PacketKind;
import org.apache.commons.pool.BasePoolableObjectFactory;
import org.apache.commons.pool.impl.GenericObjectPool;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

/**
 * This class provides all valid
 * {@link net.coalevo.empp.model.EMPPPacket} implementations.
 * <p/>
 *
 * @author Dieter Wimberger (coalevo)
 * @version @version@ (@date@)
 */
public class Packets {

  ////private static final Logger log = LoggerFactory.getLogger(Packets.class);
  private static final GenericObjectPool.Config c_PoolConfig;
  private static final Map<PacketKind, GenericObjectPool> c_PacketPools;
  private static final Map<Integer, PacketKind> c_PacketKinds;

  static {

    // pool config
    c_PoolConfig = new GenericObjectPool.Config();
    //set for
    c_PoolConfig.maxActive = 50;
    c_PoolConfig.maxIdle = 25;
    c_PoolConfig.maxWait = -1;
    c_PoolConfig.testOnBorrow = false;
    c_PoolConfig.testOnReturn = false;
    //make it growable so it adapts
    c_PoolConfig.whenExhaustedAction = GenericObjectPool.WHEN_EXHAUSTED_GROW;
    c_PacketPools = new HashMap<PacketKind, GenericObjectPool>(5);

    c_PacketKinds = new HashMap<Integer, PacketKind>(25);

    //register base types
    registerPacket(new InstantMessageImpl());
    registerPacket(new PresenceImpl());
    registerPacket(new IQImpl());
    registerPacket(new SessionImpl());
  }//static initializer

  private Packets() {

  }//constructor

  public static final void registerPacket(EMPPPacket packet) {
    Activator.log().debug("registerPacket()::" + packet.toString());
    PacketKind pk = packet.getKind();
    if (!c_PacketPools.containsKey(pk)) {
      c_PacketPools.put(pk, new GenericObjectPool(new PacketFactory(packet), c_PoolConfig));
      c_PacketKinds.put(pk.get(), pk);
    }
  }//registerPacket

  public static final void unregisterPacket(PacketKind kind) {
    Activator.log().debug("unregisterPacket()::" + kind.toString());
    c_PacketPools.remove(kind);
    c_PacketKinds.remove(kind.get());
  }//unregisterPacket

  public static final boolean isPacketAvailable(int number)
      throws Exception {
    return c_PacketKinds.containsKey(number);
  }//leasePacket

  public static final EMPPPacket leasePacket(int number)
      throws Exception {
    PacketKind pk = c_PacketKinds.get(number);
    if (pk != null) {
      return leasePacket(pk);
    }
    throw new NoSuchElementException();
  }//leasePacket

  public static final EMPPPacket leasePacket(PacketKind pk) throws Exception {
    GenericObjectPool p = c_PacketPools.get(pk);
    if (p == null) {
      Activator.log().error("No packet implementation was registered for kind " + pk.toString());
      throw new IllegalArgumentException();
    } else {
      Object o = p.borrowObject();
      System.out.println("-----> Borrowing " + o.toString());
      return (EMPPPacket) o;
//      return (EMPPPacket) p.borrowObject();
    }
  }//leasePacket

  public static final void releasePacket(EMPPPacket packet) {
    try {
      PacketKind kind = packet.getKind();
      Object o = c_PacketPools.get(kind);
      if (o == null) {
        Activator.log().error("No packet implementation was registered for kind " + kind.toString());
        throw new IllegalStateException();
      } else {

        String st = java.util.Arrays.toString(Thread.currentThread().getStackTrace());
        System.out.println("-----> Returning " + packet.toString());
        System.out.println(st);
        ((GenericObjectPool) o).returnObject(packet);
      }
    } catch (Exception ex) {
      Activator.log().error("releasePacket()", ex);
    }
  }//releasePacket


  protected static class PacketFactory
      extends BasePoolableObjectFactory {

    private EMPPPacket m_Packet;

    public PacketFactory(EMPPPacket packet) {
      m_Packet = packet;
    }//constructor

    public Object makeObject() throws Exception {
      return m_Packet.createInstance();
    }//makeObject

    public void passivateObject(Object o) {
      ((EMPPPacket) o).recycle();
    }//passivateObject

  }//inner class PacketFactory

}//class Packets

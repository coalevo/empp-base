/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.impl;

import net.coalevo.empp.model.Language;

import java.util.Locale;

/**
 * This class implements a language attribute.
 * <p/>
 *
 * @author Dieter Wimberger (coalevo)
 * @version @version@ (@date@)
 */
class LanguageImpl
    implements Language, Comparable {

  protected Locale m_Locale;

  public LanguageImpl() {
    m_Locale = Locale.getDefault();
  }//constructor

  public LanguageImpl(String identifier) {
    m_Locale = new Locale(identifier);
  }//LanguageImpl

  public LanguageImpl(Locale l) {
    m_Locale = l;
  }//LanguageImpl

  /**
   * Returns the two letter ISO code for the language.
   *
   * @return the two letter ISO 639-1 code.
   */
  public String toValue() {
    return m_Locale.getLanguage();
  }//toValue

  public Locale toLocale() {
    return m_Locale;
  }//toLocale

  public int compareTo(Object o) {
    return toValue().compareTo(((Language) o).toValue());
  }//compareTo

}//class LanguageImpl

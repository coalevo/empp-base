/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.impl;

import net.coalevo.empp.model.ApplicationErrorCondition;
import net.coalevo.empp.model.StanzaErrorCondition;
import net.coalevo.empp.util.ClassBasePoolableFactory;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * This class defines all valid {@link net.coalevo.empp.model.StanzaErrorCondition}
 * instances.
 * <p/>
 *
 * @author Dieter Wimberger (coalevo)
 * @version @version@ (@date@)
 */
public class StanzaErrorConditions {

  private static Logger log = LoggerFactory.getLogger(StanzaErrorConditions.class);
  private static final Map c_AppErrorConditions;
  private static final GenericObjectPool.Config c_PoolConfig;

  static {
    // pool config
    c_PoolConfig = new GenericObjectPool.Config();
    //set for
    c_PoolConfig.maxActive = 50;
    c_PoolConfig.maxIdle = 25;
    c_PoolConfig.maxWait = -1;
    c_PoolConfig.testOnBorrow = false;
    c_PoolConfig.testOnReturn = false;
    //make it growable so it adapts
    c_PoolConfig.whenExhaustedAction = GenericObjectPool.WHEN_EXHAUSTED_GROW;
    c_AppErrorConditions = new HashMap();
  }//static initializer

  /**
   * Defines the condition <tt>bad-request</tt>:
   * the sender has sent XML that is malformed or
   * that cannot be processed (e.g., an IQ stanza that includes an
   * unrecognized value of the 'type' attribute); the associated error
   * type SHOULD be "modify".
   */
  public static final StanzaErrorCondition BAD_REQUEST =
      new StanzaErrorConditionImpl(EMPPTokens.STANZAERROR_COND_BADREQUEST,
          StanzaErrorTypes.MODIFY);

  /**
   * Defines the condition <tt>conflict</tt>:
   * access cannot be granted because an existing
   * resource or session exists with the same name or address; the
   * associated error type SHOULD be "cancel".
   */
  public static final StanzaErrorCondition CONFLICT =
      new StanzaErrorConditionImpl(EMPPTokens.STANZAERROR_COND_CONFLICT,
          StanzaErrorTypes.CANCEL);

  /**
   * Defines the condition <tt>feature-not-implemented</tt>:
   * the feature requested is not
   * implemented by the recipient or server and therefore cannot be
   * processed; the associated error type SHOULD be "cancel".
   */
  public static final StanzaErrorCondition FEATURE_NOT_IMPLEMENTED =
      new StanzaErrorConditionImpl(EMPPTokens.STANZAERROR_COND_FEATURENOTIMPLEMENTED,
          StanzaErrorTypes.CANCEL);
  /**
   * Defines the condition <tt>forbidden</tt>:
   * the requesting entity does not possess the
   * required permissions to perform the action; the associated error
   * type SHOULD be "auth".
   */
  public static final StanzaErrorCondition FORBIDDEN =
      new StanzaErrorConditionImpl(EMPPTokens.STANZAERROR_COND_FORBIDDEN,
          StanzaErrorTypes.AUTH);
  /**
   * Defines the condition <tt>gone</tt>:
   * the recipient or server can no longer be contacted at
   * this address (the error stanza MAY contain a new address in the
   * XML character data of the <gone/> element); the associated error
   * type SHOULD be "modify".
   */
  public static final StanzaErrorCondition GONE =
      new StanzaErrorConditionImpl(EMPPTokens.STANZAERROR_COND_GONE,
          StanzaErrorTypes.MODIFY);

  /**
   * Defines the condition <tt>internal-server-error</tt>:
   * the server could not process the stanza because of a
   * misconfiguration or an otherwise-undefined internal server
   * error; the associated error type SHOULD be "wait".
   */
  public static final StanzaErrorCondition INTERNAL_SERVER_ERROR =
      new StanzaErrorConditionImpl(EMPPTokens.STANZAERROR_COND_INTERNALSERVERERROR,
          StanzaErrorTypes.WAIT);

  /**
   * Defines the condition <tt>item-not-found</tt>:
   * the addressed JID or item requested cannot be
   * found; the associated error type SHOULD be "cancel".
   */
  public static final StanzaErrorCondition ITEM_NOT_FOUND =
      new StanzaErrorConditionImpl(EMPPTokens.STANZAERROR_COND_ITEMNOTFOUND,
          StanzaErrorTypes.CANCEL);

  /**
   * Defines the condition <tt>jid-malformed</tt>:
   * the sending entity has provided or
   * communicated an EMPP address (e.g., a value of the 'to' attribute)
   * or aspect thereof (e.g., a resource identifier) that does not
   * adhere to the syntax defined in Addressing Scheme (Section 3); the
   * associated error type SHOULD be "modify".
   */
  public static final StanzaErrorCondition JID_MALFORMED =
      new StanzaErrorConditionImpl(EMPPTokens.STANZAERROR_COND_JIDMALFORMED,
          StanzaErrorTypes.MODIFY);
  /**
   * Defines the condition <tt>not-acceptable</tt>:
   * the recipient or server understands the
   * request but is refusing to process it because it does not meet
   * criteria defined by the recipient or server (e.g., a local policy
   * regarding acceptable words in messages); the associated error type
   * SHOULD be "modify".
   */
  public static final StanzaErrorCondition NOT_ACCEPTABLE =
      new StanzaErrorConditionImpl(EMPPTokens.STANZAERROR_COND_NOTACCEPTABLE,
          StanzaErrorTypes.MODIFY);

  /**
   * Defines the condition <tt>not-allowed</tt>:
   * the recipient or server does not allow any
   * entity to perform the action; the associated error type SHOULD be
   * "cancel".
   */
  public static final StanzaErrorCondition NOT_ALLOWED =
      new StanzaErrorConditionImpl(EMPPTokens.STANZAERROR_COND_NOTALLOWED,
          StanzaErrorTypes.CANCEL);

  /**
   * Defines the condition <tt>not-authorized</tt>:
   * the sender must provide proper credentials
   * before being allowed to perform the action, or has provided
   * improper credentials; the associated error type SHOULD be "auth".
   */
  public static final StanzaErrorCondition NOT_AUTHORIZED =
      new StanzaErrorConditionImpl(EMPPTokens.STANZAERROR_COND_NOTAUTHORIZED,
          StanzaErrorTypes.AUTH);

  /**
   * Defines the condition <tt>payment-required</tt>:
   * the requesting entity is not authorized to
   * access the requested service because payment is required; the
   * associated error type SHOULD be "auth".
   */
  public static final StanzaErrorCondition PAYMENT_REQUIRED =
      new StanzaErrorConditionImpl(EMPPTokens.STANZAERROR_COND_PAYMENTREQUIRED,
          StanzaErrorTypes.AUTH);
  /**
   * Defines the condition <tt>recipient-unavailable</tt>:
   * the intended recipient is temporarily
   * unavailable; the associated error type SHOULD be "wait" (note: an
   * application MUST NOT return this error if doing so would provide
   * information about the intended recipient's network availability to
   * an entity that is not authorized to know such information).
   */
  public static final StanzaErrorCondition RECIPIENT_UNAVAILABLE =
      new StanzaErrorConditionImpl(EMPPTokens.STANZAERROR_COND_RECIPIENTUNAVAILABLE,
          StanzaErrorTypes.WAIT);
  /**
   * Defines the condition <tt>redirect</tt>:
   * the recipient or server is redirecting requests for
   * this information to another entity, usually temporarily (the error
   * stanza SHOULD contain the alternate address, which MUST be a valid
   * JID, in the XML character data of the <redirect/> element); the
   * associated error type SHOULD be "modify".
   */
  public static final StanzaErrorCondition REDIRECT =
      new StanzaErrorConditionImpl(EMPPTokens.STANZAERROR_COND_REDIRECT,
          StanzaErrorTypes.MODIFY);

  /**
   * Defines the condition <tt>registration-required</tt>:
   * the requesting entity is not authorized to access the
   * requested service because registration is
   * required; the associated error type SHOULD be "auth".
   */
  public static final StanzaErrorCondition REGISTRATION_REQUIRED =
      new StanzaErrorConditionImpl(EMPPTokens.STANZAERROR_COND_REGISTRATIONREQUIRED,
          StanzaErrorTypes.AUTH);

  /**
   * Defines the condition <tt>remote-server-not-found</tt>:
   * a remote server or service specified as part or all of the JID
   * of the intended recipient does not
   * exist; the associated error type SHOULD be "cancel".
   */
  public static final StanzaErrorCondition REMOTE_SERVER_NOT_FOUND =
      new StanzaErrorConditionImpl(EMPPTokens.STANZAERROR_COND_REMOTESERVERNOTFOUND,
          StanzaErrorTypes.CANCEL);

  /**
   * Defines the condition <tt>remote-server-timeout</tt>:
   * a remote server or service specified as part or all of
   * the JID of the intended recipient (or required
   * to fulfill a request) could not be contacted within a reasonable
   * amount of time; the associated error type SHOULD be "wait".
   */
  public static final StanzaErrorCondition REMOTE_SERVER_TIMEOUT =
      new StanzaErrorConditionImpl(EMPPTokens.STANZAERROR_COND_REMOTESERVERTIMEOUT,
          StanzaErrorTypes.WAIT);

  /**
   * Defines the condition <tt>resource-constraint</tt>:
   * the server or recipient lacks the system resources necessary
   * to service the request; the associated error
   * type SHOULD be "wait".
   */
  public static final StanzaErrorCondition RESOURCE_CONSTRAINT =
      new StanzaErrorConditionImpl(EMPPTokens.STANZAERROR_COND_RESOURCECONSTRAINT,
          StanzaErrorTypes.WAIT);

  /**
   * Defines the condition <tt>service-unavailable</tt>:
   * the server or recipient does not currently provide the
   * requested service; the associated error type
   * SHOULD be "cancel".
   */
  public static final StanzaErrorCondition SERVICE_UNAVAILABLE =
      new StanzaErrorConditionImpl(EMPPTokens.STANZAERROR_COND_SERVICEUNAVAILABLE,
          StanzaErrorTypes.CANCEL);

  /**
   * Defines the condition <tt>subscription-required</tt>:
   * the requesting entity is not authorized to access the requested
   * service because a subscription is required; the associated error
   * type SHOULD be "auth".
   */
  public static final StanzaErrorCondition SUBSCRIPTION_REQUIRED =
      new StanzaErrorConditionImpl(EMPPTokens.STANZAERROR_COND_SUBSCRIPTIONREQUIRED,
          StanzaErrorTypes.AUTH);
  /**
   * Defines the condition <tt>undefined-condition</tt>:
   * the error condition is not one of those
   * defined by the other conditions in this list; any error type may
   * be associated with this condition, and it SHOULD be used only in
   * conjunction with an application-specific condition.
   */
  public static final StanzaErrorCondition UNDEFINED_CONDITION =
      new StanzaErrorConditionImpl(EMPPTokens.STANZAERROR_COND_UNDEFINEDCONDITION,
          StanzaErrorTypes.ANY);

  /**
   * Defines the condition <tt>unexpected-request</tt>:
   * the recipient or server understood the request but was not
   * expecting it at this time (e.g., the request
   * was out of order); the associated error type SHOULD be "wait".
   */
  public static final StanzaErrorCondition UNEXPECTED_REQUEST =
      new StanzaErrorConditionImpl(EMPPTokens.STANZAERROR_COND_UNEXPECTEDREQUEST,
          StanzaErrorTypes.WAIT);

  protected static final StanzaErrorCondition[] CONDITIONS =
      new StanzaErrorCondition[]{BAD_REQUEST, CONFLICT, FEATURE_NOT_IMPLEMENTED,
          FORBIDDEN, GONE, INTERNAL_SERVER_ERROR, ITEM_NOT_FOUND, JID_MALFORMED,
          NOT_ACCEPTABLE, NOT_ALLOWED, NOT_AUTHORIZED, PAYMENT_REQUIRED, RECIPIENT_UNAVAILABLE,
          REDIRECT, REGISTRATION_REQUIRED, REMOTE_SERVER_NOT_FOUND, REMOTE_SERVER_TIMEOUT,
          RESOURCE_CONSTRAINT, SERVICE_UNAVAILABLE, SUBSCRIPTION_REQUIRED,
          UNDEFINED_CONDITION, UNEXPECTED_REQUEST};

  protected static final Set STANZAERROR_CONDITIONS =
      Collections.unmodifiableSet(new HashSet(Arrays.asList(CONDITIONS)));

  /**
   * Tests if a given {@link StanzaErrorCondition} is valid by specification.
   *
   * @param cond a {@link StanzaErrorCondition} instance.
   * @return true if valid, false otherwise.
   */
  public final static boolean isValidCondition(StanzaErrorCondition cond) {
    return STANZAERROR_CONDITIONS.contains(cond);
  }//isValid

  /**
   * Returns a {@link StanzaErrorCondition} for the given identifier.
   *
   * @param identifier for a {@link StanzaErrorCondition}
   * @return a {@link StanzaErrorCondition} instance.
   */
  public final static StanzaErrorCondition getCondition(String identifier) {
    for (Object aSTANZAERROR_CONDITIONS : STANZAERROR_CONDITIONS) {
      StanzaErrorCondition cond = (StanzaErrorCondition) aSTANZAERROR_CONDITIONS;
      if (cond.getIdentifier().equals(identifier)) {
        return cond;
      }
    }
    throw new NoSuchElementException();
  }//getCondition

  /**
   * Register a {@link ApplicationErrorCondition} implementation.
   *
   * @param namespace of the {@link ApplicationErrorCondition}.
   * @param cl        the class implementing a {@link ApplicationErrorCondition}.
   */
  public static final void registerAppErrorCondition(String namespace, Class cl) {
    try {
      if (!(cl.newInstance() instanceof ApplicationErrorCondition)) {
        throw new IllegalArgumentException();
      }
    } catch (Exception ex) {
      Activator.log().error("registerAppErrorCondition(String,Class)::" + namespace + "::" + cl.toString() + ex);
      throw new IllegalArgumentException();
    }
    c_AppErrorConditions.put(namespace, new GenericObjectPool(new ClassBasePoolableFactory(cl), c_PoolConfig));
  }//registerAppErrorCondition

  /**
   * Lease a {@link ApplicationErrorCondition} instance for the given namespace.
   *
   * @param namespace of the {@link ApplicationErrorCondition} to be leased.
   * @return a {@link ApplicationErrorCondition} instance.
   * @throws Exception if a {@link ApplicationErrorCondition} for the given namespace
   *                   does not exist or cannot be borrowed from the corresponding pool.
   */
  public static final ApplicationErrorCondition leaseAppErrorCondition(String namespace)
      throws Exception {
    Object o = c_AppErrorConditions.get(namespace);
    if (o == null) {
      Activator.log().error("No ApplicationErrorCondition implementation was registered for namespace " + namespace);
      throw new IllegalArgumentException();
    } else {
      return (ApplicationErrorCondition) ((GenericObjectPool) o).borrowObject();
    }
  }//leaseAppErrorCondition

  /**
   * Tests if an {@link ApplicationErrorCondition} instance is
   * available for the given namespace.
   *
   * @param namespace of a possible {@link ApplicationErrorCondition}.
   * @return true if an {@link ApplicationErrorCondition} for the given namespace
   *         does exist, false otherwise.
   */
  public static final boolean existsAppErrorCondition(String namespace) {
    return c_AppErrorConditions.containsKey(namespace);
  }//existsAppErrorCondition

  /**
   * Release a {@link ApplicationErrorCondition} instance.
   *
   * @param apperrcond a formerly leased {@link ApplicationErrorCondition} instance.
   * @throws Exception if a namespace implementation for the {@link ApplicationErrorCondition}
   *                   does not exist or the instance cannot be returned to the corresponding pool.
   */
  public static final void releaseAppErrorCondition(ApplicationErrorCondition apperrcond)
      throws Exception {
    String namespace = apperrcond.getNamespace();
    Object o = c_AppErrorConditions.get(namespace);
    if (o == null) {
      Activator.log().error("No ApplicationErrorCondition implementation was registered for namespace " + namespace);
      throw new IllegalStateException();
    } else {
      ((GenericObjectPool) o).returnObject(apperrcond);
    }
  }//releaseAppErrorCondition


}//class StanzaErrorConditions

/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.impl;

import net.coalevo.empp.io.BytesOutputStream;
import net.coalevo.empp.model.*;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.util.crypto.RandomUtil;
import net.coalevo.foundation.util.crypto.UUIDGenerator;
import net.coalevo.foundation.util.srp.ClientSRP;
import net.coalevo.foundation.util.srp.ServerSRP;
import net.coalevo.foundation.util.srp.StoreEntry;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

/**
 * Provides an implementation of a @link Session}.
 * <p/>
 *
 * @author Dieter Wimberger (coalevo)
 * @version @version@ (@date@)
 */
class SessionImpl
    extends BaseStanza implements Session {

  //private static final Logger log = LoggerFactory.getLogger(SessionImpl.class);
  private String m_PublicEphemeral;
  private String m_Salt;
  private String m_MatchProof;
  private String m_Encrypt;
  private byte m_Index;
  private int m_KeyLength = KEYLEN_128BIT;

  public SessionImpl() {
    super(PacketKinds.SESSION);
  }//constructor

  public void setType(Type type) {
    if (SessionTypes.isValid(type)) {
      m_Type = type;
    } else {
      throw new IllegalArgumentException();
    }
  }//setType

  public void setType(String tid) {
    m_Type = SessionTypes.get(tid);
  }//setType

  public String getIdentifier() {
    return getSessionIdentifier();
  }//getIdentifier

  public void setIdentifier(String id) {
    setSessionIdentifier(id);
  }//setIdentifier

  public boolean hasEncrypt() {
    return m_Encrypt != null;
  }//willEncrypt

  public void requestEncrypt(boolean b) {
    if (b) {
      m_Encrypt = EMPPTokens.NEGOTIATE_DO;
    } else {
      m_Encrypt = EMPPTokens.NEGOTIATE_DONT;
    }
  }//requestEncrypt

  public boolean isEncryptRequested() {
    return EMPPTokens.NEGOTIATE_DO.equals(m_Encrypt);
  }//isEncryptRequested

  public void respondEncrypt(boolean b) {
    if (b) {
      m_Encrypt = EMPPTokens.NEGOTIATE_WILL;
    } else {
      m_Encrypt = EMPPTokens.NEGOTIATE_WONT;
    }
  }//respondEncrypt

  public boolean doEncrypt() {
    return EMPPTokens.NEGOTIATE_WILL.equals(m_Encrypt);
  }//doEncrypt

  public String getSalt() {
    return m_Salt;
  }//getSalt

  public void setSalt(String salt) {
    m_Salt = salt;
  }//setSalt

  public boolean hasSalt() {
    return m_Salt != null;
  }//hasSalt

  public String getMatchProof() {
    return m_MatchProof;
  }//getMatchProof

  public void setMatchProof(String matchProof) {
    m_MatchProof = matchProof;
    m_PublicEphemeral = null;
    m_Salt = null;
  }//setMatchProof

  public boolean hasMatchProof() {
    return m_MatchProof != null;
  }//hasMatchProof

  public String getPublicEphemeral() {
    return m_PublicEphemeral;
  }//getPublicEphemeral

  public byte getKDFIndex() {
    return m_Index;
  }//getKDFIndex

  public void createKDFIndex() {
    m_Index = RandomUtil.nextIndex();
  }//createKDFIndex

  public int getKeyLength() {
    return m_KeyLength;
  }//getKeyLength

  public void setKeyLength(int len) {
    if (len == KEYLEN_128BIT || len == KEYLEN_256BIT) {
      m_KeyLength = len;
    } else {
      m_KeyLength = KEYLEN_256BIT;
    }
  }//setKeyLength

  public void setPublicEphemeral(String publicEphemeral) {
    m_PublicEphemeral = publicEphemeral;
  }//setPublicEphemeral

  public boolean hasPublicEphemeral() {
    return m_PublicEphemeral != null;
  }//hasPublicEphemeral

  public void recycle() {
    super.recycle();
    setSessionIdentifier(UUIDGenerator.getUID());
    m_PublicEphemeral = null;
    m_Salt = null;
    m_MatchProof = null;
    m_Encrypt = null;
  }//reset

  public void writeTo(XMLStreamWriter writer) throws XMLStreamException {

    //1. session element
    writer.writeStartElement(EMPPTokens.ELEMENT_SESSION);
    //2. Stanza attributes
    writeStanzaAttributes(writer);

    if (isType(SessionTypes.SERVER)) {
      // public ephemeral
      if (hasSalt()) {
        //System.out.println("Wrote salt =" + m_Salt);
        writer.writeStartElement(EMPPTokens.ELEMENT_SALT);
        writer.writeCharacters(m_Salt);
        writer.writeEndElement();
      }
    }

    // public ephemeral
    if (hasPublicEphemeral()) {
      //System.out.println("Wrote public ephemeral =" + m_PublicEphemeral);
      writer.writeStartElement(
          (isType(SessionTypes.CLIENT)) ? EMPPTokens.ELEMENT_CLIENT_EPHEMERAL : EMPPTokens.ELEMENT_SERVER_EPHEMERAL);
      writer.writeCharacters(m_PublicEphemeral);
      writer.writeEndElement();
    }
    //match proof
    if (hasMatchProof()) {
      //Activator.log().debug("Wrote proof =" + m_MatchProof);
      writer.writeStartElement(EMPPTokens.ELEMENT_MATCHPROOF);
      writer.writeCharacters(m_MatchProof);
      writer.writeEndElement();
    }
    //ecrypt
    if (hasEncrypt()) {
      //Activator.log().debug("Wrote encrypt =" + m_Encrypt);
      writer.writeStartElement(EMPPTokens.ELEMENT_ENCRYPT);
      if (isType(SessionTypes.SERVER) && EMPPTokens.NEGOTIATE_WILL.equals(m_Encrypt)) {
        writer.writeAttribute(EMPPTokens.ATTR_INDEX, Byte.toString(m_Index));
      }
      if (isType(SessionTypes.CLIENT) && EMPPTokens.NEGOTIATE_DO.equals(m_Encrypt)) {
        writer.writeAttribute(EMPPTokens.ATTR_KEYLENGTH, Integer.toString(m_KeyLength));
      }
      writer.writeCharacters(m_Encrypt);
      writer.writeEndElement();
    }

    //end element
    writer.writeEndElement();
    writer.flush();

  }//writeTo

  public void readFrom(XMLStreamReader reader) throws XMLStreamException, MalformedStructureException,
      MalformedContentException {

    //first tag
    reader.nextTag();
    //read stanza common attributes
    readStanzaAttributes(reader);

    boolean done = false;
    do {
      reader.nextTag();
      String name = reader.getLocalName();
      //System.out.println("Got Tag::" + name);
      if (isType(SessionTypes.SERVER)) {
        //The first token in a server message might be <s> or <M>
        if (EMPPTokens.ELEMENT_SERVER_EPHEMERAL.equals(name)) {
          m_PublicEphemeral = reader.getElementText();
          //System.out.println("Parsed B=" + m_PublicEphemeral);
        } else if (EMPPTokens.ELEMENT_ENCRYPT.equals(name)) {
          if (reader.getAttributeCount() > 0) {
            m_Index = Byte.parseByte(reader.getAttributeValue("", EMPPTokens.ATTR_INDEX));
          }
          String neg = reader.getElementText();
          if (neg.equals(EMPPTokens.NEGOTIATE_WILL)) {
            respondEncrypt(true);
          } else {
            respondEncrypt(false);
          }
          //Activator.log().debug("Parsed Encrypt=" + m_Encrypt);
        } else if (EMPPTokens.ELEMENT_SALT.equals(name)) {
          m_Salt = reader.getElementText();
          //System.out.println("Parsed Salt=" + m_Salt);
        } else if (EMPPTokens.ELEMENT_MATCHPROOF.equals(name)) {
          m_MatchProof = reader.getElementText();
          //Activator.log().debug("Parsed Key Match Proof=" + m_MatchProof);
        } else {
          done = true;
        }
      } else if (isType(SessionTypes.CLIENT)) {
        //The first token in a server message might be <s> or <M>
        if (EMPPTokens.ELEMENT_CLIENT_EPHEMERAL.equals(name)) {
          m_PublicEphemeral = reader.getElementText();
          //System.out.println("Parsed A=" + m_PublicEphemeral);
        } else if (EMPPTokens.ELEMENT_ENCRYPT.equals(name)) {
          if (reader.getAttributeCount() > 0) {
            m_KeyLength = Integer.parseInt(reader.getAttributeValue("", EMPPTokens.ATTR_KEYLENGTH));
          }
          String neg = reader.getElementText();
          if (neg.equals(EMPPTokens.NEGOTIATE_DO)) {
            requestEncrypt(true);
          } else {
            requestEncrypt(false);
          }

          //Activator.log().debug("Parsed Encrypt=" + m_Encrypt);
        } else if (EMPPTokens.ELEMENT_MATCHPROOF.equals(name)) {
          m_MatchProof = reader.getElementText();
          //Activator.log().debug("Parsed Key Match Proof=" + m_MatchProof);
        } else {
          done = true;
        }
      } else {
        throw new MalformedStructureException();
      }
    } while (!done);

  }//readFrom

  public EMPPPacket createInstance() {
    return new SessionImpl();
  }//createInstance


  public static void main(String[] args) {
    try {
      //org.apache.log4j.BasicConfigurator.configure();
      Activator.log().debug("main()");
      ClientSRP cSRP = new ClientSRP("username", "password");
      StoreEntry se = StoreEntry.createStoreEntry("username", "password");
      BytesOutputStream bout = new BytesOutputStream(2048);
      //XMLOutputFactory factory = XMLOutputFactory.newInstance();
      //XMLStreamWriter writer = factory.createXMLStreamWriter(bout);
      SessionImpl s = new SessionImpl();
      AgentIdentifier from = c_AIDCache.get("wimpi");
      AgentIdentifier to = c_AIDCache.get("");
      s.setFrom(from);
      s.setTo(to);
      s.setPublicEphemeral(cSRP.getPublicEphemeral());
      s.setType(SessionTypes.CLIENT);
      s.preparePayload();
      s.writeTo(bout);
      System.out.println(new String(bout.toByteArray()));

      //server received
      ServerSRP sSRP = new ServerSRP(se);
      sSRP.received(s.getPublicEphemeral());

      //Server reply
      bout.reset();
      s.recycle();
      s.setType(SessionTypes.SERVER);
      s.setFrom(to);
      s.setTo(from);
      s.setPublicEphemeral(sSRP.getPublicEphemeral());
      s.setSalt(sSRP.getSalt());
      s.preparePayload();
      s.writeTo(bout);
      System.out.println(new String(bout.toByteArray()));

      //client received
      cSRP.received(s.getSalt(), s.getPublicEphemeral());

      //Client reply
      bout.reset();
      s.recycle();

      s.setType(SessionTypes.CLIENT);
      s.setFrom(from);
      s.setTo(to);
      s.setPublicEphemeral(null);
      s.setSalt(null);
      s.setMatchProof(cSRP.getKeyMatchProof());
      s.requestEncrypt(true);
      s.preparePayload();
      s.writeTo(bout);
      System.out.println(new String(bout.toByteArray()));

      //server received
      if (sSRP.checkClientKeyProof(s.getMatchProof())) {
        //server reply
        bout.reset();
        s.recycle();

        s.setType(SessionTypes.SERVER);
        s.setFrom(to);
        s.setTo(from);
        s.setPublicEphemeral(null);
        s.setSalt(null);
        s.setMatchProof(sSRP.getKeyMatchProof());
        s.respondEncrypt(true);
        s.preparePayload();
        s.writeTo(bout);
        System.out.println(new String(bout.toByteArray()));
      }

    } catch (Exception ex) {
      Activator.log().error("main()", ex);
    }
  }

}//class PresenceImpl

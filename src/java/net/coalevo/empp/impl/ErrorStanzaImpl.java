/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.impl;

import net.coalevo.empp.model.*;

import javax.xml.stream.*;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.NoSuchElementException;

/**
 * This class implements {@link ErrorStanza}.
 * <p/>
 *
 * @author Dieter Wimberger (coalevo)
 * @version @version@ (@date@)
 */
class ErrorStanzaImpl
    extends BaseStanza
    implements ErrorStanza {

  protected PacketKind m_StanzaKind;
  protected StanzaErrorType m_ErrorType;
  protected String m_Text;
  protected StanzaErrorCondition m_ErrorCondition;
  protected ApplicationErrorCondition m_ApplicationCondition;

  public ErrorStanzaImpl() {
    super(new PacketKindImpl(EMPPTokens.PACKET_ID_ERROR, EMPPTokens.PACKET_KIND_ERROR));
    m_Language = Languages.DEFAULT;
    m_ErrorCondition = StanzaErrorConditions.UNDEFINED_CONDITION;
    m_ErrorType = m_ErrorCondition.getAssociatedErrorType();
  }//constructor

  public ErrorStanzaImpl(PacketKind kind) {
    this();
    setStanzaKind(kind);
  }//ErrorStanzaImpl

  public void setStanzaKind(PacketKind kind) {
    m_StanzaKind = kind;
    if (PacketKinds.INSTANT_MESSAGE.equals(kind)) {
      m_Type = InstantMessageTypes.ERROR;
    } else if (PacketKinds.PRESENCE.equals(kind)) {
      m_Type = PresenceTypes.ERROR;
    } else if (PacketKinds.IQ.equals(kind)) {
      m_Type = IQTypes.ERROR;
    }
  }//setStanza

  public void setType(Type type) {
    setType(type.getIdentifier());
  }//setKind

  public void setType(String type) {
    if (!EMPPTokens.STANZA_TYPE_ERROR.equals(type)) {
      throw new IllegalArgumentException();
    }
  }//setKind

  public StanzaErrorType getErrorType() {
    return m_ErrorType;
  }//StanzaErrorType

  public void setErrorType(StanzaErrorType type) {
    if (StanzaErrorTypes.isValidType(type)) {
      m_ErrorType = (StanzaErrorType) type;
    } else {
      throw new IllegalArgumentException();
    }
  }//setErrorType

  public void setErrorType(String tid) {
    m_ErrorType = StanzaErrorTypes.get(tid);
  }//setKind

  public String getText() {
    return m_Text;
  }//getText

  public void setText(String str) {
    m_Text = str;
  }//setText

  public boolean hasText() {
    return m_Text != null;
  }//hasText

  public StanzaErrorCondition getErrorCondition() {
    return m_ErrorCondition;
  }//getErrorCondition

  public void setErrorCondition(StanzaErrorCondition errorCondition) {
    m_ErrorCondition = errorCondition;
    setErrorType(m_ErrorCondition.getAssociatedErrorType());
  }//setErrorCondition

  public ApplicationErrorCondition getApplicationCondition() {
    return m_ApplicationCondition;
  }//getApplicationCondition

  public void setApplicationCondition(ApplicationErrorCondition applicationCondition) {
    m_ApplicationCondition = applicationCondition;
  }//setApplicationCondition

  public boolean hasApplicationCondition() {
    return m_ApplicationCondition != null;
  }//hasApplicationCondition

  public void recycle() {
    super.recycle();
    m_StanzaKind = null;
    m_Text = null;
    m_ErrorCondition = null;
    try {
      StanzaErrorConditions.releaseAppErrorCondition(m_ApplicationCondition);
    } catch (Exception ex) {
      Activator.log().error("recycle()", ex);
    }
  }//recycle

  public void writeTo(XMLStreamWriter writer)
      throws XMLStreamException {

    //1. write kind
    writer.writeStartElement(m_StanzaKind.getIdentifier());
    //2. write attributes
    writeStanzaAttributes(writer);
    //3. write error and attributes
    writer.writeStartElement(EMPPTokens.ELEMENT_ERROR);
    writer.writeAttribute(EMPPTokens.ATTR_TYPE, m_ErrorType.toString());
    //3.1 write error condition
    m_ErrorCondition.writeExtensionSpecific(writer);

    //3.2 write text
    if (hasText()) {
      writer.writeStartElement(EMPPTokens.ELEMENT_TEXT);
      writer.writeNamespace("", EMPPTokens.NAMESPACE_EMPPSTANZAS);
      writer.writeAttribute(EMPPTokens.ATTR_XMLLANG, m_Language.toValue());
      writer.writeCharacters(m_Text);
      writer.writeEndElement();
    }
    //3.3 write app error condition
    if (hasApplicationCondition()) {
      m_ApplicationCondition.writeExtensionSpecific(writer);
    }
    //3.4 write error end
    writer.writeEndElement();
    //4. write stanza kind end
    writer.writeEndElement();
  }//writeTo

  public void readFrom(XMLStreamReader reader)
      throws XMLStreamException, MalformedStructureException, MalformedContentException {
    //TODO: Thus, the <error/> element will contain two or three child elements
    //read in stanza kind and attributes
    setStanzaKind(PacketKinds.get(reader.getLocalName()));
    readStanzaAttributes(reader);
    reader.nextTag(); //error element
    String name = reader.getLocalName();
    String nsuri = reader.getNamespaceURI();
    if (EMPPTokens.ELEMENT_ERROR.equals(name)) {
      if (reader.getAttributeCount() > 0) {
        for (int i = 0; i < reader.getAttributeCount(); i++) {
          String aname = reader.getAttributeLocalName(i);
          if (EMPPTokens.ATTR_TYPE.equals(aname)) {
            setErrorType(StanzaErrorTypes.get(reader.getAttributeValue(i)));
          } else {
            //error?
          }
        }
      } else {
        //error?
      }
    } else {
      //error?
    }
    //Condition
    reader.nextTag();//condition
    name = reader.getLocalName();
    nsuri = reader.getNamespaceURI();
    if (EMPPTokens.NAMESPACE_EMPPSTANZAS.equals(nsuri)) {
      try {
        setErrorCondition(StanzaErrorConditions.getCondition(name));
      } catch (NoSuchElementException nsex) {
        Activator.log().error("readFrom()", nsex);
        //TODO: STANZA not well formed!
      }
    } else {
      //TODO: STANZA NOT WELL FORMED
    }
    //Text (optional), application condition (optional) or end element
    boolean done = false;
    int tagsread = 0;
    do {
      reader.nextTag();//condition
      name = reader.getLocalName();
      if (++tagsread > 6) {
        //malicous input, throw appropiate exception.
        Activator.log().error("Too much tags I suppose.");
      }
      nsuri = reader.getNamespaceURI();
      if (EMPPTokens.ELEMENT_TEXT.equals(name) &&
          EMPPTokens.NAMESPACE_EMPPSTANZAS.equals(nsuri)) {
        setText(reader.getElementText());
      } else if (StanzaErrorConditions.existsAppErrorCondition(nsuri)) {
        try {
          ApplicationErrorCondition apperrc = StanzaErrorConditions.leaseAppErrorCondition(nsuri);
          apperrc.readExtensionSpecific(reader);
          setApplicationCondition(apperrc);
        } catch (Exception ex) {
          Activator.log().error("readFrom()", ex);
        }
      } else if (m_StanzaKind.equals(name) && reader.isEndElement()) {
        done = true;
      } else {
        //error
      }
      //Application condition, optional
    } while (!done);
  }//readFrom

  public EMPPPacket createInstance() {
    return new ErrorStanzaImpl();
  }//createInstance

  public static void main(String[] args) {
    try {

      StringWriter out = new StringWriter();
      XMLOutputFactory factory = XMLOutputFactory.newInstance();
      XMLStreamWriter writer = factory.createXMLStreamWriter(out);

      ErrorStanzaImpl error = new ErrorStanzaImpl(PacketKinds.INSTANT_MESSAGE);
      error.setErrorCondition(StanzaErrorConditions.RESOURCE_CONSTRAINT);
      error.setText("Server overloaded.");
      StanzaErrorConditions.registerAppErrorCondition("net:coalevo:testapperror", TestAppError.class);
      error.setApplicationCondition(new TestAppError());
      error.writeTo(writer);


      writer.close();

      System.out.println(out.toString());


      System.out.println("------------");
      StringReader in = new StringReader(out.toString());
      ErrorStanzaImpl p2 = new ErrorStanzaImpl();
      XMLInputFactory factory2 = XMLInputFactory.newInstance();
      XMLStreamReader reader = factory2.createXMLStreamReader(in);
      reader.next(); //advance to start element
      p2.readFrom(reader);
      reader.close();
      XMLStreamWriter w2 = factory.createXMLStreamWriter(System.out);
      p2.writeTo(w2);
      w2.close();

    } catch (Exception ex) {
      ex.printStackTrace();
    }

  }//main


  public static class TestAppError extends BaseApplicationErrorCondition {

    public TestAppError() {
      super("test-app-error");
    }

    public String getNamespace() {
      return "net:coalevo:testapperror";
    }//getNamespace

  }//TestAppError

}//class ErrorStanzaImpl

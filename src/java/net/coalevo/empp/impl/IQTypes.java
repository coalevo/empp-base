/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.impl;

import net.coalevo.empp.model.IQType;
import net.coalevo.empp.model.Type;

import java.util.*;

/**
 * This class defines all valid {@link IQType} instances.
 * <p/>
 *
 * @author Dieter Wimberger (coalevo)
 * @version @version@ (@date@)
 */
public class IQTypes {

  /**
   * Defines the {@link IQType} <tt>get</tt>:
   * The stanza is a request.
   */
  public static final IQType REQUEST =
      new IQTypeImpl(EMPPTokens.IQ_TYPE_REQUEST);

  /**
   * Defines the {@link IQType} <tt>set</tt>:
   * The stanza provides required data, sets new values, or
   * replaces existing values.
   */
  public static final IQType RESPONSE =
      new IQTypeImpl(EMPPTokens.IQ_TYPE_RESPONSE);


  /**
   * Defines the {@link IQType} <tt>error</tt>:
   * An error has occurred regarding processing or
   * delivery of a previously-sent request or response.
   */
  public static final IQType ERROR =
      new IQTypeImpl(EMPPTokens.IQ_TYPE_ERROR);

  /**
   * Array holding the defined {@link IQType} instances.
   */
  protected static final IQType[] TYPES =
      new IQType[]{REQUEST, RESPONSE, ERROR};

  /**
   * Set holding the defined {@link IQType} instances.
   */
  protected static final Set<IQType> IQ_TYPES =
      Collections.unmodifiableSet(new HashSet<IQType>(Arrays.asList(TYPES)));

  /**
   * Tests if the given {@link IQType} is valid.
   *
   * @param type a {@link IQType}.
   * @return true if valid, false otherwise.
   */
  public final static boolean isValid(Type type) {
    return IQ_TYPES.contains(type);
  }//isValid

  /**
   * Returns a {@link IQType} instance for the given identifier.
   *
   * @param identifier a string identifying a {@link IQType}.
   * @return the {@link IQType} with the given identifier.
   * @throws NoSuchElementException if a {@link IQType} with
   *                                the given identifier does not exist.
   */
  public final static IQType get(String identifier)
      throws NoSuchElementException {
    for (IQType aIQ_TYPES : IQ_TYPES) {
      IQType iqType = (IQType) aIQ_TYPES;
      if (iqType.getIdentifier().equals(identifier)) {
        return iqType;
      }
    }
    throw new NoSuchElementException();
  }//get

  /**
   * Provides an implementation of {@link IQType}.
   * <p/>
   *
   * @author Dieter Wimberger (coalevo)
   * @version @version@ (@date@)
   */
  static class IQTypeImpl
      extends BaseType
      implements IQType {

    public IQTypeImpl(String id) {
      super(id);
    }//constructor

    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (!(o instanceof IQTypeImpl)) {
        return false;
      }

      final IQTypeImpl iqType = (IQTypeImpl) o;

      if (m_Id != null ? !m_Id.equals(iqType.m_Id) : iqType.m_Id != null) {
        return false;
      }

      return true;
    }//equals

  }//class IQTypeImpl


}//class IQTypes

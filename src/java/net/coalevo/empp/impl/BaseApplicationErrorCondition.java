/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.impl;

import net.coalevo.empp.model.ApplicationErrorCondition;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

/**
 * This class implements {@link net.coalevo.empp.model.ApplicationErrorCondition}.
 * <p/>
 *
 * @author Dieter Wimberger (coalevo)
 * @version @version@ (@date@)
 */
public abstract class BaseApplicationErrorCondition
    implements ApplicationErrorCondition {

  private final String m_Identifier;


  public BaseApplicationErrorCondition(String id) {
    m_Identifier = id;
  }//constructor

  public abstract String getNamespace();

  public String getIdentifier() {
    return m_Identifier;
  }//toValue

  //Note: Here writing the empty element will work
  //because something will follow for sure.
  public void writeExtensionSpecific(XMLStreamWriter writer)
      throws XMLStreamException {
    writer.writeEmptyElement(getIdentifier());
    writer.writeAttribute(EMPPTokens.ATTR_XMLNS, getNamespace());
    //writer.writeEndElement();
  }//writeExtensionSpecific


  public void readExtensionSpecific(XMLStreamReader reader)
      throws XMLStreamException {

  }//readExtensionSpecific

  public void recycle() {

  }//recycle

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof BaseApplicationErrorCondition)) {
      return false;
    }

    final BaseApplicationErrorCondition appErrorCondition = (BaseApplicationErrorCondition) o;

    if (m_Identifier != null ? !m_Identifier.equals(appErrorCondition.m_Identifier) : appErrorCondition.m_Identifier != null) {
      return false;
    }

    return true;
  }//equals

  public int hashCode() {
    return (m_Identifier != null ? m_Identifier.hashCode() : 0);
  }//hashCode


}//class StanzaErrorConditionImpl

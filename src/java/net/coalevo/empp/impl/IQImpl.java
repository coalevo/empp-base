/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.impl;

import net.coalevo.empp.model.*;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

/**
 * This class implements {@link IQ}.
 * <p/>
 *
 * @author Dieter Wimberger (coalevo)
 * @version @version@ (@date@)
 */
class IQImpl
    extends ExtensibleBaseStanza
    implements IQ {

  public IQImpl() {
    super(PacketKinds.IQ);
    m_Language = null;
  }//constructor

  public void setType(Type type) {
    if (IQTypes.isValid(type)) {
      m_Type = type;
    } else {
      throw new IllegalArgumentException();
    }
  }//setKind

  public void setType(String tid) {
    m_Type = IQTypes.get(tid);
  }//setKind

  public void writeTo(XMLStreamWriter writer)
      throws XMLStreamException {
    //1. IQ element
    writer.writeStartElement(EMPPTokens.ELEMENT_IQ);
    //2. Stanza attributes
    writeStanzaAttributes(writer);
    //3. write extensions
    writeExtensions(writer);
    //4. write end iq
    writer.writeEndElement();
    writer.flush();
  }//writeTo

  public void readFrom(XMLStreamReader reader) throws XMLStreamException, MalformedStructureException,
      MalformedContentException {
    //1. read stanza common attributes
    reader.nextTag();
    readStanzaAttributes(reader);

    boolean done = false;
    do {
      reader.nextTag();
      String name = reader.getLocalName();
      String nsuri = reader.getNamespaceURI();
      if (nsuri != null && nsuri.length() > 0) {
        if (Stanzas.existsIQExtension(nsuri)) {
          try {
            IQNamespaceExtension iqex = Stanzas.leaseIQExtension(nsuri);
            iqex.readExtensionSpecific(reader);
            addExtension(nsuri, iqex);
          } catch (Exception ex) {
            //
          }
        } else {
          skipTo(reader, name);
        }
      } else if (EMPPTokens.ELEMENT_IQ.equals(name) && reader.isEndElement()) {
        done = true;
      }
    } while (!done);
  }//readFrom

  public void recycle() {
    super.recycle();    
  }//reset

  public EMPPPacket createInstance() {
    return new IQImpl();
  }//createInstance

}//class IQImpl

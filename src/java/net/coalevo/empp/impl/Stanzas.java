/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.impl;

import net.coalevo.empp.model.*;
import net.coalevo.empp.util.ClassBasePoolableFactory;
import org.apache.commons.pool.BasePoolableObjectFactory;
import org.apache.commons.pool.ObjectPool;
import org.apache.commons.pool.impl.GenericObjectPool;

import java.util.HashMap;
import java.util.Map;

/**
 * This class implements a factory and pool for
 * {@link net.coalevo.empp.model.Stanza} instances:
 * <ol>
 * <li>{@link net.coalevo.empp.model.InstantMessage}</li>
 * <li>{@link net.coalevo.empp.model.Presence}</li>
 * <li>{@link net.coalevo.empp.model.IQ}</li>
 * <li>as well as their respective {@link net.coalevo.empp.model.ErrorStanza}</li>
 * </ol>
 * <p/>
 *
 * @author Dieter Wimberger (coalevo)
 * @version @version@ (@date@)
 */
public class Stanzas {

  //private static final Logger log = LoggerFactory.getLogger(Stanzas.class);
  private static final GenericObjectPool.Config c_PoolConfig;
  private static final Map<String, ObjectPool> c_IQExtensions;
  private static final Map<String, ObjectPool> c_IMExtensions;
  private static final Map<String, ObjectPool> c_PresenceExtensions;

  static {
    // pool config
    c_PoolConfig = new GenericObjectPool.Config();
    //set for
    c_PoolConfig.maxActive = 50;
    c_PoolConfig.maxIdle = 25;
    c_PoolConfig.maxWait = -1;
    c_PoolConfig.testOnBorrow = false;
    c_PoolConfig.testOnReturn = false;
    //make it growable so it adapts
    c_PoolConfig.whenExhaustedAction = GenericObjectPool.WHEN_EXHAUSTED_GROW;
    c_IQExtensions = new HashMap<String, ObjectPool>();
    c_IMExtensions = new HashMap<String, ObjectPool>();
    c_PresenceExtensions = new HashMap<String, ObjectPool>();

  }//static initializer

  public static final InstantMessage leaseInstantMessage()
      throws Exception {
    return (InstantMessage) Packets.leasePacket(EMPPTokens.PACKET_ID_MESSAGE);
  }//leaseInstantMessage

  public static final void releaseInstantMessage(InstantMessage im)
      throws Exception {
    Packets.releasePacket(im);
  }//releaseInstantMessage

  public static final void registerIMExtension(String namespace, Class cl) {
    if (InstantMessageNamespaceExtension.class.isAssignableFrom(cl)) {
      c_IMExtensions.put(namespace, new GenericObjectPool(new ClassBasePoolableFactory(cl), c_PoolConfig));
    }
    Activator.log().error("registerIMExtension(String,Class)::" + namespace + "::" + cl.toString());
    throw new IllegalArgumentException();
  }//registerIMExtension

  public static final void unregisterIMExtension(String namespace) {
    ObjectPool p = c_IMExtensions.remove(namespace);
    try {
      p.clear();
      p.close();
    } catch (Exception ex) {
      Activator.log().error("unregisterIMExtension()", ex);
    }
  }//unregisterIMExtension

  public static final boolean existsIMExtension(String namespace) {
    return c_IMExtensions.containsKey(namespace);
  }//existsIMExtension

  public static final InstantMessageNamespaceExtension leaseIMExtension(String namespace)
      throws Exception {
    Object o = c_IMExtensions.get(namespace);
    if (o == null) {
      Activator.log().error("No InstantMessageNamespaceExtension implementation was registered for namespace " + namespace);
      throw new IllegalArgumentException();
    } else {
      return (InstantMessageNamespaceExtension) ((GenericObjectPool) o).borrowObject();
    }
  }//leaseIMExtension

  public static final void releaseIMExtension(InstantMessageNamespaceExtension imext)
      throws Exception {
    String namespace = ((InstantMessageNamespaceExtension) imext).getNamespace();
    Object o = c_IMExtensions.get(namespace);
    if (o == null) {
      Activator.log().error("No InstantMessageNamespaceExtension implementation was registered for namespace " + namespace);
      throw new IllegalStateException();
    } else {
      ((GenericObjectPool) o).returnObject(imext);
    }
  }//releaseIMExtension

  public static final Presence leasePresence()
      throws Exception {
    return (Presence) Packets.leasePacket(EMPPTokens.PACKET_ID_PRESENCE);
  }//leasePresence

  public static final void releasePresence(Presence p)
      throws Exception {
    Packets.releasePacket(p);
  }//releasePresence

  public static final void registerPresenceExtension(String namespace, Class cl) {
    if (PresenceNamespaceExtension.class.isAssignableFrom(cl)) {
      c_PresenceExtensions.put(namespace, new GenericObjectPool(new ClassBasePoolableFactory(cl), c_PoolConfig));
    } else {
      Activator.log().error("registerPresenceExtension(String,Class)::" + namespace + "::" + cl.toString());
      throw new IllegalArgumentException();
    }
  }//registerPresenceExtension

  public static final void unregisterPresenceExtension(String namespace) {
    ObjectPool p = c_PresenceExtensions.remove(namespace);
    try {
      p.clear();
      p.close();
    } catch (Exception ex) {
      Activator.log().error("unregisterPresenceExtension()", ex);
    }
  }//unregisterPresenceExtension

  public static final boolean existsPresenceExtension(String namespace) {
    return c_PresenceExtensions.containsKey(namespace);
  }//existsPresenceExtension

  public static final PresenceNamespaceExtension leasePresenceExtension(String namespace)
      throws Exception {
    Object o = c_PresenceExtensions.get(namespace);
    if (o == null) {
      Activator.log().error("No PresenceNamespaceExtension implementation was registered for namespace " + namespace);
      throw new IllegalArgumentException();
    } else {
      return (PresenceNamespaceExtension) ((GenericObjectPool) o).borrowObject();
    }
  }//leasePresenceExtension

  public static final void releasePresenceExtension(PresenceNamespaceExtension pext)
      throws Exception {
    String namespace = ((PresenceNamespaceExtension) pext).getNamespace();
    Object o = c_PresenceExtensions.get(namespace);
    if (o == null) {
      Activator.log().error("No PresenceNamespaceExtension implementation was registered for namespace " + namespace);
      throw new IllegalStateException();
    } else {
      ((GenericObjectPool) o).returnObject(pext);
    }
  }//releasePresenceExtension


  public static final IQ leaseIQ()
      throws Exception {
    return (IQ) Packets.leasePacket(EMPPTokens.PACKET_ID_IQ);
  }//leaseIQ

  public static final void releaseIQ(IQ iq)
      throws Exception {
    Packets.releasePacket(iq);
  }//releaseIQ

  public static final void registerIQExtension(String namespace, Class cl) {
    if (IQNamespaceExtension.class.isAssignableFrom(cl)) {
      c_IQExtensions.put(namespace, new GenericObjectPool(new ClassBasePoolableFactory(cl), c_PoolConfig));
    } else {
      Activator.log().error("registerIQExtension(String,Class)::" + namespace + "::" + cl.toString());
      throw new IllegalArgumentException();
    }
  }//registerIQExtension

  public static final void unregisterIQExtension(String namespace) {
    ObjectPool p = c_IQExtensions.remove(namespace);
    try {
      p.clear();
      p.close();
    } catch (Exception ex) {
      Activator.log().error("unregisterIQExtension()", ex);
    }
  }//unregisterIQExtension

  public static final boolean existsIQExtension(String namespace) {
    return c_IQExtensions.containsKey(namespace);
  }//existsIQ

  public static final IQNamespaceExtension leaseIQExtension(String namespace)
      throws Exception {
    Object o = c_IQExtensions.get(namespace);
    if (o == null) {
      Activator.log().error("No IQNamespaceExtension implementation was registered for namespace " + namespace);
      throw new IllegalArgumentException();
    } else {
      return (IQNamespaceExtension) ((GenericObjectPool) o).borrowObject();
    }
  }//leaseIQExtension

  public static final void releaseIQExtension(IQNamespaceExtension iqext)
      throws Exception {
    String namespace = ((IQNamespaceExtension) iqext).getNamespace();
    Object o = c_IQExtensions.get(namespace);
    if (o == null) {
      Activator.log().error("No IQNamespaceExtension implementation was registered for namespace " + namespace);
      throw new IllegalStateException();
    } else {
      ((GenericObjectPool) o).returnObject(iqext);
    }
  }//releaseIQExtension

  public static final ErrorStanza leaseError()
      throws Exception {
    return (ErrorStanza) Packets.leasePacket(EMPPTokens.PACKET_ID_ERROR);
  }//leaseError

  public static final void releaseError(ErrorStanza es)
      throws Exception {
    Packets.releasePacket(es);
  }//releaseError


  protected static class InstantMessageFactory
      extends BasePoolableObjectFactory {


    public Object makeObject() throws Exception {
      return new InstantMessageImpl();
    }//makeObject

    public void passivateObject(Object o) {
      ((InstantMessageImpl) o).recycle();
    }//passivateObject

  }//inner class InstantMessageFactory

  protected static class PresenceFactory
      extends BasePoolableObjectFactory {


    public Object makeObject() throws Exception {
      return new PresenceImpl();
    }//makeObject

    public void passivateObject(Object o) {
      ((PresenceImpl) o).recycle();
    }//passivateObject

  }//inner class PresenceFactory

  protected static class ErrorFactory
      extends BasePoolableObjectFactory {


    public Object makeObject() throws Exception {
      return new ErrorStanzaImpl();
    }//makeObject

    public void passivateObject(Object o) {
      ((ErrorStanzaImpl) o).recycle();
    }//passivateObject

  }//inner class ErrorStanzaFactory

}//class Stanzas

/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.impl;

import net.coalevo.empp.model.PresenceType;
import net.coalevo.empp.model.StanzaErrorType;

import java.util.*;

/**
 * This class defines all valid {@link StanzaErrorType}
 * instances.
 * <p/>
 *
 * @author Dieter Wimberger (coalevo)
 * @version @version@ (@date@)
 */
public class StanzaErrorTypes {

  /**
   * Defines the type <tt>cancel</tt>:
   * do not retry (the error is unrecoverable)
   */
  public static final StanzaErrorType CANCEL =
      new StanzaErrorTypeImpl(EMPPTokens.STANZAERROR_TYPE_CANCEL);

  /**
   * Defines the type <tt>continue</tt>:
   * proceed (the condition was only a warning)
   */
  public static final StanzaErrorType CONTINUE =
      new StanzaErrorTypeImpl(EMPPTokens.STANZAERROR_TYPE_CONTINUE);

  /**
   * Defines the type <tt>modify</tt>:
   * retry after changing the data sent
   */
  public static final StanzaErrorType MODIFY =
      new StanzaErrorTypeImpl(EMPPTokens.STANZAERROR_TYPE_MODIFY);

  /**
   * Defines the type <tt>auth</tt>:
   * retry after providing credentials
   */
  public static final StanzaErrorType AUTH =
      new StanzaErrorTypeImpl(EMPPTokens.STANZAERROR_TYPE_AUTH);

  /**
   * Defines the type <tt>any</tt>:
   * This is used for the {@link net.coalevo.empp.model.StanzaErrorCondition}
   * <tt>undefined-condition</tt> to indicate that an application condition
   * is required.
   */
  public static final StanzaErrorType ANY =
      new StanzaErrorTypeImpl("any");

  /**
   * Defines the type <tt>wait</tt>:
   * retry after waiting (the error is temporary)
   */
  public static final StanzaErrorType WAIT =
      new StanzaErrorTypeImpl(EMPPTokens.STANZAERROR_TYPE_WAIT);


  /**
   * Array holding the defined {@link StanzaErrorType} instances.
   */
  protected static final StanzaErrorType[] TYPES =
      new StanzaErrorType[]{CANCEL, CONTINUE, MODIFY, AUTH, WAIT};

  /**
   * Set holding the defined {@link StanzaErrorType} instances.
   */
  protected static final Set<StanzaErrorType> STANZAERROR_TYPES =
      Collections.unmodifiableSet(
          new HashSet<StanzaErrorType>(Arrays.asList(TYPES))
      );

  /**
   * Tests if the given {@link StanzaErrorType} is valid.
   *
   * @param type a {@link StanzaErrorType}.
   * @return true if valid, false otherwise.
   */
  public final static boolean isValidType(StanzaErrorType type) {
    return STANZAERROR_TYPES.contains(type);
  }//isValid

  /**
   * Returns a {@link StanzaErrorType} instance for the given identifier.
   *
   * @param identifier a string identifying a {@link StanzaErrorType}.
   * @return the {@link PresenceType} with the given identifier.
   * @throws NoSuchElementException if a {@link StanzaErrorType} with
   *                                the given identifier does not exist.
   */
  public final static StanzaErrorType get(String identifier)
      throws NoSuchElementException {
    for (StanzaErrorType type : STANZAERROR_TYPES) {
      if (type.getIdentifier().equals(identifier)) {
        return type;
      }
    }
    throw new NoSuchElementException();
  }//get


  /**
   * Provides an implementation of {@link StanzaErrorType}.
   * <p/>
   *
   * @author Dieter Wimberger (coalevo)
   * @version @version@ (@date@)
   */
  static class StanzaErrorTypeImpl
      extends BaseType
      implements StanzaErrorType {

    public StanzaErrorTypeImpl(String id) {
      super(id);
    }//constructor

    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (!(o instanceof StanzaErrorType)) {
        return false;
      }
      final StanzaErrorTypeImpl type = (StanzaErrorTypeImpl) o;
      if (m_Id != null ? !m_Id.equals(type.m_Id) : type.m_Id != null) {
        return false;
      }
      return true;
    }//equals

  }//class StanzaErrorTypeImpl

}//class StanzaErrorTypes

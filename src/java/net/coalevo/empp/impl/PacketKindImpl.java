/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.impl;

import net.coalevo.empp.model.PacketKind;

/**
 * This class implements {@link net.coalevo.empp.model.PacketKind}.
 * <p/>
 *
 * @author Dieter Wimberger (coalevo)
 * @version @version@ (@date@)
 */
class PacketKindImpl
    extends BaseType
    implements PacketKind {

  protected final Integer m_Number;

  public PacketKindImpl(int num, String id) {
    super(id);
    m_Number = num;
  }//constructor

  public Integer get() {
    return m_Number;
  }//get

  public int getNumber() {
    return m_Number;
  }//getNumber


  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }

    if (!(o instanceof PacketKindImpl)) {
      return false;
    }

    final PacketKindImpl packetKind = (PacketKindImpl) o;

    if (m_Number.equals(packetKind.m_Number)) {
      return false;
    }
    if (m_Id != null ? !m_Id.equals(packetKind.m_Id) : packetKind.m_Id != null) {
      return false;
    }
    return true;
  }//equals

  public int hashCode() {
    int result = super.hashCode();
    result = 29 * result + m_Number;
    return result;
  }//hashCode

}//class PacketKindImpl

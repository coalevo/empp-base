/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.impl;

import net.coalevo.empp.model.PacketKind;

import java.util.*;

/**
 * Provides defined {@link PacketKind} instances.
 * <p/>
 *
 * @author Dieter Wimberger (coalevo)
 * @version @version@ (@date@)
 */
public class PacketKinds {

  /**
   * Defines the {@link PacketKind} of a {@link net.coalevo.empp.model.Session}.
   */
  public static final PacketKind SESSION =
      new PacketKindImpl(EMPPTokens.PACKET_ID_SESSION, EMPPTokens.PACKET_KIND_SESSION);

  /**
   * Defines the {@link PacketKind} of a {@link net.coalevo.empp.model.InstantMessage}.
   */
  public static PacketKind INSTANT_MESSAGE =
      new PacketKindImpl(EMPPTokens.PACKET_ID_MESSAGE, EMPPTokens.PACKET_KIND_MESSAGE);

  /**
   * Defines the {@link PacketKind} of a {@link net.coalevo.empp.model.IQ}
   */
  public static final PacketKind IQ =
      new PacketKindImpl(EMPPTokens.PACKET_ID_IQ, EMPPTokens.PACKET_KIND_IQ);

  /**
   * Defines the {@link PacketKind} of a {@link net.coalevo.empp.model.Presence}
   */
  public static final PacketKind PRESENCE =
      new PacketKindImpl(EMPPTokens.PACKET_ID_PRESENCE, EMPPTokens.PACKET_KIND_PRESENCE);


  protected static final PacketKind[] KINDS =
      new PacketKind[]{SESSION, INSTANT_MESSAGE, IQ, PRESENCE};

  protected static final Set<PacketKind> PACKET_KINDS =
      Collections.unmodifiableSet(new HashSet<PacketKind>(Arrays.asList(KINDS)));

  /**
   * Tests if the given {@link PacketKind} is registered here.
   *
   * @param p the {@link PacketKind} to be tested.
   * @return true if a valid {@link PacketKind} false otherwise.
   */
  public final static boolean isValidKind(PacketKind p) {
    return PACKET_KINDS.contains(p);
  }//isValid

  /**
   * Returns the {@link PacketKind} with the given identifier.
   *
   * @param identifier the identifier as <tt>String</tt>.
   * @return a {@link PacketKind}
   * @throws NoSuchElementException if no {@link PacketKind} exists for the
   *                                given identifier.
   */
  public final static PacketKind get(String identifier) {
    for (PacketKind packetKind : PACKET_KINDS) {
      if (packetKind.getIdentifier().equals(identifier)) {
        return packetKind;
      }
    }
    throw new NoSuchElementException();
  }//get

  /**
   * Returns the {@link PacketKind} with the given identifier.
   *
   * @param nid the numerical identifier as <tt>Integer</tt>.
   * @return a {@link PacketKind}
   * @throws NoSuchElementException if no {@link PacketKind} exists for the
   *                                given numerical identifier.
   */
  public final static PacketKind get(Integer nid) {
    for (PacketKind packetKind : PACKET_KINDS) {
      if (packetKind.get().equals(nid)) {
        return packetKind;
      }
    }
    throw new IllegalArgumentException();
  }//get

}//class PacketKinds

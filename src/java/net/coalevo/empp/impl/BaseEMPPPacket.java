/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.impl;

import net.coalevo.empp.io.StreamFactory;
import net.coalevo.empp.model.EMPPPacket;
import net.coalevo.empp.model.MalformedContentException;
import net.coalevo.empp.model.MalformedStructureException;
import net.coalevo.empp.model.PacketKind;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.model.AgentIdentifierInstanceCache;
import net.coalevo.foundation.model.MalformedAgentIdentifierException;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;
import java.io.*;


/**
 * This class implements an abstract base
 * {@link net.coalevo.empp.model.EMPPPacket}.
 * <p/>
 *
 * @author Dieter Wimberger (coalevo)
 * @version @version@ (@date@)
 */
public abstract class BaseEMPPPacket
    implements EMPPPacket {

  protected static StreamFactory c_StreamFactory = Activator.getStreamFactory();
  protected static AgentIdentifierInstanceCache c_AIDCache =
      new AgentIdentifierInstanceCache(100);

  protected final PacketKind m_Kind;
  protected AgentIdentifier m_From;
  protected AgentIdentifier m_To;
  private String m_SessionIdentifier;
  protected byte[] m_Payload;
  protected int m_PLength;

  public BaseEMPPPacket(PacketKind pk) {
    m_Kind = pk;
  }//BaseEMPPPacket

  public AgentIdentifier getFrom() {
    return m_From;
  }//getFrom

  public void setFrom(AgentIdentifier from) {
    m_From = from;
  }//setFrom

  public boolean hasFrom() {
    return m_From != null;
  }//hasFrom

  public AgentIdentifier getTo() {
    return m_To;
  }//getTo

  public void setTo(AgentIdentifier to) {
    m_To = to;
  }//setTo

  public boolean hasTo() {
    return m_To != null;
  }//hasTo

  public String getSessionIdentifier() {
    return m_SessionIdentifier;
  }//getIdentifier

  public void setSessionIdentifier(String id) {
    m_SessionIdentifier = id;
  }//setIdentifier

  public boolean hasSessionIdentifier() {
    return m_SessionIdentifier != null;
  }//hasSessionIdentifier

  public PacketKind getKind() {
    return m_Kind;
  }//getKind

  public void recycle() {
    m_SessionIdentifier = null;
    m_From = null;
    m_To = null;
    m_Payload = null;
    m_PLength = 0;
  }//reset

  public void writeTo(DataOutput dout)
      throws IOException {
    dout.writeInt(m_Kind.getNumber());
    dout.writeUTF(m_SessionIdentifier);
    dout.writeUTF(m_From.toString());
    dout.writeUTF(m_To.toString());
    dout.writeShort(m_PLength);
    dout.write(m_Payload, 0, m_PLength);
  }//writeTo

  public void readFrom(DataInput din) throws IOException, MalformedAgentIdentifierException {
    //KIND is read by the decoder and used to lease a correct packet
    m_SessionIdentifier = din.readUTF();
    m_From = c_AIDCache.get(din.readUTF());
    m_To = c_AIDCache.get(din.readUTF());
    m_PLength = din.readUnsignedShort();
    m_Payload = new byte[m_PLength];
    din.readFully(m_Payload, 0, m_PLength);
  }//readFrom

  protected abstract void writeTo(XMLStreamWriter writer)
      throws XMLStreamException;

  protected abstract void readFrom(XMLStreamReader reader)
      throws XMLStreamException, MalformedContentException, MalformedStructureException;

  public abstract void preparePayload();

  public abstract void parsePayload()
      throws XMLStreamException, MalformedContentException, MalformedStructureException;

  public abstract EMPPPacket createInstance();

  //test routine
  public static void printInOut(BaseEMPPPacket p) throws Exception {
    StringWriter out = new StringWriter();
    System.out.println("------------");

    XMLStreamWriter writer = Activator.getStreamFactory().forOutput(out);

    p.writeTo(writer);
    writer.flush();
    System.out.println(out.toString());

    StringReader in = new StringReader(out.toString());
    BaseEMPPPacket p2 = (BaseEMPPPacket) p.createInstance();
    XMLStreamReader reader = Activator.getStreamFactory().forInput(in);
    p2.readFrom(reader);
    reader.close();
    XMLStreamWriter w2 = Activator.getStreamFactory().forOutput(System.out);
    p2.writeTo(w2);
    w2.close();
    System.out.println();
  }//main

}//class BaseEMPPPacket

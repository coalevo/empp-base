/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.impl;

import net.coalevo.empp.io.StreamFactory;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;
import org.slf4j.LoggerFactory;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;

/**
 * Provides a <tt>BundleActivator</tt> implementation.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class Activator implements BundleActivator {

  private static StreamFactory c_StreamFactory;
  private static ServiceMediator c_Services;
  private static boolean c_Started = false;

//  private static LogProxy c_Log;
  private static Logger c_Log;
  private static Marker c_LogMarker;

  public void start(BundleContext bundleContext) throws Exception {
    //1. Log
    c_LogMarker = MarkerFactory.getMarker(Activator.class.getName());
    c_Log = LoggerFactory.getLogger("empp-base");
//    c_Log = new LogProxy();
//    c_Log.activate(bundleContext);
    //2. Services
    c_Services = new ServiceMediator();
    c_Services.activate(bundleContext);
    c_Started = true;
  }//start

  public void stop(BundleContext bundleContext) throws Exception {
    if (c_Services != null) {
      c_Services.deactivate();
      c_Services = null;
    }
//    if (c_Log != null) {
//      c_Log.deactivate();
//      c_Log = null;
//    }
    c_LogMarker = null;
    c_StreamFactory = null;
  }//stop

  public static XMLInputFactory getXMLInputFactory() {
    return c_Services.getXMLInputFactory(ServiceMediator.WAIT_UNLIMITED);
  }//XMLInputFactory

  public static XMLOutputFactory getXMLOutputFactory() {
    return c_Services.getXMLOutputFactory(ServiceMediator.WAIT_UNLIMITED);
  }//XMLOutputFactory

  public static StreamFactory getStreamFactory() {
    if (c_StreamFactory == null) {
      c_StreamFactory = new StreamFactory();
      c_StreamFactory.prepare();
    }
    return c_StreamFactory;
  }//getStreamFactory

  public static boolean isStarted() {
    return c_Started;
  }//isStarted

  /**
   * Return the bundles logger.
   *
   * @return the <tt>Logger</tt>.
   */
  public static Logger log() {
    if(c_Log == null) {
      c_Log = LoggerFactory.getLogger("empp-base");
    }
    return c_Log;
  }//log

}//class Activator

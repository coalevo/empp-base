/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.impl;

import net.coalevo.empp.model.PresenceStatusType;

import java.util.*;

/**
 * Provides defined {@link PresenceStatusType} instances.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class PresenceStatusTypes {

  /**
   * Defines the {@link PresenceStatusType} <tt>Available</tt>.
   */
  public static final PresenceStatusType AVAILABLE =
      new PresenceStatusTypeImpl(EMPPTokens.PRESENCE_STATUSTYPE_AVAILABLE);

  /**
   * Defines the {@link PresenceStatusType} <tt>AdvertisedAvailable</tt>.
   */
  public static final PresenceStatusType ADVERTISED_AVAILABLE =
      new PresenceStatusTypeImpl(EMPPTokens.PRESENCE_STATUSTYPE_ADVERTISED_AVAILABLE);

  /**
   * Defines the {@link PresenceStatusType} <tt>Unavailable</tt>.
   */
  public static final PresenceStatusType ADVERTISED_TEMPORARILY_UNAVAILABLE =
      new PresenceStatusTypeImpl(EMPPTokens.PRESENCE_STATUSTYPE_ADVERTISED_TEMPORARY_UNAVAILABLE);

  /**
   * Defines the {@link PresenceStatusType} <tt>Unavailable</tt>.
   */
  public static final PresenceStatusType TEMPORARILY_UNAVAILABLE =
      new PresenceStatusTypeImpl(EMPPTokens.PRESENCE_STATUSTYPE_TEMPORARY_UNAVAILABLE);

  /**
   * Defines the {@link PresenceStatusType} <tt>Unvailable</tt>.
   */
  public static final PresenceStatusType UNAVAILABLE =
      new PresenceStatusTypeImpl(EMPPTokens.PRESENCE_STATUSTYPE_UNAVAILABLE);


  /**
   * Array holding the defined {@link PresenceStatusType} instances.
   */

  protected static final PresenceStatusType[] TYPES =
      new PresenceStatusType[]{ADVERTISED_AVAILABLE, AVAILABLE, UNAVAILABLE, TEMPORARILY_UNAVAILABLE,ADVERTISED_TEMPORARILY_UNAVAILABLE};

  /**
   * Set holding the defined {@link PresenceStatusType} instances.
   */
  protected static final Set<PresenceStatusType> PRESENCE_TYPES =
      Collections.unmodifiableSet(new HashSet<PresenceStatusType>(Arrays.asList(TYPES)));

  /**
   * Tests if the given {@link PresenceStatusType} is valid.
   *
   * @param ptype a {@link PresenceStatusType}.
   * @return true if valid, false otherwise.
   */
  public final static boolean isValid(PresenceStatusType ptype) {
    return PRESENCE_TYPES.contains(ptype);
  }//isValid

  /**
   * Returns a {@link PresenceStatusType} instance for the given identifier.
   *
   * @param identifier a string identifying a {@link PresenceStatusType}.
   * @return the {@link PresenceStatusType} with the given identifier.
   * @throws NoSuchElementException if a {@link PresenceStatusType} with
   *                                the given identifier does not exist.
   */
  public final static PresenceStatusType get(String identifier)
      throws NoSuchElementException {
    for (PresenceStatusType pst : PRESENCE_TYPES) {
      if (pst.getIdentifier().equals(identifier)) {
        return pst;
      }
    }
    throw new NoSuchElementException();
  }//get

  /**
   * Provides an implementation of {@link PresenceStatusType}.
   * <p/>
   *
   * @author Dieter Wimberger (wimpi)
   * @version @version@ (@date@)
   */
  static class PresenceStatusTypeImpl
      extends BaseType
      implements PresenceStatusType {

    public PresenceStatusTypeImpl(String id) {
      super(id);
    }//constructor

    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (!(o instanceof PresenceStatusTypeImpl)) {
        return false;
      }

      final PresenceStatusTypeImpl pType = (PresenceStatusTypeImpl) o;

      if (m_Id != null ? !m_Id.equals(pType.m_Id) : pType.m_Id != null) {
        return false;
      }

      return true;
    }//equals

  }//class PresenceStatusTypeImpl

}//class PresenceStatusTypes

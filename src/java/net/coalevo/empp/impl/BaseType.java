/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.impl;


import net.coalevo.empp.model.Type;

/**
 * This class implements a <tt>BaseType</tt>.
 * <p/>
 *
 * @author Dieter Wimberger (coalevo)
 * @version @version@ (@date@)
 */
class BaseType
    implements Type {

  protected final String m_Id;

  public BaseType(String id) {
    m_Id = id;
  }//constructor

  public final String getIdentifier() {
    return m_Id;
  }//getIdentifier

  public String toString() {
    return m_Id;
  }//toString

  public int hashCode() {
    return (m_Id != null ? m_Id.hashCode() : 0);
  }//hashCode

}//class BaseType

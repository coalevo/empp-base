/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.impl;

import net.coalevo.empp.model.Presence;
import net.coalevo.empp.model.PresenceType;
import net.coalevo.empp.model.Type;

import java.util.*;

/**
 * This class defines all valid {@link PresenceType}
 * instances.
 * <p/>
 *
 * @author Dieter Wimberger (coalevo)
 * @version @version@ (@date@)
 */
public class PresenceTypes {

  /**
   * Defines the {@link PresenceType} for the {@link Presence}
   * that signals that the sender became present.
   */
  public static final PresenceType PRESENT =
      new PresenceTypeImpl(EMPPTokens.PRESENCE_TYPE_BECAMEPRESENT);

  /**
   * Defines the {@link PresenceType} for the {@link Presence}
   * that signals that the sender became absent.
   */
  public final static PresenceType ABSENT =
      new PresenceTypeImpl(EMPPTokens.PRESENCE_TYPE_BECAMEABSENT);

  /**
   * Defines the {@link PresenceType} for the {@link Presence}
   * that signals a status update of the sender.
   */
  public final static PresenceType STATUSUPDATE =
      new PresenceTypeImpl(EMPPTokens.PRESENCE_TYPE_STATUSUPDATE);

  /**
   * Defines the {@link PresenceType} for the {@link Presence}
   * that signals a update of the resources of the sender.
   */
  public final static PresenceType RESOURCEUPDATE =
      new PresenceTypeImpl(EMPPTokens.PRESENCE_TYPE_RESOURCEUPDATE);


  /**
   * Defines the {@link PresenceType} for the {@link Presence}
   * that signals that the sender requested subscription to the
   * receivers presence.
   */
  public final static PresenceType SUBSCRIPTION =
      new PresenceTypeImpl(EMPPTokens.PRESENCE_TYPE_SUBSCRIPTION);

  /**
   * Defines the type <tt>probe</tt>:
   * A request for an entity's current presence; SHOULD be
   * generated only by a server on behalf of a user.
   */
  public final static PresenceType PROBE =
      new PresenceTypeImpl(EMPPTokens.PRESENCE_TYPE_PROBE);

  /**
   * Defines the type <tt>error</tt>:
   * An error has occurred regarding processing or delivery of
   * a previously-sent presence stanza.
   */
  public final static PresenceType ERROR =
      new PresenceTypeImpl(EMPPTokens.PRESENCE_TYPE_ERROR);

  /**
   * Defines the type <tt>privacy</tt>:
   * Allows to block or unblock and to list incoming or outgoing
   * blocks.
   */
  public final static PresenceType PRIVACY =
      new PresenceTypeImpl(EMPPTokens.PRESENCE_TYPE_PRIVACY);

  /**
   * Array holding the defined {@link PresenceType} instances.
   */
  protected static final PresenceType[] TYPES =
      new PresenceType[]{PRESENT, ABSENT, STATUSUPDATE, RESOURCEUPDATE, SUBSCRIPTION, PRIVACY, PROBE, ERROR};

  /**
   * Set holding the defined {@link PresenceType} instances.
   */
  protected static final Set<PresenceType> PRESENCE_TYPES =
      Collections.unmodifiableSet(new HashSet<PresenceType>(Arrays.asList(TYPES)));

  /**
   * Tests if the given {@link PresenceType} is valid.
   *
   * @param ptype a {@link PresenceType}.
   * @return true if valid, false otherwise.
   */
  public final static boolean isValid(Type ptype) {
    return PRESENCE_TYPES.contains(ptype);
  }//isValid

  /**
   * Returns a {@link PresenceType} instance for the given identifier.
   *
   * @param identifier a string identifying a {@link PresenceType}.
   * @return the {@link PresenceType} with the given identifier.
   * @throws NoSuchElementException if a {@link PresenceType} with
   *                                the given identifier does not exist.
   */
  public final static PresenceType get(String identifier)
      throws NoSuchElementException {
    for (PresenceType presenceType : PRESENCE_TYPES) {
      if (presenceType.getIdentifier().equals(identifier)) {
        return presenceType;
      }
    }
    throw new NoSuchElementException();
  }//get


  /**
   * This class implements {@link PresenceType}.
   * <p/>
   *
   * @author Dieter Wimberger (coalevo)
   * @version @version@ (@date@)
   */
  static class PresenceTypeImpl
      extends BaseType
      implements PresenceType {

    public PresenceTypeImpl(String id) {
      super(id);
    }//constructor

    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (!(o instanceof PresenceTypeImpl)) {
        return false;
      }

      final PresenceTypeImpl pType = (PresenceTypeImpl) o;

      if (m_Id != null ? !m_Id.equals(pType.m_Id) : pType.m_Id != null) {
        return false;
      }

      return true;
    }//equals

  }//class PresenceTypeImpl

}//class PresenceTypes

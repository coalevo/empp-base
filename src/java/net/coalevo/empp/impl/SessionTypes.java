/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.impl;

import net.coalevo.empp.model.SessionType;
import net.coalevo.empp.model.Type;

import java.util.*;

/**
 * Provides all defined {@link SessionType}
 * instances.
 * <p/>
 *
 * @author Dieter Wimberger (coalevo)
 * @version @version@ (@date@)
 */
public class SessionTypes {

  /**
   * Defines the {@link SessionType} for a client originating
   * {@link net.coalevo.foundation.model.Session} stanza.
   */
  public static final SessionType CLIENT =
      new SessionTypeImpl(EMPPTokens.SESSION_TYPE_CLIENT);

  /**
   * Defines the {@link SessionType} for a client originating
   * {@link net.coalevo.foundation.model.Session} stanza.
   */
  public static final SessionType SERVER =
      new SessionTypeImpl(EMPPTokens.SESSION_TYPE_SERVER);


  /**
   * Array holding the defined {@link SessionType} instances.
   */
  protected static final SessionType[] TYPES =
      new SessionType[]{CLIENT, SERVER};

  /**
   * Set holding the defined {@link SessionType} instances.
   */
  protected static final Set<SessionType> SESSION_TYPES =
      Collections.unmodifiableSet(new HashSet<SessionType>(Arrays.asList(TYPES)));


  /**
   * Tests if the given {@link SessionType} is valid.
   *
   * @param ptype a {@link SessionType}.
   * @return true if valid, false otherwise.
   */

  public final static boolean isValid(Type ptype) {
    return SESSION_TYPES.contains(ptype);
  }//isValid

  /**
   * Returns a {@link SessionType} instance for the given identifier.
   *
   * @param identifier a string identifying a {@link SessionType}.
   * @return the {@link SessionType} with the given identifier.
   * @throws NoSuchElementException if a {@link SessionType} with
   *                                the given identifier does not exist.
   */
  public final static SessionType get(String identifier) {
    for (SessionType sessionType : SESSION_TYPES) {
      if (sessionType.getIdentifier().equals(identifier)) {
        return sessionType;
      }
    }
    throw new NoSuchElementException();
  }//get

  /**
   * This class implements {@link net.coalevo.empp.model.SessionType}.
   * <p/>
   *
   * @author Dieter Wimberger (coalevo)
   * @version @version@ (@date@)
   */
  static class SessionTypeImpl
      extends BaseType
      implements SessionType {

    public SessionTypeImpl(String id) {
      super(id);
    }//constructor

    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (!(o instanceof SessionTypeImpl)) {
        return false;
      }

      final SessionTypeImpl pType = (SessionTypeImpl) o;

      if (m_Id != null ? !m_Id.equals(pType.m_Id) : pType.m_Id != null) {
        return false;
      }

      return true;
    }//equals

  }//class SessionTypeImpl


}//class SessionTypes

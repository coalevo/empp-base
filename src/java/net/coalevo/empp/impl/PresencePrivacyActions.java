/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.impl;

import net.coalevo.empp.model.PresencePrivacyAction;

import java.util.*;

/**
 * Provides defined {@link PresencePrivacyAction} instances.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class PresencePrivacyActions {

  /**
   * Defines the {@link PresencePrivacyAction} <tt>block-in</tt>.
   */
  public static final PresencePrivacyAction BLOCK_INBOUND =
      new PresencePrivacyActionImpl(EMPPTokens.PRIVACY_ACTION_BLOCKIN);

  /**
   * Defines the {@link PresencePrivacyAction} <tt>block-out</tt>.
   */
  public static final PresencePrivacyAction BLOCK_OUTBOUND =
      new PresencePrivacyActionImpl(EMPPTokens.PRIVACY_ACTION_BLOCKOUT);

  /**
   * Defines the {@link PresencePrivacyAction} <tt>unblock-in</tt>.
   */
  public static final PresencePrivacyAction UNBLOCK_INBOUND =
      new PresencePrivacyActionImpl(EMPPTokens.PRIVACY_ACTION_UNBLOCKIN);

  /**
   * Defines the {@link PresencePrivacyAction} <tt>unblock-out</tt>.
   */
  public static final PresencePrivacyAction UNBLOCK_OUTBOUND =
      new PresencePrivacyActionImpl(EMPPTokens.PRIVACY_ACTION_UNBLOCKOUT);

  /**
   * Defines the {@link PresencePrivacyAction} <tt>list-ib</tt>.
   */
  public static final PresencePrivacyAction LIST_INBOUND_BLOCKED =
      new PresencePrivacyActionImpl(EMPPTokens.PRIVACY_ACTION_LISTBLOCKIN);

  /**
   * Defines the {@link PresencePrivacyAction} <tt>list-ob</tt>.
   */
  public static final PresencePrivacyAction LIST_OUTBOUND_BLOCKED =
      new PresencePrivacyActionImpl(EMPPTokens.PRIVACY_ACTION_LISTBLOCKOUT);

  /**
   * Array holding the defined {@link PresencePrivacyAction} instances.
   */
  protected static final PresencePrivacyAction[] ACTIONS =
      {
          BLOCK_INBOUND, BLOCK_OUTBOUND, UNBLOCK_INBOUND, UNBLOCK_OUTBOUND,
          LIST_INBOUND_BLOCKED, LIST_OUTBOUND_BLOCKED
      };

  /**
   * Set holding the defined {@link PresencePrivacyAction} instances.
   */
  protected static final Set<PresencePrivacyAction> ACTION_TYPES =
      Collections.unmodifiableSet(new HashSet<PresencePrivacyAction>(Arrays.asList(ACTIONS)));

  /**
   * Tests if the given {@link PresencePrivacyAction} is valid.
   *
   * @param ptype a {@link PresencePrivacyAction}.
   * @return true if valid, false otherwise.
   */
  public final static boolean isValid(PresencePrivacyAction ptype) {
    return ACTION_TYPES.contains(ptype);
  }//isValid

  /**
   * Tests if the given {@link PresencePrivacyAction} is related
   * to a listing.
   *
   * @param ptype a {@link PresencePrivacyAction}.
   * @return true if valid, false otherwise.
   */
  public final static boolean isListAction(PresencePrivacyAction ptype) {
    if (ptype.equals(LIST_INBOUND_BLOCKED)) {
      return true;
    }
    if (ptype.equals(LIST_OUTBOUND_BLOCKED)) {
      return true;
    }
    return false;
  }//isListAction

  /**
   * Returns a {@link PresencePrivacyAction} instance for the given identifier.
   *
   * @param identifier a string identifying a {@link PresencePrivacyAction}.
   * @return the {@link PresencePrivacyAction} with the given identifier.
   * @throws NoSuchElementException if a {@link PresencePrivacyAction} with
   *                                the given identifier does not exist.
   */
  public final static PresencePrivacyAction get(String identifier)
      throws NoSuchElementException {
    for (PresencePrivacyAction pst : ACTION_TYPES) {
      if (pst.getIdentifier().equals(identifier)) {
        return pst;
      }
    }
    throw new NoSuchElementException();
  }//get

  /**
   * Provides an implementation of {@link PresencePrivacyAction}.
   * <p/>
   *
   * @author Dieter Wimberger (wimpi)
   * @version @version@ (@date@)
   */
  static class PresencePrivacyActionImpl
      extends BaseType
      implements PresencePrivacyAction {

    public PresencePrivacyActionImpl(String id) {
      super(id);
    }//constructor

    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (!(o instanceof PresencePrivacyActions.PresencePrivacyActionImpl)) {
        return false;
      }

      final PresencePrivacyActions.PresencePrivacyActionImpl pType = (PresencePrivacyActionImpl) o;

      if (m_Id != null ? !m_Id.equals(pType.m_Id) : pType.m_Id != null) {
        return false;
      }

      return true;
    }//equals

  }//class PresencePrivacyActionImpl

}//class PresencePrivacyAction

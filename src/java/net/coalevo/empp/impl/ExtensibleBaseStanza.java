/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.impl;

import net.coalevo.empp.model.ExtensibleStanza;
import net.coalevo.empp.model.NamespaceExtension;
import net.coalevo.empp.model.PacketKind;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * This abstract class extends {@link net.coalevo.empp.impl.BaseStanza} to allow
 * adding {@link NamespaceExtension} instances.
 * <p/>
 * When {@link net.coalevo.empp.util.Recyclable#recycle()} is called,
 * all extensions will be recycled.
 *
 * @author Dieter Wimberger (coalevo)
 * @version @version@ (@date@)
 */
abstract class ExtensibleBaseStanza
    extends BaseStanza implements ExtensibleStanza {

  protected Map<String, NamespaceExtension> m_Extensions;

  public ExtensibleBaseStanza(PacketKind pk) {
    super(pk);
    m_Extensions = new HashMap<String, NamespaceExtension>();
  }//constructor

  public void recycle() {
    super.recycle();
    //recycle extension instances
    for (String ns : m_Extensions.keySet()) {
      m_Extensions.get(ns).recycle();
    }
    //clear map
    m_Extensions.clear();
  }//recycle

  /**
   * Adds a {@link NamespaceExtension} with a given namespace
   * to this <tt>ExtensibleStanza</tt>.
   * This will be done while the packet is read.
   *
   * @param namespace the namespace identifier.
   * @param se        a {@link NamespaceExtension} instance.
   */
  public void addExtension(String namespace, NamespaceExtension se) {
    m_Extensions.put(namespace, se);
  }//addExtension

  /**
   * Iterator over the contained extension namespaces.
   *
   * @return an <tt>Iterator</tt> instance.
   */
  public Iterator<String> getExtensions() {
    return m_Extensions.keySet().iterator();
  }//getExtensions

  public boolean hasExtension(String namespace) {
    return m_Extensions.containsKey(namespace);
  }//hasExtension

  public boolean hasExtensions() {
    return m_Extensions.size() > 0;
  }//hasExtensions

  /**
   * Returns a {@link NamespaceExtension} for a given namespace.
   *
   * @param namespace the namespace identifier.
   * @return a {@link NamespaceExtension} instance or null if no such extension
   *         has been added for the given namespace.
   */
  public NamespaceExtension getExtension(String namespace) {
    return m_Extensions.get(namespace);
  }//getNamespaceExtension

  public void writeExtensions(XMLStreamWriter writer)
      throws XMLStreamException {
    for (Iterator iterator = m_Extensions.keySet().iterator(); iterator.hasNext();) {
      String ns = (String) iterator.next();
      m_Extensions.get(ns).writeExtensionSpecific(writer);
    }
  }//writeExtensions

  protected void skipTo(XMLStreamReader reader, String to) throws XMLStreamException {
    String name;
    do {
      reader.next();
      if(reader.isCharacters()) {
        reader.next();
      }
      name = reader.getLocalName();
      //System.err.println("Skipped " + name + " Finding " + findend);
    } while(!(to.equals(name) && reader.isEndElement()));
  }//skipTo

}//class ExtensibleStanza

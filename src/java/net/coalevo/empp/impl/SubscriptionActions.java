/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.impl;

import net.coalevo.empp.model.PresenceStatusType;
import net.coalevo.empp.model.SubscriptionAction;

import java.util.*;

/**
 * Provides defined {@link PresenceStatusType} instances.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class SubscriptionActions {

  /**
   * Defines the {@link SubscriptionAction} <tt>request</tt>.
   */
  public static final SubscriptionAction REQUEST =
      new SubscriptionActionImpl(EMPPTokens.SUBSCRIPTION_ACTION_REQEST);

  /**
   * Defines the {@link SubscriptionAction} <tt>allow</tt>.
   */
  public static final SubscriptionAction ALLOW =
      new SubscriptionActionImpl(EMPPTokens.SUBSCRIPTION_ACTION_ALLOW);

  /**
   * Defines the {@link SubscriptionAction} <tt>deny</tt>.
   */
  public static final SubscriptionAction DENY =
      new SubscriptionActionImpl(EMPPTokens.SUBSCRIPTION_ACTION_DENY);

  /**
   * Defines the {@link SubscriptionAction} <tt>unsubscribe</tt>.
   */
  public static final SubscriptionAction UNSUBSCRIBE =
      new SubscriptionActionImpl(EMPPTokens.SUBSCRIPTION_ACTION_UNSUBSCRIBE);

  /**
   * Defines the {@link SubscriptionAction} <tt>cancel</tt>.
   */
  public static final SubscriptionAction CANCEL =
      new SubscriptionActionImpl(EMPPTokens.SUBSCRIPTION_ACTION_CANCEL);

  /**
   * Defines the {@link SubscriptionAction} <tt>list-req<tt>.
   */
  public static final SubscriptionAction LIST_REQ =
      new SubscriptionActionImpl(EMPPTokens.SUBSCRIPTION_ACTION_LISTREQ);

  /**
   * Defines the {@link SubscriptionAction} <tt>list-from<tt>.
   */
  public static final SubscriptionAction LIST_FROM =
      new SubscriptionActionImpl(EMPPTokens.SUBSCRIPTION_ACTION_LISTFROM);

  /**
   * Defines the {@link SubscriptionAction} <tt>list-to<tt>.
   */
  public static final SubscriptionAction LIST_TO =
      new SubscriptionActionImpl(EMPPTokens.SUBSCRIPTION_ACTION_LISTTO);

  /**
   * Defines the {@link SubscriptionAction} <tt>list-ad<tt>.
   */
  public static final SubscriptionAction LIST_AD =
      new SubscriptionActionImpl(EMPPTokens.SUBSCRIPTION_ACTION_LISTAD);

  /**
   * Array holding the defined {@link SubscriptionAction} instances.
   */
  protected static final SubscriptionAction[] ACTIONS =
      {REQUEST, ALLOW, DENY, UNSUBSCRIBE, CANCEL, LIST_REQ, LIST_FROM, LIST_TO, LIST_AD};

  /**
   * Set holding the defined {@link SubscriptionAction} instances.
   */
  protected static final Set<SubscriptionAction> ACTION_TYPES =
      Collections.unmodifiableSet(new HashSet<SubscriptionAction>(Arrays.asList(ACTIONS)));

  /**
   * Tests if the given {@link SubscriptionAction} is valid.
   *
   * @param ptype a {@link SubscriptionAction}.
   * @return true if valid, false otherwise.
   */
  public final static boolean isValid(SubscriptionAction ptype) {
    return SubscriptionActions.ACTION_TYPES.contains(ptype);
  }//isValid

  /**
   * Tests if the given {@link SubscriptionAction} is related to a listing.
   *
   * @param ptype a {@link SubscriptionAction}.
   * @return true if valid, false otherwise.
   */
  public final static boolean isListAction(SubscriptionAction ptype) {
    if (ptype.equals(LIST_REQ)) {
      return true;
    }
    if (ptype.equals(LIST_FROM)) {
      return true;
    }
    if (ptype.equals(LIST_TO)) {
      return true;
    }
    if (ptype.equals(LIST_AD)) {
      return true;
    }
    return false;
  }//isListAction

  /**
   * Returns a {@link SubscriptionAction} instance for the given identifier.
   *
   * @param identifier a string identifying a {@link SubscriptionAction}.
   * @return the {@link SubscriptionAction} with the given identifier.
   * @throws NoSuchElementException if a {@link SubscriptionAction} with
   *                                the given identifier does not exist.
   */
  public final static SubscriptionAction get(String identifier)
      throws NoSuchElementException {
    for (SubscriptionAction pst : SubscriptionActions.ACTION_TYPES) {
      if (pst.getIdentifier().equals(identifier)) {
        return pst;
      }
    }
    throw new NoSuchElementException();
  }//get

  /**
   * Provides an implementation of {@link SubscriptionAction}.
   * <p/>
   *
   * @author Dieter Wimberger (wimpi)
   * @version @version@ (@date@)
   */
  static class SubscriptionActionImpl
      extends BaseType
      implements SubscriptionAction {

    public SubscriptionActionImpl(String id) {
      super(id);
    }//constructor

    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (!(o instanceof SubscriptionActionImpl)) {
        return false;
      }

      final SubscriptionActions.SubscriptionActionImpl pType = (SubscriptionActionImpl) o;

      if (m_Id != null ? !m_Id.equals(pType.m_Id) : pType.m_Id != null) {
        return false;
      }

      return true;
    }//equals

  }//class SubscriptionActionImpl

}//class SubscriptionActions

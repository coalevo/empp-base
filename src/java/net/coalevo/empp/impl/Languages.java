/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.impl;

import net.coalevo.empp.model.Language;

import java.util.HashMap;
import java.util.Map;

/**
 * This class is a factory for {@link Language}
 * instances.
 * <p/>
 * {@link Language} instances will be cached for
 * reuse.
 * </p>
 *
 * @author Dieter Wimberger (coalevo)
 * @version @version@ (@date@)
 * @see net.coalevo.empp.model.Language
 */
public class Languages {

  private static final Map<String, Language> c_Languages =
      new HashMap<String, Language>(20);

  /**
   * Defines the systems default language, which will be
   * derived from the systems default <tt>Locale</tt>.
   */
  public static final Language DEFAULT = new LanguageImpl();

  /**
   * Returns a language for an ISO 631-1 defined two letter
   * identifier.
   * <p/>
   * Note that these instances will be cached.
   * </p>
   *
   * @param iso6311ID the ISO 631-1 two letter identifier.
   * @return a corresponding {@link Language} instance.
   */
  public static final Language getLanguage(String iso6311ID) {
    if (c_Languages.containsKey(iso6311ID)) {
      return c_Languages.get(iso6311ID);
    } else {
      Language l = new LanguageImpl(iso6311ID);
      c_Languages.put(iso6311ID, l);
      return l;
    }
  }//getLanguage

}//class Languages

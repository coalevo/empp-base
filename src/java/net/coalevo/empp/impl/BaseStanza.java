/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.impl;

import net.coalevo.empp.io.BytesInputStream;
import net.coalevo.empp.io.BytesOutputStream;
import net.coalevo.empp.model.*;
import net.coalevo.foundation.util.crypto.UUIDGenerator;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;


/**
 * This class implements an abstract base {@link Stanza}.
 * <p/>
 *
 * @author Dieter Wimberger (coalevo)
 * @version @version@ (@date@)
 */
abstract class BaseStanza
    extends BaseEMPPPacket
    implements Stanza {

  protected String m_Identifier;
  protected Language m_Language;
  protected Type m_Type;
  protected String m_Resource;

  public BaseStanza(PacketKind pk) {
    super(pk);
    setIdentifier(UUIDGenerator.getUID());
  }//BaseStanza

  public String getIdentifier() {
    return m_Identifier;
  }//getIdentifier

  public void setIdentifier(String id) {
    m_Identifier = id;
  }//setIdentifier

  public boolean hasIdentifier() {
    return m_Identifier != null;
  }//hasIdentifier

  public String getResource() {
    return m_Resource;
  }//getResource

  public void setResource(String str) {
    m_Resource = str;
  }//setResource

  public boolean hasResource() {
    return m_Resource != null;
  }//hasResource

  public Type getType() {
    return m_Type;
  }//getKind

  public boolean isType(Type t) {
    return m_Type != null && m_Type.equals(t);
  }//isType

  public abstract void setType(Type t);

  protected abstract void setType(String tid);

  public boolean hasType() {
    return m_Type != null;
  }//hasType

  public Language getLanguage() {
    return m_Language;
  }//getLanguage


  public void setLanguage(Language lang) {
    m_Language = lang;
  }//setLanguage

  public boolean hasLanguage() {
    return m_Language != null;
  }//hasLanguage

  public void recycle() {
    super.recycle();
    //m_Language = Languages.DEFAULT;
    m_Type = null;
    setIdentifier(UUIDGenerator.getUID());
    m_Resource = null;
  }//recycle

  public void preparePayload() {
    //Activator.log().debug("preparePayload()");
    BytesOutputStream bout = c_StreamFactory.leaseBytesOutputStream();
    try {
      writeTo(c_StreamFactory.forOutput(bout));
      //Activator.log().debug("preparePayload()::written.");
      m_Payload = bout.toByteArray();
      m_PLength = m_Payload.length;
      //Activator.log().debug("preparePayload()::done::len="+m_PLength);
    } catch (XMLStreamException ex) {
      Activator.log().error("preparePayload()", ex);
    } finally {
      c_StreamFactory.releaseBytesOutputStream(bout);
    }
  }//preparePayload

  public void parsePayload()
      throws XMLStreamException, MalformedContentException, MalformedStructureException {
    BytesInputStream bin = new BytesInputStream(m_Payload);
    readFrom(c_StreamFactory.forInput(bin));
    bin = null;
  }//parsePayload


  public abstract void writeTo(XMLStreamWriter writer)
      throws XMLStreamException;

  public abstract void readFrom(XMLStreamReader reader)
      throws XMLStreamException, MalformedStructureException, MalformedContentException;


  protected void writeStanzaAttributes(XMLStreamWriter writer)
      throws XMLStreamException {
    if (hasIdentifier()) {
      writer.writeAttribute(EMPPTokens.ATTR_ID, m_Identifier);
    }
    if (hasType()) {
      writer.writeAttribute(EMPPTokens.ATTR_TYPE, m_Type.toString());
    }
    if (hasLanguage()) {
      writer.writeAttribute(EMPPTokens.ATTR_XMLLANG, m_Language.toValue());
    }
    if (hasResource()) {
      writer.writeAttribute(EMPPTokens.ATTR_RESOURCE, m_Resource);
    }
  }//writeStanzaAttributes

  protected void readStanzaAttributes(XMLStreamReader reader)
      throws XMLStreamException, MalformedStructureException, MalformedContentException {

    for (int i = 0; i < reader.getAttributeCount(); i++) {
      String attname = reader.getAttributeName(i).getLocalPart();
      if (EMPPTokens.ATTR_ID.equals(attname)) {
        setIdentifier(reader.getAttributeValue(i));
      } else if (EMPPTokens.ATTR_TYPE.equals(attname)) {
        setType(reader.getAttributeValue(i));
      } else if (EMPPTokens.ATTR_LANG.equals(attname)) {
        setLanguage(Languages.getLanguage(reader.getAttributeValue(i)));
      } else if (EMPPTokens.ATTR_RESOURCE.equals(attname)) {
        setResource(reader.getAttributeValue(i));
      }
    }
  }//readStanzaAttributes

}//class BaseStanza

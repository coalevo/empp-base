/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.impl;

import net.coalevo.empp.model.*;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.util.crypto.UUIDGenerator;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

/**
 * This class implements a {@link net.coalevo.empp.model.InstantMessage}.
 * <p/>
 *
 * @author Dieter Wimberger (coalevo)
 * @version @version@ (@date@)
 */
class InstantMessageImpl
    extends ExtensibleBaseStanza
    implements InstantMessage {

  ////private static final Logger log = LoggerFactory.getLogger(InstantMessageImpl.class);
  protected Map<Language, String> m_Subjects;
  protected Map<Language, String> m_Bodies;
  protected String m_Thread;
  protected boolean m_ConfirmationRequired;
  protected long m_ReceptionTime = -1;
  protected MessageEventType m_MessageEventType;
  protected String m_FormattingHint;

  public InstantMessageImpl() {
    super(PacketKinds.INSTANT_MESSAGE);
    m_Subjects = new TreeMap<Language, String>();
    m_Bodies = new TreeMap<Language, String>();
    //m_Language = new LanguageImpl();
  }//constructor

  public void setType(Type type) {
    if (InstantMessageTypes.isValid(type)) {
      m_Type = type;
    } else {
      throw new IllegalArgumentException();
    }
  }//setType

  public void setType(String tid) {
    m_Type = InstantMessageTypes.get(tid);
  }//setType

  public void setConfirmationRequired(boolean b) {
    m_ConfirmationRequired = b;
  }//setConfirmationRequired

  public boolean isConfirmationRequired() {
    return m_ConfirmationRequired;
  }//isConfirmationRequired

  public void setSubject(String subject) {
    setSubject(m_Language, subject);
  }//setSubject

  public void setSubject(Language l, String s) {
    m_Subjects.put(((l == null) ? Languages.DEFAULT : l), s);
  }//setSubject

  public void removeSubject(Language l) {
    m_Subjects.remove(l);
  }//removeSubject

  public String getSubject(Language l) {
    l = (l == null) ? Languages.DEFAULT : l;
    if (m_Subjects.containsKey(l)) {
      return m_Subjects.get(l);
    } else {
      return null;
    }
  }//getSubject

  public String getSubject() {
    return getSubject(m_Language);
  }//getSubject

  public Iterator getSubjects() {
    return m_Subjects.entrySet().iterator();
  }//getSubjects

  public boolean hasSubjects() {
    return !m_Subjects.isEmpty();
  }//hasSubjects

  public boolean hasSubject(Language l) {
    return m_Subjects.containsKey(l);
  }//hasSubject

  public boolean hasSubject() {
    return m_Subjects.containsKey(Languages.DEFAULT);
  }//hasSubject

  public void setFormattingHint(String str) {
    m_FormattingHint = str;
  }//setFormattingHint

  public String getFormattingHint() {
    return m_FormattingHint;
  }//getFormattingHint

  public boolean hasFormattingHint() {
    return m_FormattingHint != null && m_FormattingHint.length() > 0;  
  }//hasFormattingHint

  public void setBody(String body) {
    setBody(m_Language, body);
  }//setBody

  public void setBody(Language l, String b) {
    m_Bodies.put((l == null) ? Languages.DEFAULT : l, b);
  }//setBody

  public void removeBody(Language l) {
    m_Bodies.remove(l);
  }//removeBody

  public String getBody(Language l) {
    l = (l != null) ? l : Languages.DEFAULT;
    if (m_Bodies.containsKey(l)) {
      return m_Bodies.get(l);
    } else {
      return null;
    }
  }//getBody

  public String getBody() {
    return getBody(m_Language);
  }//getBody

  public Iterator getBodies() {
    return m_Bodies.entrySet().iterator();
  }//getBodies

  public boolean hasBody(Language l) {
    return m_Bodies.containsKey(l);
  }//hasBody

  public boolean hasBody() {
    return m_Bodies.containsKey(Languages.DEFAULT);
  }//hasBody

  public boolean hasBodies() {
    return !m_Bodies.isEmpty();
  }//hasBodies


  public String getThread() {
    return m_Thread;
  }//getThread

  public void setThread(String str) {
    m_Thread = str;
  }//setThread

  public boolean hasThread() {
    return m_Thread != null;
  }//hasThread

  public boolean isThread(String thread) {
    if (hasThread()) {
      return m_Thread.equals(thread);
    } else {
      return false;
    }
  }//isThread

  public long getReceptionTime() {
    return m_ReceptionTime;
  }//getReceptionTime

  public void setReceptionTime(long receptionTime) {
    m_ReceptionTime = receptionTime;
  }//setReceptionTime

  public MessageEventType getEventType() {
    return m_MessageEventType;
  }//getEventType

  public void setEventType(MessageEventType messageEventType) {
    if (MessageEventTypes.isValid(messageEventType)) {
      m_MessageEventType = messageEventType;
    }
  }//setEventType

  public void recycle() {
    super.recycle();
    m_Subjects.clear();
    m_Bodies.clear();
    m_Thread = null;
    m_ReceptionTime = -1;
    m_MessageEventType = null;
    m_FormattingHint = null;
  }//reset

  public ErrorStanza createError() {
    return new ErrorStanzaImpl(m_Kind);
  }//createError

  public void writeTo(XMLStreamWriter writer)
      throws XMLStreamException {

    //1. Message element
    writer.writeStartElement(EMPPTokens.ELEMENT_MESSAGE);
    //2. Attributes
    writeStanzaAttributes(writer);
    if (InstantMessageTypes.CONFIRMATION.equals(m_Type)) {
      writer.writeStartElement(EMPPTokens.ELEMENT_TIMESTAMP);
      writer.writeCharacters(String.valueOf(m_ReceptionTime));
      writer.writeEndElement();
    } else if (InstantMessageTypes.EVENT.equals(m_Type)) {
      writer.writeAttribute(EMPPTokens.ATTR_EVENT, m_MessageEventType.toString());
    } else {
      writer.writeAttribute(EMPPTokens.ATTR_CONFIRM, String.valueOf(m_ConfirmationRequired));
      if(hasFormattingHint()) {
        writer.writeAttribute(EMPPTokens.ATTR_FORMATTINGHINT, m_FormattingHint);
      }
      //3. Subject
      for (Iterator iter = getSubjects(); iter.hasNext();) {
        writer.writeStartElement(EMPPTokens.ELEMENT_SUBJECT);
        Map.Entry<Language, String> entry = (Map.Entry<Language, String>) iter.next();
        Language l = entry.getKey();
        if (!l.equals(m_Language)) {
          writer.writeAttribute(EMPPTokens.ATTR_XMLLANG, l.toValue());
        }
        writer.writeCData(entry.getValue());
        writer.writeEndElement();
      }
      //4. Body
      for (Iterator iter = getBodies(); iter.hasNext();) {
        writer.writeStartElement(EMPPTokens.ELEMENT_BODY);
        Map.Entry<Language, String> entry = (Map.Entry<Language, String>) iter.next();
        Language l = entry.getKey();
        if (!l.equals(m_Language)) {
          writer.writeAttribute(EMPPTokens.ATTR_XMLLANG, l.toValue());
        }
        writer.writeCData(entry.getValue());
        writer.writeEndElement();
      }

      //5. Thread
      if (hasThread()) {
        writer.writeStartElement(EMPPTokens.ELEMENT_THREAD);
        writer.writeCharacters(m_Thread);
        writer.writeEndElement();
      }
    }
    //6.Extensions
    writeExtensions(writer);

    writer.writeEndElement();
    writer.flush();
  }//writeTo

  public void readFrom(XMLStreamReader reader)
      throws XMLStreamException, MalformedStructureException, MalformedContentException {
    //Assume the cursor is on a presence start element
    //and return from this in end element position.
    //1. read stanza common attributes
    reader.nextTag();
    readStanzaAttributes(reader);
    if (InstantMessageTypes.CONFIRMATION.equals(m_Type)) {
      reader.nextTag();
      if (EMPPTokens.ELEMENT_TIMESTAMP.equals(reader.getLocalName())) {
        m_ReceptionTime = Long.parseLong(reader.getElementText());
      }
      return;
    } else if (InstantMessageTypes.EVENT.equals(m_Type)) {
      setEventType(MessageEventTypes.get(
          reader.getAttributeValue("", EMPPTokens.ATTR_EVENT)));
      return;
    }
    setConfirmationRequired(new Boolean(
        reader.getAttributeValue("", EMPPTokens.ATTR_CONFIRM)));
    setFormattingHint(reader.getAttributeValue("", EMPPTokens.ATTR_FORMATTINGHINT));
    boolean done = false;
    do {
      reader.nextTag();
      String name = reader.getLocalName();
      String nsuri = reader.getNamespaceURI();
      if (EMPPTokens.ELEMENT_SUBJECT.equals(name)) {
        if (reader.getAttributeCount() == 0) {
          setSubject(reader.getElementText());
        } else if (EMPPTokens.ATTR_LANG.equals(reader.getAttributeLocalName(0))) {
          setSubject(Languages.getLanguage(reader.getAttributeValue(0)), reader.getElementText());
        } else {
          throw new XMLStreamException();
        }
      } else if (EMPPTokens.ELEMENT_BODY.equals(name)) {
        if (reader.getAttributeCount() == 0) {
          setBody(reader.getElementText());
          //System.err.println(m_Bodies);
        } else if (EMPPTokens.ATTR_LANG.equals(reader.getAttributeLocalName(0))) {
          setBody(Languages.getLanguage(reader.getAttributeValue(0)), reader.getElementText());
          //System.err.println(m_Bodies);
        } else {
          throw new XMLStreamException();
        }
      } else if (EMPPTokens.ELEMENT_THREAD.equals(name)) {
        setThread(reader.getElementText());
      } else if (nsuri != null && nsuri.length() > 0) {
        if (Stanzas.existsIMExtension(nsuri)) {
          try {
            InstantMessageNamespaceExtension imex = Stanzas.leaseIMExtension(nsuri);
            imex.readExtensionSpecific(reader);
            addExtension(nsuri, imex);
          } catch (Exception ex) {
            //TODO: log
          }
        } else {
          skipTo(reader, name);
        }
      } else {
        done = true;
      }
    } while (!done);
  }//readFrom

  public EMPPPacket createInstance() {
    return new InstantMessageImpl();
  }//createInstance

  public static void main(String[] args) {
    try {
      //org.apache.log4j.BasicConfigurator.configure();
      InstantMessageImpl mes = new InstantMessageImpl();
      mes.setIdentifier(UUIDGenerator.getUID());
      //from
      AgentIdentifier from = c_AIDCache.get("jim");
      mes.setFrom(from);

      //to
      AgentIdentifier to = c_AIDCache.get("joe");
      mes.setTo(to);
      //type
      mes.setType(InstantMessageTypes.CHAT);

      mes.setSubject("Permission Update");
      mes.setBody("You are now allowed to do something more.");
      mes.setSubject(Languages.getLanguage("es"), "Cambio de Permisos");
      mes.setBody(Languages.getLanguage("es"), "Ya puedes hacer algo mas.");
      mes.setFormattingHint("tml");
      printInOut(mes);
      mes.recycle();
      mes.setIdentifier(UUIDGenerator.getUID());
      mes.setFrom(from);
      mes.setTo(to);
      mes.setType(InstantMessageTypes.CONFIRMATION);
      mes.setReceptionTime(System.currentTimeMillis());
      printInOut(mes);
      mes.recycle();
      mes.setIdentifier(UUIDGenerator.getUID());
      mes.setFrom(from);
      mes.setTo(to);
      mes.setType(InstantMessageTypes.EVENT);
      mes.setEventType(MessageEventTypes.MSG_START);
      mes.setReceptionTime(System.currentTimeMillis());
      printInOut(mes);

    } catch (Exception ex) {
      Activator.log().error("main()", ex);
    }

  }//main

}//class InstantMessageImpl

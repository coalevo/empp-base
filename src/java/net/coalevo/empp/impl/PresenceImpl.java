/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.impl;

import net.coalevo.empp.model.*;
import net.coalevo.foundation.model.AgentIdentifier;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Provides the implementation of {@link Presence}.
 * <p/>
 *
 * @author Dieter Wimberger (coalevo)
 * @version @version@ (@date@)
 */
class PresenceImpl
    extends ExtensibleBaseStanza
    implements Presence {

  //private static final Logger log = LoggerFactory.getLogger(PresenceImpl.class);
  protected PresenceStatusType m_StatusType;
  protected SubscriptionAction m_SubscriptionAction;
  protected PresencePrivacyAction m_PrivacyAction;
  protected String m_Description;
  protected String m_Reason;
  protected String m_Resources;
  protected List<ListItem> m_Items = new ArrayList<ListItem>();

  public PresenceImpl() {
    super(PacketKinds.PRESENCE);
  }//constructor

  public void setType(Type type) {
    if (PresenceTypes.isValid(type)) {
      m_Type = type;
    } else {
      throw new IllegalArgumentException();
    }
  }//setKind

  public void setType(String tid) {
    m_Type = PresenceTypes.get(tid);
  }//setKind

  public void setStatusType(PresenceStatusType pst) {
    m_StatusType = pst;
  }//setStatusType

  public PresenceStatusType getStatusType() {
    return m_StatusType;
  }//getStatusType

  public boolean hasStatusType() {
    return m_StatusType != null;
  }//hasStatusType

  public String getDescription() {
    return m_Description;
  }//getDescription

  public void setDescription(String str) {
    m_Description = str;
  }//setDescription

  public boolean hasDescription() {
    return m_Description != null && m_Description.length() > 0;
  }//hasDescription

  public String getReason() {
    return m_Reason;
  }//getReason

  public void setReason(String str) {
    m_Reason = str;
  }//setReason

  public boolean hasReason() {
    return m_Reason != null;
  }//hasReason

  public void setResources(String[] res) {
    m_Resources = join(res, LIST_SPLIT);
  }//setResources

  public String[] getResources() {
    return m_Resources.split(LIST_SPLIT);
  }//getResources

  public boolean hasResources() {
    return m_Resources != null;
  }//hasResources

  public void setSubscriptionAction(SubscriptionAction action) {
    if (SubscriptionActions.isValid(action)) {
      m_SubscriptionAction = action;
    }
  }//setPresenceSubscriptionAction

  public SubscriptionAction getSubscriptionAction() {
    return m_SubscriptionAction;
  }//getSubscriptionAction

  public boolean hasSubscriptionAction() {
    return m_SubscriptionAction != null;
  }//hasSubscriptionAction

  public PresencePrivacyAction getPrivacyAction() {
    return m_PrivacyAction;
  }//getPresencePrivacyAction

  public void setPrivacyAction(PresencePrivacyAction privacyAction) {
    m_PrivacyAction = privacyAction;
  }//setPresencePrivacyAction

  public boolean hasPrivacyAction() {
    return m_PrivacyAction != null;
  }//hasPresencePrivacyAction

  public void addItem(AgentIdentifier aid, String nick) {
    m_Items.add(new ListItemImpl(aid, nick));
  }//addItem

  public Iterator<ListItem> getItems() {
    return m_Items.listIterator();
  }//getItems

  public int getItemCount() {
    return m_Items.size();
  }//getItemCount

  public void recycle() {
    super.recycle();
    m_StatusType = null;
    m_Description = null;
    m_Reason = null;
    m_SubscriptionAction = null;
    m_PrivacyAction = null;
    m_Items.clear();
  }//reset

  public void writeTo(XMLStreamWriter writer) throws XMLStreamException {
    //Activator.log().debug("writeTo(XMLStreamWriter)");
    //1. Presence element
    writer.writeStartElement(EMPPTokens.ELEMENT_PRESENCE);
    //2. Stanza attributes
    writeStanzaAttributes(writer);

    //3. Write per type
    if (PresenceTypes.PRESENT.equals(m_Type) || PresenceTypes.STATUSUPDATE.equals(m_Type)) {
      //write status and description
      writer.writeStartElement(EMPPTokens.ELEMENT_STATUS);
      writer.writeCharacters(m_StatusType.toString());
      writer.writeEndElement();

      if (hasDescription()) {
        writer.writeStartElement(EMPPTokens.ELEMENT_DESCRIPTION);
        writer.writeCData(m_Description);
        writer.writeEndElement();
      }

      if (hasResources()) {
        writer.writeStartElement(EMPPTokens.ELEMENT_RESOURCES);
        writer.writeCharacters(m_Resources);
        writer.writeEndElement();
      }

    } else if (PresenceTypes.SUBSCRIPTION.equals(m_Type)) {
      writer.writeStartElement(EMPPTokens.ELEMENT_ACTION);
      writer.writeCharacters(m_SubscriptionAction.toString());
      writer.writeEndElement();
      if (SubscriptionActions.isListAction(m_SubscriptionAction) && m_Items.size() > 0) {
        writer.writeStartElement(EMPPTokens.ELEMENT_LIST);
        for (ListItem item : m_Items) {
          writer.writeStartElement(EMPPTokens.ELEMENT_ITEM);
          writer.writeAttribute(EMPPTokens.ATTR_ID, item.getIdentifier().getIdentifier());
          writer.writeAttribute(EMPPTokens.ATTR_ALIAS, item.getAlias());
          writer.writeEndElement();
        }
        writer.writeEndElement();
      } else {
        if (hasReason()) {
          writer.writeStartElement(EMPPTokens.ELEMENT_REASON);
          writer.writeCData(m_Reason);
          writer.writeEndElement();
        }
      }
    } else if (PresenceTypes.PRIVACY.equals(m_Type)) {
      writer.writeStartElement(EMPPTokens.ELEMENT_ACTION);
      writer.writeCharacters(m_PrivacyAction.toString());
      writer.writeEndElement();
      if (PresencePrivacyActions.isListAction(m_PrivacyAction) && m_Items.size() > 0) {
        writer.writeStartElement(EMPPTokens.ELEMENT_LIST);
        for (ListItem item : m_Items) {
          writer.writeStartElement(EMPPTokens.ELEMENT_ITEM);
          writer.writeAttribute(EMPPTokens.ATTR_ID, item.getIdentifier().getIdentifier());
          writer.writeAttribute(EMPPTokens.ATTR_ALIAS, item.getAlias());
          writer.writeEndElement();
        }
        writer.writeEndElement();
      } else {
        if (hasReason()) {
          writer.writeStartElement(EMPPTokens.ELEMENT_REASON);
          writer.writeCData(m_Reason);
          writer.writeEndElement();
        }
      }
    }
    //4. Presence Extensions
    writeExtensions(writer);
    //5. end presence element
    writer.writeEndElement();
    writer.flush();
  }//writeTo

  public void readFrom(XMLStreamReader reader) throws XMLStreamException, MalformedStructureException,
      MalformedContentException {
    //System.err.println("!!!!!!!!PAYLOAD == " + new String(m_Payload));
    //Assume the cursor is on a presence start element
    //and return from this in end element position.
    //1. read stanza common attributes
    reader.nextTag();
    //System.err.println("TAG: " + reader.getLocalName());
    readStanzaAttributes(reader);
    boolean done = false;
    do {
      reader.nextTag();
      String name = reader.getLocalName();
      String nsuri = reader.getNamespaceURI();
      //System.err.println("TAG: " + name + " NAMESPACE: -->" + nsuri +"<--");
      if (PresenceTypes.PRESENT.equals(m_Type) || PresenceTypes.STATUSUPDATE.equals(m_Type)) {
        if (EMPPTokens.ELEMENT_STATUS.equals(name)) {
          setStatusType(PresenceStatusTypes.get(reader.getElementText()));
        } else if (EMPPTokens.ELEMENT_DESCRIPTION.equals(name)) {
          m_Description = reader.getElementText();
        } else if (EMPPTokens.ELEMENT_RESOURCES.equals(name)) {
          m_Resources = reader.getElementText();
        }
      } else if (PresenceTypes.SUBSCRIPTION.equals(m_Type)) {
        if (EMPPTokens.ELEMENT_ACTION.equals(name)) {
          setSubscriptionAction(SubscriptionActions.get(reader.getElementText()));
          if (SubscriptionActions.isListAction(m_SubscriptionAction)) {
            //read in list
            reader.nextTag();
            name = reader.getLocalName();
            nsuri = reader.getNamespaceURI();
            if (EMPPTokens.ELEMENT_LIST.equals(name)) {
              boolean listdone = false;
              do {
                reader.nextTag();
                name = reader.getLocalName();
                if (EMPPTokens.ELEMENT_LIST.equals(name)) {
                  listdone = true;
                  continue;
                }
                if (EMPPTokens.ELEMENT_ITEM.equals(name) && reader.isStartElement()) {
                  m_Items.add(new ListItemImpl(
                      c_AIDCache.get(reader.getAttributeValue(0)),
                      reader.getAttributeValue(1)
                  ));
                }

              } while (!listdone);
            }

          }
        } else if (EMPPTokens.ELEMENT_REASON.equals(name)) {
          m_Reason = reader.getElementText();
        }
      } else if (PresenceTypes.PRIVACY.equals(m_Type)) {
        if (EMPPTokens.ELEMENT_ACTION.equals(name)) {
          setPrivacyAction(PresencePrivacyActions.get(reader.getElementText()));
          if (PresencePrivacyActions.isListAction(m_PrivacyAction)) {
            //read in list
            reader.nextTag();
            name = reader.getLocalName();
            nsuri = reader.getNamespaceURI();
            if (EMPPTokens.ELEMENT_LIST.equals(name)) {
              boolean listdone = false;
              do {
                reader.nextTag();
                name = reader.getLocalName();
                if (EMPPTokens.ELEMENT_LIST.equals(name)) {
                  listdone = true;
                  continue;
                }
                if (EMPPTokens.ELEMENT_ITEM.equals(name) && reader.isStartElement()) {
                  m_Items.add(new ListItemImpl(
                      c_AIDCache.get(reader.getAttributeValue(0)),
                      reader.getAttributeValue(1)
                  ));
                }

              } while (!listdone);
            }

          }
        } else if (EMPPTokens.ELEMENT_REASON.equals(name)) {
          m_Reason = reader.getElementText();
        }
      }
      if (nsuri != null && nsuri.length() > 0) {
        if (Stanzas.existsPresenceExtension(nsuri)) {
          try {
            PresenceNamespaceExtension pex = Stanzas.leasePresenceExtension(nsuri);
            pex.readExtensionSpecific(reader);
            addExtension(nsuri, pex);
          } catch (Exception ex) {
            Activator.log().error("readFrom()", ex);
          }
        } else {
          System.out.println("Will call skip");
          skipTo(reader, name);
        }
      } else if (EMPPTokens.ELEMENT_PRESENCE.equals(name) && reader.isEndElement()) {
        done = true;
      }
    } while (!done);
  }//readFrom

  public EMPPPacket createInstance() {
    return new PresenceImpl();
  }//createInstance

  public static void main(String[] args) {
    //org.apache.log4j.BasicConfigurator.configure();

    String[] test = {"Hello", "World"};
    System.out.println(join(test, LIST_SPLIT));
    try {
      PresenceImpl p = new PresenceImpl();
      AgentIdentifier aid = new AgentIdentifier("wimpi@coalevo.net");
      p.setTo(aid);
      p.setFrom(aid);
      p.setType(PresenceTypes.PRESENT);
      p.setStatusType(PresenceStatusTypes.AVAILABLE);
      p.setResources(test);
      p.setDescription("I am available.");
      printInOut(p);
      p.recycle();

      p.setTo(aid);
      p.setFrom(aid);
      p.setType(PresenceTypes.STATUSUPDATE);
      p.setStatusType(PresenceStatusTypes.ADVERTISED_TEMPORARILY_UNAVAILABLE);
      p.setResources(test);
      p.setDescription("I am available.");
      printInOut(p);
      p.recycle();

      p.setTo(aid);
      p.setFrom(aid);
      p.setType(PresenceTypes.ABSENT);
      printInOut(p);
      p.recycle();

      p.setTo(aid);
      p.setFrom(aid);
      p.setType(PresenceTypes.SUBSCRIPTION);
      p.setSubscriptionAction(SubscriptionActions.REQUEST);
      printInOut(p);
      p.recycle();

      p.setTo(aid);
      p.setFrom(aid);
      p.setType(PresenceTypes.SUBSCRIPTION);
      p.setSubscriptionAction(SubscriptionActions.ALLOW);
      printInOut(p);
      p.recycle();

      p.setTo(aid);
      p.setFrom(aid);
      p.setType(PresenceTypes.SUBSCRIPTION);
      p.setSubscriptionAction(SubscriptionActions.DENY);
      p.setReason("Hmpf.");
      printInOut(p);
      p.recycle();

      p.setTo(aid);
      p.setFrom(aid);
      p.setType(PresenceTypes.SUBSCRIPTION);
      p.setSubscriptionAction(SubscriptionActions.CANCEL);
      p.setReason("Hmpf.");
      printInOut(p);
      p.recycle();

      p.setTo(aid);
      p.setFrom(aid);
      p.setType(PresenceTypes.SUBSCRIPTION);
      p.setSubscriptionAction(SubscriptionActions.UNSUBSCRIBE);
      p.setReason("Hmpf.");
      printInOut(p);
      p.recycle();

      p.setTo(aid);
      p.setFrom(aid);
      p.setType(PresenceTypes.SUBSCRIPTION);
      p.setSubscriptionAction(SubscriptionActions.LIST_REQ);
      p.addItem(c_AIDCache.get("wimpi"), "Wimpi");
      p.addItem(c_AIDCache.get("kenshin"), "Kenshin");
      printInOut(p);
      p.recycle();

      p.setTo(aid);
      p.setFrom(aid);
      p.setType(PresenceTypes.SUBSCRIPTION);
      p.setSubscriptionAction(SubscriptionActions.LIST_FROM);
      p.addItem(c_AIDCache.get("wimpi"), "Wimpi");
      p.addItem(c_AIDCache.get("kenshin"), "Kenshin");
      printInOut(p);
      p.recycle();

      p.setTo(aid);
      p.setFrom(aid);
      p.setType(PresenceTypes.SUBSCRIPTION);
      p.setSubscriptionAction(SubscriptionActions.LIST_TO);
      p.addItem(c_AIDCache.get("wimpi"), "Wimpi");
      p.addItem(c_AIDCache.get("kenshin"), "Kenshin");
      printInOut(p);
      p.recycle();

      p.setTo(aid);
      p.setFrom(aid);
      p.setType(PresenceTypes.PRIVACY);
      p.setPrivacyAction(PresencePrivacyActions.LIST_INBOUND_BLOCKED);
      p.addItem(c_AIDCache.get("wimpi"), "Wimpi");
      p.addItem(c_AIDCache.get("kenshin"), "Kenshin");
      printInOut(p);
      p.recycle();

      p.setTo(aid);
      p.setFrom(aid);
      p.setType(PresenceTypes.PRIVACY);
      p.setPrivacyAction(PresencePrivacyActions.BLOCK_INBOUND);
      printInOut(p);
      p.recycle();

      p.setTo(aid);
      p.setFrom(aid);
      p.setType(PresenceTypes.PRIVACY);
      p.setPrivacyAction(PresencePrivacyActions.BLOCK_OUTBOUND);
      printInOut(p);
      p.recycle();

      p.setTo(aid);
      p.setFrom(aid);
      p.setType(PresenceTypes.PRIVACY);
      p.setPrivacyAction(PresencePrivacyActions.UNBLOCK_INBOUND);
      printInOut(p);
      p.recycle();

      p.setTo(aid);
      p.setFrom(aid);
      p.setType(PresenceTypes.PRIVACY);
      p.setPrivacyAction(PresencePrivacyActions.UNBLOCK_OUTBOUND);
      printInOut(p);
      p.recycle();

      //String str = "<presence id=\"f5Ub5tE5tK02YQvzzau3XdMGcTd06Zk+\" type=\"present\"><status>available</status><roles xmlns=\"net.coalevo.security.roles\"><role>User</role></roles></presence>";
      //StringReader sr = new StringReader(str);
      //XMLStreamReader xmlsr = Activator.getStreamFactory().forInput(sr);
      //PresenceImpl pi = new PresenceImpl();
      //pi.readFrom(xmlsr);

    } catch (Exception ex) {
      ex.printStackTrace();
      //Activator.log().error("main()",ex);
    }
  }//main

  public static class ListItemImpl implements ListItem {

    private AgentIdentifier m_Identifier;
    private String m_Alias;

    public ListItemImpl(AgentIdentifier aid, String nick) {
      m_Identifier = aid;
      m_Alias = nick;
    }//constructor

    public AgentIdentifier getIdentifier() {
      return m_Identifier;
    }//getIdentifier

    public void setIdentifier(AgentIdentifier identifier) {
      m_Identifier = identifier;
    }//setIdentifier

    public String getAlias() {
      return m_Alias;
    }//getAlias

    public void setAlias(String alias) {
      m_Alias = alias;
    }//setAlias

  }//ListItem

  private static final String join(String[] list, String sym) {
    if (list == null || list.length == 0) {
      return null;
    }
    StringBuilder sb = new StringBuilder();
    int last = list.length - 1;
    for (int i = 0; i < last; i++) {
      sb.append(list[i]).append(sym);
    }
    sb.append(list[last]);
    return sb.toString();
  }//join

  private static final String LIST_SPLIT = ",";
}//class PresenceImpl

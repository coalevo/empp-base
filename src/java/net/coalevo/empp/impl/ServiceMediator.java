/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.impl;


import net.coalevo.stax.service.StAXFactoryService;
import org.osgi.framework.*;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * Implements a mediator pattern class for services from the OSGi container.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class ServiceMediator {

  private BundleContext m_BundleContext;

  private StAXFactoryService m_StAXFactoryService;
  private CountDownLatch m_StAXFactoryServiceLatch;

  private XMLInputFactory m_InputFactory;
  private XMLOutputFactory m_OutputFactory;

  public StAXFactoryService getStAXFactoryService(long wait) {
    try {
      if (wait < 0) {
        m_StAXFactoryServiceLatch.await();
      } else if (wait > 0) {
        m_StAXFactoryServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      e.printStackTrace(System.err);
    }

    return m_StAXFactoryService;
  }//getStAXFactoryService

  public XMLInputFactory getXMLInputFactory(long wait) {
    try {
      if (wait < 0) {
        m_StAXFactoryServiceLatch.await();
      } else if (wait > 0) {
        m_StAXFactoryServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      e.printStackTrace(System.err);
    }

    return m_InputFactory;
  }//getXMLInputFactory

  public XMLOutputFactory getXMLOutputFactory(long wait) {
    try {
      if (wait < 0) {
        m_StAXFactoryServiceLatch.await();
      } else if (wait > 0) {
        m_StAXFactoryServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      e.printStackTrace(System.err);
    }

    return m_OutputFactory;
  }//getXMLOutputFactory

  public boolean activate(BundleContext bc) {
    //get the context
    m_BundleContext = bc;

    m_StAXFactoryServiceLatch = createWaitLatch();

    //prepareDefinitions listener
    ServiceListener serviceListener = new ServiceListenerImpl();

    //prepareDefinitions the filter
    String filter = "(objectclass=" + StAXFactoryService.class.getName() + ")";

    try {
      //add the listener to the bundle context.
      bc.addServiceListener(serviceListener, filter);

      //ensure that already registered Service instances are registered with
      //the manager
      ServiceReference[] srl = bc.getServiceReferences(null, filter);
      for (int i = 0; srl != null && i < srl.length; i++) {
        serviceListener.serviceChanged(new ServiceEvent(ServiceEvent.REGISTERED, srl[i]));
      }
    } catch (InvalidSyntaxException ex) {
      ex.printStackTrace(System.err);
      return false;
    }
    return true;
  }//activate

  public void deactivate() {
    m_StAXFactoryService = null;
    m_StAXFactoryServiceLatch = null;

    m_BundleContext = null;
  }//deactivate

  private CountDownLatch createWaitLatch() {
    return new CountDownLatch(1);
  }//createWaitLatch

  private class ServiceListenerImpl
      implements ServiceListener {

    public void serviceChanged(ServiceEvent ev) {
      ServiceReference sr = ev.getServiceReference();
      Object o = null;
      switch (ev.getType()) {
        case ServiceEvent.REGISTERED:
          o = m_BundleContext.getService(sr);
          if (o == null) {
            return;
          } else if (o instanceof StAXFactoryService) {
            m_StAXFactoryService = (StAXFactoryService) o;
            m_InputFactory = m_StAXFactoryService.getXMLInputFactory();
            m_OutputFactory = m_StAXFactoryService.getXMLOutputFactory();
            m_StAXFactoryServiceLatch.countDown();
          } else {
            m_BundleContext.ungetService(sr);
          }
          break;
        case ServiceEvent.UNREGISTERING:
          o = m_BundleContext.getService(sr);
          if (o == null) {
            return;
          } else if (o instanceof StAXFactoryService) {
            m_StAXFactoryService = null;
            m_InputFactory = null;
            m_OutputFactory = null;
            m_StAXFactoryServiceLatch = createWaitLatch();
          } else {
            m_BundleContext.ungetService(sr);
          }
          break;
      }
    }
  }//inner class ServiceListenerImpl

  public static long WAIT_UNLIMITED = -1;
  public static long NO_WAIT = 0;

}//class ServiceMediator

/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.impl;

import net.coalevo.empp.model.MessageEventType;

import java.util.*;

/**
 * Provides defined {@link MessageEventType} instances.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class MessageEventTypes {

  /**
   * Defines the {@link MessageEventType} <tt>start</tt>.
   */
  public static final MessageEventType MSG_START =
      new MessageEventTypes.MessageEventTypeImpl(EMPPTokens.EVENT_TYPE_START);

  /**
   * Defines the {@link MessageEventType} <tt>stop</tt>.
   */
  public static final MessageEventType MSG_STOP =
      new MessageEventTypes.MessageEventTypeImpl(EMPPTokens.EVENT_TYPE_STOP);

  /**
   * Array holding the defined {@link MessageEventType} instances.
   */
  protected static final MessageEventType[] TYPES =
      {MSG_START, MSG_STOP};

  /**
   * Set holding the defined {@link MessageEventType} instances.
   */
  protected static final Set<MessageEventType> NOTIFICATION_TYPES =
      Collections.unmodifiableSet(new HashSet<MessageEventType>(Arrays.asList(MessageEventTypes.TYPES)));

  /**
   * Tests if the given {@link MessageEventType} is valid.
   *
   * @param ptype a {@link MessageEventType}.
   * @return true if valid, false otherwise.
   */
  public final static boolean isValid(MessageEventType ptype) {
    return MessageEventTypes.NOTIFICATION_TYPES.contains(ptype);
  }//isValid

  /**
   * Returns a {@link MessageEventType} instance for the given identifier.
   *
   * @param identifier a string identifying a {@link MessageEventType}.
   * @return the {@link MessageEventType} with the given identifier.
   * @throws NoSuchElementException if a {@link MessageEventType} with
   *                                the given identifier does not exist.
   */
  public final static MessageEventType get(String identifier)
      throws NoSuchElementException {
    for (MessageEventType pst : MessageEventTypes.NOTIFICATION_TYPES) {
      if (pst.getIdentifier().equals(identifier)) {
        return pst;
      }
    }
    throw new NoSuchElementException();
  }//get

  /**
   * Provides an implementation of {@link MessageEventType}.
   * <p/>
   *
   * @author Dieter Wimberger (wimpi)
   * @version @version@ (@date@)
   */
  static class MessageEventTypeImpl
      extends BaseType
      implements MessageEventType {

    public MessageEventTypeImpl(String id) {
      super(id);
    }//constructor

    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (!(o instanceof MessageEventTypes.MessageEventTypeImpl)) {
        return false;
      }

      final MessageEventTypes.MessageEventTypeImpl pType = (MessageEventTypes.MessageEventTypeImpl) o;

      if (m_Id != null ? !m_Id.equals(pType.m_Id) : pType.m_Id != null) {
        return false;
      }

      return true;
    }//equals

  }//class MessageEventTypeImpl

}//class MessageEventType

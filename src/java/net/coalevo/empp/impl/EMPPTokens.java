/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.empp.impl;

/**
 * This non-instantiable class defines EMPP specific
 * constant tokens.
 * <p/>
 *
 * @author Dieter Wimberger (coalevo)
 * @version @version@ (@date@)
 */
class EMPPTokens {


  private EMPPTokens() {

  }//constructor

  public static final String STANZA_TYPE_ERROR = "error";

  public static final String MESSAGE_TYPE_CHAT = "chat";
  public static final String MESSAGE_TYPE_GROUPCHAT = "groupchat";
  public static final String MESSAGE_TYPE_NEWS = "news";
  public static final String MESSAGE_TYPE_NOTIFICATION = "notification";
  public static final String MESSAGE_TYPE_CONFIRMATION = "confirmation";
  public static final String MESSAGE_TYPE_EVENT = "event";

  public static final String MESSAGE_TYPE_ERROR = STANZA_TYPE_ERROR;


  public static final String EVENT_TYPE_START = "start";
  public static final String EVENT_TYPE_STOP = "stop";


  public static final String PRESENCE_TYPE_BECAMEPRESENT = "present";
  public static final String PRESENCE_TYPE_BECAMEABSENT = "absent";
  public static final String PRESENCE_TYPE_STATUSUPDATE = "status";
  public static final String PRESENCE_TYPE_SUBSCRIPTION = "subscription";
  public static final String PRESENCE_TYPE_RESOURCEUPDATE = "resources";
  public static final String PRESENCE_TYPE_PRIVACY = "privacy";


  public static final String PRESENCE_TYPE_PROBE = "probe";
  public static final String PRESENCE_TYPE_ERROR = STANZA_TYPE_ERROR;

  public static final String PRESENCE_STATUSTYPE_ADVERTISED_AVAILABLE = "ad-available";
  public static final String PRESENCE_STATUSTYPE_AVAILABLE = "available";
  public static final String PRESENCE_STATUSTYPE_UNAVAILABLE = "unavailable";
  public static final String PRESENCE_STATUSTYPE_TEMPORARY_UNAVAILABLE = "tmp-unavailable";
  public static final String PRESENCE_STATUSTYPE_ADVERTISED_TEMPORARY_UNAVAILABLE = "ad-tmp-unavailable";

  public static final String SUBSCRIPTION_ACTION_REQEST = "request";
  public static final String SUBSCRIPTION_ACTION_ALLOW = "allow";
  public static final String SUBSCRIPTION_ACTION_DENY = "deny";
  public static final String SUBSCRIPTION_ACTION_UNSUBSCRIBE = "unsubscribe";
  public static final String SUBSCRIPTION_ACTION_CANCEL = "cancel";

  public static final String SUBSCRIPTION_ACTION_LISTREQ = "list-req";
  public static final String SUBSCRIPTION_ACTION_LISTFROM = "list-from";
  public static final String SUBSCRIPTION_ACTION_LISTTO = "list-to";
  public static final String SUBSCRIPTION_ACTION_LISTAD = "list-ad";

  public static final String PRIVACY_ACTION_BLOCKIN = "block-in";
  public static final String PRIVACY_ACTION_BLOCKOUT = "block-out";
  public static final String PRIVACY_ACTION_UNBLOCKIN = "unblock-in";
  public static final String PRIVACY_ACTION_UNBLOCKOUT = "unblock-out";
  public static final String PRIVACY_ACTION_LISTBLOCKIN = "list-ib";
  public static final String PRIVACY_ACTION_LISTBLOCKOUT = "list-ob";


  public static final String IQ_TYPE_REQUEST = "request";
  public static final String IQ_TYPE_RESPONSE = "response";
  public static final String IQ_TYPE_ERROR = STANZA_TYPE_ERROR;


  public static final String STANZAERROR_TYPE_CANCEL = "cancel";
  public static final String STANZAERROR_TYPE_CONTINUE = "continue";
  public static final String STANZAERROR_TYPE_MODIFY = "modify";
  public static final String STANZAERROR_TYPE_AUTH = "auth";
  public static final String STANZAERROR_TYPE_WAIT = "wait";

  public static final String STANZAERROR_COND_BADREQUEST = "bad-request";
  public static final String STANZAERROR_COND_CONFLICT = "conflict";
  public static final String STANZAERROR_COND_FEATURENOTIMPLEMENTED = "feature-not-implemented";
  public static final String STANZAERROR_COND_FORBIDDEN = "forbidden";
  public static final String STANZAERROR_COND_GONE = "gone";
  public static final String STANZAERROR_COND_INTERNALSERVERERROR = "internal-server-error";
  public static final String STANZAERROR_COND_ITEMNOTFOUND = "item-not-found";
  public static final String STANZAERROR_COND_JIDMALFORMED = "jid-malformed";
  public static final String STANZAERROR_COND_NOTACCEPTABLE = "not-acceptable";
  public static final String STANZAERROR_COND_NOTALLOWED = "not-allowed";
  public static final String STANZAERROR_COND_NOTAUTHORIZED = "not-authorized";
  public static final String STANZAERROR_COND_PAYMENTREQUIRED = "payment-required";
  public static final String STANZAERROR_COND_RECIPIENTUNAVAILABLE = "recipient-unavailable";
  public static final String STANZAERROR_COND_REDIRECT = "redirect";
  public static final String STANZAERROR_COND_REGISTRATIONREQUIRED = "registration-required";
  public static final String STANZAERROR_COND_REMOTESERVERNOTFOUND = "remote-server-not-found";
  public static final String STANZAERROR_COND_REMOTESERVERTIMEOUT = "remote-server-timeout";
  public static final String STANZAERROR_COND_RESOURCECONSTRAINT = "resource-constraint";
  public static final String STANZAERROR_COND_SERVICEUNAVAILABLE = "service-unavailable";
  public static final String STANZAERROR_COND_SUBSCRIPTIONREQUIRED = "subscription-required";
  public static final String STANZAERROR_COND_UNDEFINEDCONDITION = "undefined-condition";
  public static final String STANZAERROR_COND_UNEXPECTEDREQUEST = "unexpected-request";

  public static final String STREAMERROR_COND_BADFORMAT = "bad-format";
  public static final String STREAMERROR_COND_BADNAMESPACEPREFIX = "bad-namespace-prefix";
  public static final String STREAMERROR_COND_CONFLICT = STANZAERROR_COND_CONFLICT;
  public static final String STREAMERROR_COND_CONNECTIONTIMEOUT = "connection-timeout";
  public static final String STREAMERROR_COND_HOSTGONE = "host-gone";
  public static final String STREAMERROR_COND_HOSTUNKNOWN = "host-unknown";
  public static final String STREAMERROR_COND_IMPROPERADDRESSING = "improper-addressing";
  public static final String STREAMERROR_COND_INTERNALSERVERERROR = STANZAERROR_COND_INTERNALSERVERERROR;
  public static final String STREAMERROR_COND_INVALIDFROM = "invalid-from";
  public static final String STREAMERROR_COND_INVALIDID = "invalid-id";
  public static final String STREAMERROR_COND_INVALIDNAMESPACE = "invalid-namespace";
  public static final String STREAMERROR_COND_INVALIDXML = "invalid-xml";
  public static final String STREAMERROR_COND_NOTAUTHORIZED = STANZAERROR_COND_NOTAUTHORIZED;
  public static final String STREAMERROR_COND_POLICYVIOLATION = "policy-violation";
  public static final String STREAMERROR_COND_REMOTECONNFAILED = "remote-connection-failed";
  public static final String STREAMERROR_COND_RESOURCECONSTRAINT = STANZAERROR_COND_RESOURCECONSTRAINT;
  public static final String STREAMERROR_COND_RESTRICTEDXML = "restricted-xml";
  public static final String STREAMERROR_COND_SEEOTHERHOST = "see-other-host";
  public static final String STREAMERROR_COND_SYSTEMSHUTDOWN = "system-shutdown";
  public static final String STREAMERROR_COND_UNDEFINEDCONDITION = STANZAERROR_COND_UNDEFINEDCONDITION;
  public static final String STREAMERROR_COND_UNSUPPORTEDENCODING = "unsupported-encoding";
  public static final String STREAMERROR_COND_UNSUPPORTEDSTANZATYPE = "unsupported-stanza-type";
  public static final String STREAMERROR_COND_UNSUPPORTEDVERSION = "unsupported-version";
  public static final String STREAMERROR_COND_XMLNOTWELLFORMED = "xml-not-well-formed";

  public static final String SESSION_TYPE_CLIENT = "client";
  public static final String SESSION_TYPE_SERVER = "server";

  public static final String ELEMENT_STREAM = "stream";
  public static final String ELEMENT_MESSAGE = "message";
  public static final String ELEMENT_SUBJECT = "subject";
  public static final String ELEMENT_BODY = "body";
  public static final String ELEMENT_THREAD = "thread";
  public static final String ELEMENT_PRESENCE = "presence";
  public static final String ELEMENT_SHOW = "show";
  public static final String ELEMENT_STATUS = "status";
  public static final String ELEMENT_PRIORITY = "priority";
  public static final String ELEMENT_IQ = "iq";
  public static final String ELEMENT_ERROR = "error";
  public static final String ELEMENT_TEXT = "text";
  public static final String ELEMENT_QUERY = "query";
  public static final String ELEMENT_ITEM = "item";
  public static final String ELEMENT_GROUP = "group";
  public static final String ELEMENT_PRESENCE_IN = "presence-in";
  public static final String ELEMENT_PRESENCE_OUT = "presence-out";
  public static final String ELEMENT_LIST = "list";
  public static final String ELEMENT_SESSION = "session";
  public static final String ELEMENT_CLIENT_EPHEMERAL = "A";
  public static final String ELEMENT_SERVER_EPHEMERAL = "B";
  public static final String ELEMENT_SALT = "s";
  public static final String ELEMENT_MATCHPROOF = "M";
  public static final String ELEMENT_ENCRYPT = "encrypt";
  public static final String ELEMENT_ACTION = "action";
  public static final String ELEMENT_DESCRIPTION = "description";
  public static final String ELEMENT_REASON = "reason";
  public static final String ELEMENT_RESOURCES = PRESENCE_TYPE_RESOURCEUPDATE;
  public static final String ELEMENT_TIMESTAMP = "timestamp";

  public static final String ATTR_TO = "to";
  public static final String ATTR_FROM = "from";
  public static final String ATTR_ID = "id";
  public static final String ATTR_XMLLANG = "xml:lang";
  public static final String ATTR_LANG = "lang";
  public static final String ATTR_VERSION = "version";
  public static final String ATTR_TYPE = "type";
  public static final String ATTR_CODE = "code";
  public static final String ATTR_XMLNS = "xmlns";
  public static final String ATTR_NAME = "name";
  public static final String ATTR_SUBSCRIPTION = "subscription";
  public static final String ATTR_ASK = "ask";
  public static final String ATTR_VALUE = "value";
  public static final String ATTR_ACTION = "action";
  public static final String ATTR_ORDER = "order";
  public static final String ATTR_INDEX = "index";
  public static final String ATTR_KEYLENGTH = "keylen";
  public static final String ATTR_RESOURCE = "resource";
  public static final String ATTR_CONFIRM = "confirm";
  public static final String ATTR_EVENT = "event";
  public static final String ATTR_ALIAS = "alias";
  public static final String ATTR_FORMATTINGHINT = "format";

  public static final String PRIVACYRULE_TYPE_GROUP = ELEMENT_GROUP;
  public static final String PRIVACYRULE_TYPE_SUBSCRIPTION = ATTR_SUBSCRIPTION;
  public static final String PRIVACYRULE_ACTION_ALLOW = "allow";
  public static final String PRIVACYRULE_ACTION_DENY = "deny";
  public static final String PRIVACYRULE_BLOCKINGTYPE_MESSAGE = ELEMENT_MESSAGE;
  public static final String PRIVACYRULE_BLOCKINGTYPE_PRESENCE_IN = ELEMENT_PRESENCE_IN;
  public static final String PRIVACYRULE_BLOCKINGTYPE_PRESENCE_OUT = ELEMENT_PRESENCE_OUT;
  public static final String PRIVACYRULE_BLOCKINGTYPE_IQ = ELEMENT_IQ;

  public static final String NAMESPACE_EMPPSTANZAS = "urn:ietf:params:xml:ns:empp-stanzas";
  public static final String NAMESPACE_XML = "http://www.w3.org/XML/1998/namespace";

  public static final String NEGOTIATE_DO = "do";
  public static final String NEGOTIATE_DONT = "dont";
  public static final String NEGOTIATE_WONT = "wont";
  public static final String NEGOTIATE_WILL = "will";

  public static final String STREAM_VERSION = "1.0";

  public static final String PACKET_KIND_ERROR = ELEMENT_ERROR;
  public static final String PACKET_KIND_IQ = ELEMENT_IQ;
  public static final String PACKET_KIND_MESSAGE = ELEMENT_MESSAGE;
  public static final String PACKET_KIND_PRESENCE = ELEMENT_PRESENCE;
  public static final String PACKET_KIND_SESSION = ELEMENT_SESSION;

  public static final int PACKET_ID_MESSAGE = 1;
  public static final int PACKET_ID_PRESENCE = 2;
  public static final int PACKET_ID_IQ = 3;
  public static final int PACKET_ID_SESSION = 4;
  public static final int PACKET_ID_ERROR = 5;
  public static final int PACKET_ID_FILE = 6;

}//class EMPPTokens
